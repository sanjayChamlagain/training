<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends BaseModel
{
    protected $table='district';
    protected $fillable=['id','name'];
    protected $rules=[
      'id'=>'integer',
      
      'name'=>'string|required',
      
    ];

}
