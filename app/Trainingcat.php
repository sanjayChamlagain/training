<?php

namespace App;

class Trainingcat extends BaseModel {

    protected $table = 'training_category';
    protected $fillable = ['event_id', 'name_en', 'name_np'];
    protected $rules = [
        'event_id' => 'integer',
        'name_en' => 'string|required',
        'name_np' => 'nullable|string',
        
    ];
    
}
