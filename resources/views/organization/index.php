<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<div  id="body" class="content-wrapper">
<?php  } ?>
<!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            organization
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">

                    <form class="form-horizontal" id="form" onsubmit="formSubmit(event)" method="post" action="">

                        <input type="hidden" id="id" name="id" value="">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">organization</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">code</label>
                                
                                <div class="col-sm-9">
                                    <input class="form-control" id="code" placeholder="code" type="text" name="code" value="">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="name_en" class="col-sm-3 control-label">Name(english)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_en" placeholder="Name(english)" type="text" name="name_en" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Name(Nepali)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_np" placeholder="Name(Nepali)" type="text" name="name_np" value="">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">organization Type</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="org_type" name="org_type">      
                                      <option id="select" value="">Select one..</option>
                                        
                                            <?php foreach($orgs as $c):?>
                                                
                                                <option value="<?php echo $c->name_en;?>"><?php echo $c->name_en;?></option>
                                                
                                            <?php endforeach;?>
                                                                 
                                  
                                                  <!-- <option value="yes" selected="selected">Yes</option> -->
                                                  <!-- <option value="no" >No</option> -->
                                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">District</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="district" name="district">                                    
                                                     <option id="select" value="">Select one..</option>
                                                  <!-- <option value="jhapa">Jhapa</option>
                                                  <option value="ilam" >Ilam</option>
                                                  <option value="ktm" >Kathmandu</option> -->
                                                  <option value="D69">Achham</option><option value="D49">Baglung</option><option value="D39">Arghakhachi</option><option value="D73">Baitadi</option><option value="D71">Bajhang</option><option value="D70">Bajura</option><option value="D62">Banke</option><option value="D33">Bara</option><option value="D63">Bardiya</option><option value="D29">Bhaktapur</option><option value="D07">Bhojpur</option><option value="D35">Chitawan</option><option value="D74">Dadeldhura</option><option value="D66">Dailekh</option><option value="D60">Dang</option><option value="D72">Darchula</option><option value="D25">Dhading</option><option value="D08">Dhankuta</option><option value="D17">Dhanusha</option><option value="D22">Dolakha</option><option value="D53">Dolpa</option><option value="D68">Doti</option><option value="D44">Gorkha</option><option value="D41">Gulmi</option><option value="D54">Humla</option><option value="D03">Ilam</option><option value="D65">Jajarkot</option><option value="D04">Jhapa</option><option value="D55">Jumla</option><option value="D67">Kailali</option><option value="D56">Kalikot</option><option value="D75">Kanchanpur</option><option value="D38">Kapilvastu</option><option value="D47">Kaski</option><option value="D27">Kathmandu</option><option value="D30">Kavrepalanchowk</option><option value="D12">Khotang</option><option value="D28">Lalitpur</option><option value="D46">Lamjung</option><option value="D18">Mahottari</option><option value="D31">Makawanpur</option><option value="D45">Manang</option><option value="D10">Morang</option><option value="D52">Mugu</option><option value="D51">Mustang</option><option value="D50">Myagdi</option><option value="D36">Nawalparasi</option><option value="D26">Nuwakot</option><option value="D14">Okhaldhunga</option><option value="D40">Palpa</option><option value="D02">Panchthar</option><option value="D48">Parbat</option><option value="D34">Parsa</option><option value="D59">Pyuthan</option><option value="D21">Ramechhap</option><option value="D24">Rasuwa</option><option value="D32">Rautahat</option><option value="D58">Rolpa</option><option value="D57">Rukum</option><option value="D37">Rupandehi</option><option value="D61">Salyan</option><option value="D05">Sankhuwasava</option><option value="D15">Saptari</option><option value="D19">Sarlahi</option><option value="D23">Sidhupalchowk</option><option value="D20">Sindhuli</option><option value="D16">Siraha</option><option value="D11">Solukhumbu</option><option value="D09">Sunsari</option><option value="D64">Surkhet</option><option value="D42">Syangja</option><option value="D43">Tanahun</option><option value="D01">Taplejung</option><option value="D06">Terhathum</option><option value="D13">Udayapur</option>
                        
                                                
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">VDC</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="vdc" placeholder="vdc" type="text" name="vdc" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Ward No.</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="ward" placeholder="Ward No." type="text" name="ward" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Street</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="street" placeholder="street" type="text" name="street" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">House No.</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="house_no" placeholder="House No." type="text" name="house_no" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Latitude</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="latitude" placeholder="latitude" type="text" name="latitude" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Longitude</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="longitude" placeholder="Longitude" type="text" name="longitude" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Phone</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="phone" placeholder="phone" type="text" name="phone" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Fax</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="fax" placeholder="fax" type="text" name="fax" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="email" placeholder="Email" type="text" name="email" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">website</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="website" placeholder="Website" type="text" name="website" value="">
                                </div>
                            </div>










                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
    <div class="box col-md-4">
        <div class="box-header with-border">
            <h3 class="box-title">organization</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
         <div id="table" class="box-body">
            <form id="tableform" >
            <b>SHOW</b><select id="selectentry" onchange="table(event)">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
                <b>ENTRIES</b>

                <div style="float:right">
          <form id="srch" name="srch">
        <input  id="searchfill" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn" name="submit">Search</button>
      </form>
    </div>

            <div id="showtable" class="box-body">
                <table id="organization-table" class="table table-striped table-bordered">
                  <tr><th>ID</th><th>Code</th><th>Name_en</th><th>Organization Type</th><th>ACTIONS</th></tr>

                    </table>
            </div>
          <div id="pagg">

            <ul class="pagination pagination-sm">
            </ul>

              </div>
        </form>


        </div>
    </div>
</div>

        </div>
    </section>
    <!-- /.content -->

<!-- /.content-wrapper -->

<script type="text/javascript">
var baseurl="<?php echo url('/');?>";
// var baseurl=window.location.hostname;


function table(){

    var entry=$("#selectentry").val();

var search=$("#searchfill").val();
 if(($("#searchfill").val())!=""){
    var search=$("#searchfill").val();
  }
var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index;
}
$.ajax({

    method:'get',
    url:baseurl+"/organization/list?entry="+entry+"&page="+page+"&search="+search,
    success:function(response){
        createTable(response);

    },
    fail:function(){
        alert("failed");
    }
});



}



function searchClicked(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
    url:baseurl+"/organization/list?entry="+entry+"&search="+search,
    success:function(response){
      createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});
}


function createTable(resp){

    var t=document.getElementById("organization-table");
   $("body #organization-table").find("tr:gt(0)").remove();

    var rowCount=1;
    var data=resp.data;
    for(var i in data){
    var row=t.insertRow(rowCount);

    row.insertCell(0).innerHTML=data[i].id;
    row.insertCell(1).innerHTML=data[i].code;
    row.insertCell(2).innerHTML=data[i].name_en;
    row.insertCell(3).innerHTML=data[i].org_type;
    row.insertCell(4).innerHTML="<a href='javascript:void(0)' onclick='edit("+data[i].id+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='delt("+data[i].id+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
        rowCount++;

   }
   callForPagination(resp);
     return true;
}

   function formSubmit(e){
    // alert(baseurl);
    e.preventDefault();
    if( $('#id').val()==""){
      // var url="http://localhost/pfmsp/organization/creates";
            var url=baseurl+"/organization/creates";
            }
           else{
            var url=baseurl+"/organization/updates/"+ $('#id').val();
            }
       $.ajax({
           method:"POST",
           url:url,
           data:$("#form").serialize(),
           success:function(resp){
            var a =JSON.parse(resp);
             if(a.status==1){
            $("#code").val("");
             $('#name_en').val("");
             $('#name_np').val("");
             $("#org_type").val("");
             $('#district').val("");
             $('#vdc').val("");
             $("#ward").val("");
             $('#street').val("");
             $('#house_no').val("");
             $("#latitude").val("");
             $('#longitude').val("");
             $('#phone').val("");
             $('#fax').val("");
             $("#email").val("");
             $('#website').val("");
             $('#submit').text("Create");
         }
               table();
                if(a.records){
                 var records=a.records;
                   a.records.delete;
                     toast(a);
              $("body .pagination li a").trigger("click",records);

                 }else{
                   toast(a);
                   table();
               }


        },
          fail:function(){

          alert("failed");
     }
      });
}

function edit(id){
    $("#submit").text("update");
     $.ajax({
     method:'get',
       url:baseurl+"/organization/edits/"+id,
     success:function(resp){

             $('#id').val(resp.id);
             $('#code').val(resp.code);
             $('#name_en').val(resp.name_en);
             $('#name_np').val(resp.name_np);
              $("#org_type").val(resp.org_type);
             $('#district').val(resp.district);
             $('#vdc').val(resp.vdc);
             $("#ward").val(resp.ward);
             $('#street').val(resp.street);
             $('#house_no').val(resp.house_no);
             $("#latitude").val(resp.latitude);
             $('#longitude').val(resp.longitude);
             $('#phone').val(resp.phone);
             $('#fax').val(resp.fax);
             $("#email").val(resp.email);
             $('#website').val(resp.website);
             

         },
        fail:function(){

         }
     });

   }


function delt(id){
    var conf=confirm("Are you sure you want to delete?");
   if(conf){
    $.ajax({
        method:'get',
      url:baseurl+"/organization/deletes/"+id,
        success:function(resp){
             var a =JSON.parse(resp);
             toast(a);
               table();
        },

        fail:function(){
            alert("Fail");

        }
    });
}
}
function resetForm(){
  $('#id').val('');
  $('#submit').text('Create');
}
</script>

<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>
