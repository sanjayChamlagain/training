<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="<?php echo csrf_token();?>" />
  <title><?php echo empty($title)?"PFMSP":$title;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo asset('assets/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo asset('css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo asset('css/skins/_all-skins.min.css');?>">
  <link rel="stylesheet" href="<?php echo asset('css/jquery.dataTables.min.css');?>">
  <link rel="stylesheet" href="<?php echo asset('css/jquery.toast.min.css');?>">
  <link rel="stylesheet" href="<?php echo asset('css/loader.css');?>">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
      <script type="text/javascript">
      var baseUrl="<?php echo url('/');?>";
          var notification;
          <?php
          //echo $msg;
          if(Session::has('msg')):?>
              notification = JSON.parse('<?php echo Session::get('msg');?>');
          <?php endif;?>
      </script>
</head>

<style type="text/css">
  .content-wrapper
  {
    margin-top:50px;
  }
</style>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header" style="position:fixed;" >
    <!-- Logo -->
    <a href="<?php echo url('/');?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>PFMSP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>PFMSP</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-fixed-top" >
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!--<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
              <span class="hidden-xs fa fa-user fa-lg"></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
             <li class="user-header">
                <p id="user-fullname">

                 
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <!-- <a id="passwordchange" class="btn btn-default btn-flat" data-toggle='modal' data-target='#modal-default'onclick="profile()">Change Password</a>  -->
                </div>
                <div class="pull-right">
                  <a href="<?php echo url('/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
    
  </header>


