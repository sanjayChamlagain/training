<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="<?php echo csrf_token(); ?>" />
        <title>@yield('title')</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo asset('assets/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo asset('css/AdminLTE.min.css'); ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo asset('css/skins/_all-skins.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo asset('css/jquery.dataTables.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo asset('css/jquery.toast.min.css'); ?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var notification;
<?php
//echo $msg;
if (Session::has('msg')):
    ?>
                notification = JSON.parse('<?php echo Session::get('msg'); ?>);
<?php endif; ?>
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
<?php var_dump(Session::all()); ?>
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo url('/'); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>DES</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>DES</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Notifications: style can be found in dropdown.less -->
                            <!-- Tasks: style can be found in dropdown.less -->
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <!--<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                                    <span class="hidden-xs fa fa-user fa-lg"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <p>
                                            Alexander Pierce - Web Developer
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">

                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <!--<li class="header">MAIN NAVIGATION</li>-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Setup</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo url('/country'); ?>"><i class="fa fa-circle-o"></i>Country</a></li>
                                <li><a href="<?php echo url('/state'); ?>"><i class="fa fa-circle-o"></i>State</a></li>
                                <li><a href="<?php echo url('/district'); ?>"><i class="fa fa-circle-o"></i>District</a></li>
                                <li><a href="<?php echo url('/local-gov-type'); ?>"><i class="fa fa-circle-o"></i>Local Govn. Type</a></li>
                                <li><a href="<?php echo url('/local-gov'); ?>"><i class="fa fa-circle-o"></i>Local Govn.</a></li>
                                <li><a href="<?php echo url('/org-type'); ?>"><i class="fa fa-circle-o"></i>Orgn. Type</a></li>
                                <li><a href="<?php echo url('/org-level'); ?>"><i class="fa fa-circle-o"></i>Orgn. Level</a></li>
                                <li><a href="<?php echo url('/org'); ?>"><i class="fa fa-circle-o"></i>Organization</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->
           @yield('content)
<footer class="main-footer">
<!--    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>-->
    <strong>Copyright &copy; 2014-2016 <a href="#">SAIPAL</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo asset('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo asset('assets/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- SlimScroll -->
<script src="<?php echo asset('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo asset('assets/plugins/fastclick/fastclick.js');?>"></script>
<script src="<?php echo asset('js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo asset('js/jquery.toast.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo asset('js/app.min.js');?>"></script>
<script src="<?php echo asset('js/scripts.js');?>"></script>
<script type="text/javascript">
(function(){
    //work for the notifications
    if(typeof notification === "object" && notification !== null){
        if(notification.status==0){
           var icon = "error";
        }
        else if(notification.status==1){
            var icon = "success";
        }
        else if(notification.status==2){
            var icon = "warning";
        }
        else{
            var icon = "";
        }
        var tostObj = {
            heading:notification.title,
            text:notification.text,
            showHideTransition: 'slide',
            position:'top-right',
            icon:icon
        };
        $.toast(tostObj);
    }
    $('#country-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: $('#country-table').data('url'),
        columns: [
            {data: 0, name: 'id', title:'ID'},
            {data: 1, name: 'name_en',title:'Name(English)'},
            {data: 2, name: 'name_np',title:'Name(Nepali)'}
        ]
    });
})();
</script>
</body>
</html>
