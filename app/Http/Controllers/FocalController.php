<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Focal;
use App\Govorg;
 use App\Govpost;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class FocalController extends Controller {

    public function index() {
      // return view('Govorg.index');
        //$countries = \App\Country::select('id','name_en')->get();
        return view('focal_person.index',['orgs'=>$this->getOrgs(),'pars'=>$this->getPars()]);
    }

    
    public function creates(Request $request){
      $Org = new Focal();
      if($Org->validate($request->all())){
          $Org->fill($request->all());
          $Org->save();
         
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $govid=$request->input("gov_org_id",null);
         if($govid==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $post=$request->input("post_id",null);
         if($post==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Post is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to save data"]);
      }
    }

    
    public function edits($id){
        $focal = Focal::find($id,['id','gov_org_id','name_en','name_np','post_id','phone','mobile','email','approved','disabled']);
        return $focal;
    }

    
    public function updates(Request $request,$id){
       $focal = Focal::find($id);
      if($focal->validate($request->all())){
          $focal->fill($request->all());
          $focal->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $govid=$request->input("gov_org_id",null);
         if($govid==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $post=$request->input("post_id",null);
         if($post==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Post is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update data"]);
      }
    }



    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
      if($search==null){
        $acts = DB::table('focal_person')->select(['focal_person.id', 'focal_person.gov_org_id', 'focal_person.name_en','gov_org.name_en as cname_en','gov_post.name_en as postname'])
        ->join('gov_org','focal_person.gov_org_id','=','gov_org.id')
        ->join('gov_post','focal_person.post_id','=','gov_post.id')
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
      else{

       $acts = DB::table('focal_person')->select(['focal_person.id', 'focal_person.gov_org_id', 'focal_person.name_en','gov_org.name_en as cname_en','gov_post.name_en as postname'])
        ->join('gov_org','focal_person.gov_org_id','=','gov_org.id')
        ->join('gov_post','focal_person.post_id','=','gov_post.id')
        ->where('focal_person.name_en', 'LIKE', "%$search%")
         
         ->orwhere('gov_org.name_en','LIKE',"%$search%")
         ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }




        // return Datatables::of($Govorgs)->addColumn('action', function ($Govorgs) {
        //         return '<a href="javascript:void(0)" onClick="stedit('.$Govorgs->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
        //                 '&nbsp;&nbsp;<a href="javascript:void(0)" class="btn btn-xs btn-danger" onClick="stdelete('.$Govorgs->id.')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
        //     })->make();
    }

    
    public function deletes($id){
        $focal = Focal::find($id);
        try{
        $focal->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }
      catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }

    public function getOrgs(){
        return \App\Govorg::select('id','name_en')->get();
    }


	public function getPars(){
        return \App\Govpost::select('id','name_en')->get();
    }
}
