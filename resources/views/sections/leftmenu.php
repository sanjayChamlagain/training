<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar" style="position:fixed;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!--<li class="header">MAIN NAVIGATION</li>-->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>PFMSP</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo url('/project');?>" onclick="ajaxCall(event,'project')"><i class="fa fa-circle-o"></i>Project Component</a></li>
             <li><a href="<?php echo url('/activity');?>" onclick="ajaxCall(event,'activity')"><i class="fa fa-circle-o"></i>Project Activity</a></li>
              <li><a href="<?php echo url('/subactivity');?>" onclick="ajaxCall(event,'subactivity')"><i class="fa fa-circle-o"></i>Project Sub-Activity</a></li>
             <li><a href="<?php echo url('/designate');?>" onclick="ajaxCall(event,'designate')"><i class="fa fa-circle-o"></i>Designation</a></li>
              <li><a href="<?php echo url('/staff');?>" onclick="ajaxCall(event,'staff')"><i class="fa fa-circle-o"></i>Staff Info</a></li>
              
           
           
          </ul>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Gov Organization</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
         <li><a href="<?php echo url('/govtype');?>" onclick="ajaxCall(event,'govtype')"><i class="fa fa-circle-o"></i>Gov Organization Type</a></li>
               <li><a href="<?php echo url('/govorg');?>" onclick="ajaxCall(event,'govorg')"><i class="fa fa-circle-o"></i>Gov Organization</a></li>
                <li><a href="<?php echo url('/govpost');?>" onclick="ajaxCall(event,'govpost')"><i class="fa fa-circle-o"></i>Government Post</a></li>
                <li><a href="<?php echo url('/focal');?>" onclick="ajaxCall(event,'focal')"><i class="fa fa-circle-o"></i>Focal Person</a></li>
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Events</span>

            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
         <li><a href="<?php echo url('/trainingtype');?>" onclick="ajaxCall(event,'trainingtype')"><i class="fa fa-circle-o"></i>Types of Event</a></li>
          <li style="display:none;"><a href="<?php echo url('/trainingcat');?>" onclick="ajaxCall(event,'trainingcat')"><i class="fa fa-circle-o"></i>Event Category</a></li>
<!--           <li><a href="<?php echo url('/trainingtopic');?>" onclick="ajaxCall(event,'trainingtopic')"><i class="fa fa-circle-o"></i>Training Topic</a></li> -->
           <li><a href="<?php echo url('/resource');?>" onclick="ajaxCall(event,'resource')"><i class="fa fa-circle-o"></i>Resource Person</a></li>
           <li><a href="<?php echo url('/traininginfo');?>" onclick="ajaxCall(event,'traininginfo')"><i class="fa fa-circle-o"></i>Create Training</a></li>
           <li><a href="<?php echo url('/traininginfo/view');?>" onclick="ajaxCall(event,'traininginfo/view')"><i class="fa fa-circle-o"></i>View Training List</a></li>
          </ul>
        </li>


        </li>
         <li id="userview" class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>User</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
           <ul class="treeview-menu">
         <li id="pending" ><a href="<?php echo url('/pendinguser');?>"  onclick="ajaxCall(event,'pendinguser')" ><i class="fa fa-circle-o"></i>View User</a></li> 
         <li><a href="<?php echo url('/register');?>" onclick="ajaxCall(event,'register')"><i class="fa fa-circle-o"></i>Registration</a></li>
        
         </ul>
          </li>   
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Report</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
           <ul class="treeview-menu">
         <li><a href="<?php echo url('/participant');?>" onclick="ajaxCall(event,'participant')"><i class="fa fa-circle-o"></i>Participant</a></li>
         <li><a href="<?php echo url('/events');?>" onclick="ajaxCall(event,'events')"><i class="fa fa-circle-o"></i>Events</a></li>
          <li><a href="<?php echo url('/ppr');?>" onclick="ajaxCall(event,'ppr')"><i class="fa fa-circle-o"></i>PPR Indicator</a></li>
        
         </ul>
          </li>   
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->
