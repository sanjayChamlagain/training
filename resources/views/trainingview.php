<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<div  id="body" class="content-wrapper">
<?php  } ?>
    <section class="content-header">
      <h1>
     Training List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

       
  <div class="box-header with-border">
  

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div id="table" class="box-body">
          <div class="mb10">
          <div class="pull-left">
                        <form id="tableform" >
                        <b>SHOW</b><select id="selectentry" onchange="viewTrainingList(event)">
                          <option value="15">15</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                            <b>ENTRIES</b>
                          </form>
                          </div>
                <div style="float:right">
                      <form id="srch" name="srch" onsubmit="searchClicked(event)" >
                    <input  id="searchfill" placeholder="  search here" type="text" name="search">
                    <button type="submit"  id="searchbtn" name="submit">Search</button>
                  </form>
                </div>
            </div>
                        <div id="showtable" class="box-body">
                            <table id="branch-admin" class="table table-striped table-bordered">
                              <tr><th>Training Name</th><th>Immediate Result</th><th>Sub Activity</th><th>Organised For</th><th>Start Date</th><th>End Date</th><th>Action</th></tr>

                                </table>
                        </div>
                      <div id="pagg">

                        <ul class="pagination pagination-sm">

                        </ul>

                          </div>




                    </div>
                </div>
        

        </div>
        <!-- /.box-body -->
        <!-- /.box-footer-->
      </div>
      </section>    <!-- /.box -->




  <!-- /.content-wrapper -->
<script type="text/javascript">




   function table(){

    var entry=$("#selectentry").val();

var search=$("#searchfill").val();
 if(($("#searchfill").val())!=""){
    var search=$("#searchfill").val();
  }
var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index;
}
$.ajax({

    method:'get',
    url:baseUrl+"/traininginfo/list?entry="+entry+"&page="+page+"&search="+search,
    success:function(response){
        createTable(response);

    },
    fail:function(){
        alert("failed");
    }
});



}



function searchClicked(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
    url:baseUrl+"/traininginfo/list?entry="+entry+"&search="+search,
    success:function(response){
      createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});
}


function createTable(resp){
    var t=document.getElementById("branch-admin");
   $("body #branch-admin").find("tr:gt(0)").remove();
    //}
    var rowCount=1;
    var data=resp.data;

    for(var i in data){

    var row=t.insertRow(rowCount);

    row.insertCell(0).innerHTML=data[i].name;
    row.insertCell(1).innerHTML=data[i].act;
    row.insertCell(2).innerHTML=data[i].subact;
    row.insertCell(3).innerHTML=data[i].organizer;
    row.insertCell(4).innerHTML=data[i].start_date;
    row.insertCell(5).innerHTML=data[i].end_date;

    row.insertCell(6).innerHTML="<a href='javascript:void(0)' onclick='editTrainingAll("+data[i].id+")' class='btn btn-xs btn-primary' ><i class='glyphicon glyphicon-edit'></i>Edit</a>";

        rowCount++;

   }
 callForPagination(resp);
   return true;
}


  </script>
<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>
