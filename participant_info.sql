-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2017 at 06:51 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pfmsp`
--

-- --------------------------------------------------------

--
-- Table structure for table `participant_info`
--

CREATE TABLE `participant_info` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `caste` varchar(100) NOT NULL,
  `age` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `ministry` varchar(2540) NOT NULL,
  `agency` varchar(250) NOT NULL,
  `district` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `training_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participant_info`
--

INSERT INTO `participant_info` (`id`, `name`, `gender`, `caste`, `age`, `title`, `level`, `ministry`, `agency`, `district`, `phone`, `email`, `training_id`) VALUES
(18, 'zsdf', '1', '1', '1', '1', '0', 'asdf', '', '1', 'sdf', 'asd', 12),
(19, 'zsdfg', '1', '1', '1', '1', '0', 'sdfg', '', '1', 'sdf', 'sdf', 12),
(20, 'sdfg', '1', '1', '1', '1', '0', 'asdf', '', '3', 'sdfg', 'sdf', 12),
(21, 'sdfg', '1', '1', '1', '1', '0', 'sdfg', '', '2', 'sd', 'sdf', 12),
(22, 'sdfg', '1', '1', '1', '3', '0', 'dfg', '', '1', 'ggf', 'fghm', 13),
(28, 'kk', 'male', 'Brahmin', '1', '1', '1', 'jj', 'iii', 'D49', 'juu', 'yyy', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `participant_info`
--
ALTER TABLE `participant_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_id` (`training_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `participant_info`
--
ALTER TABLE `participant_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `participant_info`
--
ALTER TABLE `participant_info`
  ADD CONSTRAINT `participant_info_ibfk_1` FOREIGN KEY (`training_id`) REFERENCES `training_info` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
