<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainingcat;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;
//repo test
class TrainingcatController extends Controller {

    public function index() {

    
      return view('training_category.index',['events'=>$this->getEvents()]);
     
    }
   
    public function getEvents(){
        return \App\Trainingtype::select('id','name_en')->get();
    }
  

    public function creates(Request $request){
      $coun = new Trainingcat();
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
        
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $event=$request->input("event_id",null);
         if($event==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Event Type is required."]);
        }
         $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        // $name_np=$request->input("name_np",null);
        //  if($name_np==""){
        //   return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        // }
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
      }
    }

   

    public function edits($id){
      $coun = Trainingcat::find($id,['id','event_id','name_en','name_np']);
      return $coun;

    }

    

    public function updates(Request $request,$id){
       $coun = Trainingcat::find($id);
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $event=$request->input("event_id",null);
         if($event==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Event Type is required."]);
        }
         $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        // $name_np=$request->input("name_np",null);
        //  if($name_np==""){
        //   return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        // }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update"]);
      }
    }

    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
    if($search==null){
       // return $train = DB::table('training_category')->paginate($entry,['*'],'page', $page );

      // $table=$this->getData($countries);

      $acts = DB::table('training_category')->select(['training_category.id','training_category.name_en','training_type.name_en as cname_en'])
        ->join('training_type','training_category.event_id','=','training_type.id')
        
        ->Paginate($entry,['*'],'page', $page );
        return $acts;

     }
     else{

       $acts = DB::table('training_category')->select(['training_category.id','training_category.name_en','training_type.name_en as cname_en'])
        ->join('training_type','training_category.event_id','=','training_type.id')
        ->where('name_en', 'LIKE', "%$search%")->paginate($entry,['*'],'page', $page );
       return $train;
     }

       


    }

   

    public function deletes($id){
        $coun = Trainingcat::find($id);
        try{
        $coun->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }
}
