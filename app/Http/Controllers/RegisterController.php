<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use DB;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;
use Mail;

class RegisterController extends Controller {

    public function index() {


      return view('login.register',['staff'=>$this->getStaff()]);

    }
    public function create(Request $request){
      $user = new Users();
   // $name=$request->input("fullname");
        $username=$request->input("username");
        $staff_id=$request->input("staff_id");
       $b=DB::table("users")->where('staff_id','=',$staff_id)->count();

      $a=DB::table("users")->where('username','=',$username)->count();

      if($b==1){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"* Staff is already registered."]);

      }

      if($a==1){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"* Username  already exist. Please try with new Username"]);

      }
      if($user->validate($request->all())){
         
 	     	$user->fill($request->all());
         
         $user->flag=0;
        
         $pass=$request->input("password");
          $hpass=Hash::make($pass);
          $user->password=$hpass;
        
          
        $user->save();
          return json_encode(['status'=>1,'title'=>"success",'text'=>"Thank you for Registering!!"]);
        
        }else{
           $pass=$request->input("password",null);
     $cpass=$request->input("password_confirmation",null);
     if($pass!=$cpass){
         return json_encode(['status'=>0,'title'=>"error",'text'=>"* Password donot match."]);
      
     }
     if($staff_id==""){
       return json_encode(['status'=>0,'title'=>"error",'text'=>"* Please select Staff."]);

     }
     if($username==""){
       return json_encode(['status'=>0,'title'=>"error",'text'=>"* Username is Required."]);

     }
     
      if(strlen($pass)<6){
       return json_encode(['status'=>0,'title'=>"error",'text'=>"* Password should be minimum 6 character."]);

     }
      
         return json_encode(['status'=>0,'title'=>"error",'text'=>"Failed to register"]);



            }
          }


public function getStaff(){
   return \App\Staff::select('id','name_en')->get();
}



  }
