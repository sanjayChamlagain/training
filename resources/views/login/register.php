<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<div  id="body" class="content-wrapper">
<?php  } ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Registration
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-12">

                  <!-- <div class="register-box-body"> -->
    <!-- <p class="login-box-msg">Create New User</p> -->

    <form class="form-horizontal" id="form" onsubmit="formSubmit(event)">      <div class="form-group has-feedback" style="width:500px">
         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
         <div class="box col-md-9">
             <div class="">

             </div>
             <div class="box-body">
               
                  <div class="form-group">
                                 <label for="code" class="col-sm-12 ">Staff<b style="color:red">*</b></label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="staff_id" name="staff_id">      
                                      <option id="select" value="">Select one..</option>
                                        
                                            <?php foreach($staff as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name_en;?></option>
                                                
                                            <?php endforeach;?>
                                                                 
                                  
                                                  <!-- <option value="yes" selected="selected">Yes</option> -->
                                                  <!-- <option value="no" >No</option> -->
                                                
                                    </select>
                                </div>
                            </div>



                
                        <div id="spouse" class="form-group" >
                     <label for="code" class="col-sm-12 ">Username<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="username" placeholder="Username" type="text" name="username" >
                     </div>
                 </div>

                 <div id="spouse" class="form-group" >
                     <label for="code" class="col-sm-12 ">Password<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="password" placeholder="Password" type="password" name="password" >
                     </div>
                 </div>
                  <div class="form-group">
                     <label for="code" class="col-sm-12 ">Confirm Password<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="password_confirmation" placeholder="Repeat password" type="password" name="password_confirmation" >
                     </div>
                 </div>

             </div>
             <div class="box-footer">
                 <button type="submit" id="submit"  class="btn btn-success ">Register</button>
                 <div class="pull-right">                   
                   <a href="<?php echo url('/');?>" class="btn btn-default btn-flat">Cancel</a>   
                 </div>
                 <!-- <button  type="reset"  class="btn btn-danger pull-right">Reset</button> -->
             </div>

     </div>

  </div>
  <!-- /.login-box-body -->
  </div>
  </form>

  </div>

        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
   var baseurl="<?php echo url('/');?>";   
   function formSubmit(e){    
    e.preventDefault();   
       $.ajax({   
           method:"POST",   
             url:baseurl+"/register/create",    
           data:$("#form").serialize(),   
           success:function(resp){    
            var a =JSON.parse(resp);    
            if(a.status==1){    
              $("#username").val("");
              $('#password').val("");   
            $('#password_confirmation').val("");  
            $("#staff_id").val("");  
              // setTimeout(function(){ window.location=baseurl;},3000);   
          }   
               toast(a);    
        },    
          fail:function(){    
          alert("failed");    
     }    
      });   
}
</script>

<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>
