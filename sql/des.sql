-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2017 at 08:03 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `des`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `name_en`, `name_np`) VALUES
(2, '977', 'Nepal', 'नेपाल'),
(21, 'adfadf', 'Maldives', 'Maldives'),
(28, 'slkjd', 'srilanka', 'srilanka'),
(34, '324', 'china', 'chinas'),
(35, 'adsf', 'Bhutan', 'Bhutan'),
(36, 'adf', 'usa', 'usa'),
(42, '877', 'india', 'india'),
(54, 'h3', 'bangladesh', 'banglades'),
(68, 'NC', 'Nepal', 'Nepali'),
(75, '33', 'chilli', 'chilli'),
(79, 'nb', 'bngggla', 'bnggggla'),
(81, '23', 'banglades', 'banglades'),
(83, 'udf', 'mexico', 'mexico'),
(84, 'nn', 'brazil', 'brazil'),
(86, 'nz', 'newzealand', 'newzealand'),
(87, 'm', 'nn', 'nn'),
(88, 'nb', 'mnmnmn', 'nmmnmn'),
(91, 'new', 'france', 'france'),
(93, 'nb', 'nbas', 'nbas'),
(94, 'mnb', 'djdksf', 'sjhfio'),
(95, 'ksd', 'asjhdio', 'laskndi'),
(96, 'odjfdskf', 'dknf', 'skdnfo'),
(97, 'jad', 'sdkfnl', 'sdkfn'),
(98, 'hj', 'jkdhf', 'sdjbfo'),
(99, 'dhfo', 'aoeoyr9', 'aoiierh'),
(100, 'sj', 'er', 'er'),
(101, 'new', 'saudi', 'saudi'),
(102, 'gg', 'nep', 'bb'),
(103, 'tt', 'nep', 'ff'),
(104, 'tt', 'nep', 'ff'),
(105, 's2', 'nep', 'nep'),
(106, 'hfo', 'dhroiew', 'kjebrkiur'),
(107, 'nb', 'pakistan', 'pakistan');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `state_id` int(11) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `code`, `state_id`, `name_en`, `name_np`) VALUES
(1, 'mn', 3, 'jhapa', 'jhapa'),
(2, 'mor', 3, 'morang', 'Morang'),
(3, 'tap', 6, 'taplejung', 'taplejung'),
(4, 'sag', 7, 'mustang', 'mustang'),
(5, 'sap', 1, 'saptari', 'saptari'),
(6, 'siraha', 1, 'siraha', 'siraha'),
(7, 'ilam', 6, 'ilam', 'ilam'),
(9, 'mh', 4, 'mg', 'mh'),
(10, 'g', 10, 'kolkata', 'kolkata'),
(11, 'sankhwasava', 6, 'sankhawasava', 'sankhawasava'),
(17, 'nm', 21, 'dist', 'dist'),
(18, 'jjd', 11, 'jhga', 'jhga');

-- --------------------------------------------------------

--
-- Table structure for table `local_gov`
--

CREATE TABLE `local_gov` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `state_id` int(11) NOT NULL,
  `local_gov_type_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `local_gov`
--

INSERT INTO `local_gov` (`id`, `code`, `state_id`, `local_gov_type_id`, `district_id`, `name_en`, `name_np`) VALUES
(1, 'siopoen', 6, 8, 3, 'Football', 'Football'),
(2, 'yu', 3, 2, 5, 'feed', 'feed'),
(4, 'tt', 7, 4, 4, 'tourisms', 'tourisms'),
(5, 'njn', 1, 4, 5, 'education and games.', 'education and games.'),
(6, 'jdj', 6, 2, 11, 'child health', 'ch'),
(7, 'osihad', 4, 5, 9, 'aijpo', 'lsid'),
(8, 'hoadf', 7, 4, 4, 'aosjfho', 'isdohfo'),
(10, 'poasjfp', 10, 9, 10, 'apofjr', 'eprip'),
(11, 'oiho', 6, 8, 3, 'aoppup', 'oieuwrpo'),
(12, 'pihp', 1, 9, 5, 'poiejp', 'epowur'),
(16, 'nnn', 3, 6, 1, 'nfornest', 'nfornest'),
(17, 'iof', 3, 2, 1, 'kaslnfl', 'dflfff'),
(19, 'nm', 21, 4, 17, 'transport', 'transport'),
(20, 'isdsh', 11, 5, 18, 'jssaoi', 'usdhfu'),
(21, 'cd', 1, 19, 6, 'online', 'online');

-- --------------------------------------------------------

--
-- Table structure for table `local_gov_type`
--

CREATE TABLE `local_gov_type` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `local_gov_type`
--

INSERT INTO `local_gov_type` (`id`, `code`, `name_en`, `name_np`) VALUES
(1, 'jshimm', 'Health', 'swasthya'),
(2, 'skjfif', 'children', 'balbalika'),
(4, 'euoiee', 'service', 'sewa'),
(5, 'code', 'oldage', 'old'),
(6, 'usdhf98e', 'oidshf9ie', 'iisdhf90ds'),
(8, 'ui', 'games', 'games'),
(9, 'sjN', 'women devlopment', 'womenN'),
(10, 'doshfoi', 'old', 'old'),
(12, 'ohhsifo;i', 'doishf9syyyy', 'oidhf0syfuuu'),
(13, 'kjshfoi', 'lksjfli', 'oidf'),
(14, 'nnnn', 'nnnn', 'nnnnn'),
(16, 'oidshf', 'oisddfy', 'oidfh'),
(17, 'oidshfo', 'sdofj', 'sdoijfpo'),
(18, 'lksajfl', 'idjfpo', 'oishfpo'),
(19, 'hi', 'news', 'news');

-- --------------------------------------------------------

--
-- Table structure for table `org_level`
--

CREATE TABLE `org_level` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org_level`
--

INSERT INTO `org_level` (`id`, `code`, `name_en`, `name_np`) VALUES
(7, 'jd', 'level2', 'level2'),
(8, 'l3', 'level3', 'level3'),
(9, 'jsfo', 'oidhfo', 'odoijfp'),
(10, 'l4', 'lvl4', 'lvl4'),
(14, 'poijf', 'sdf', 'dsfdsf'),
(15, 'sdfsd', 'sdfsdf', 'sfsds'),
(16, 'fdsf', 'dsfs', 'sdfs'),
(17, 'fsdf', 'dsf', 'sdf'),
(18, 'dsf', 'dsfs', 'dsfsdf'),
(20, 'sdf', 'sdfbb', 'sdfbb'),
(21, 'hd', 'siauhdoi', 'SJANDOHI'),
(22, 'LKIFDNO', 'skndofA', 'IOJSDOI'),
(23, 'IJEFDPO', 'OIEJFO', 'OIJFDDOP'),
(24, 'KDFOE', 'NFDIK', 'KNWDI'),
(25, 'AJNOID', 'SKNDFO', 'SKLND');

-- --------------------------------------------------------

--
-- Table structure for table `org_str`
--

CREATE TABLE `org_str` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `state_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `local_gov_id` int(11) NOT NULL,
  `org_type_id` int(11) NOT NULL,
  `org_level_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `path` varchar(50) DEFAULT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL,
  `latitude` varchar(200) DEFAULT NULL,
  `longitude` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org_str`
--

INSERT INTO `org_str` (`id`, `code`, `state_id`, `district_id`, `local_gov_id`, `org_type_id`, `org_level_id`, `parent_id`, `path`, `name_en`, `name_np`, `latitude`, `longitude`) VALUES
(52, '12', 1, 5, 2, 1, 15, 0, '52', 'a', 'a', '5678', '567'),
(53, '23', 3, 1, 16, 5, 15, 52, '52-53', 'b', 'b', 'fghjk', 'ghj'),
(54, 'f', 1, 5, 2, 5, 17, 55, '52-60-55-54', 'c', 'c', 'ffgh', 'yghj'),
(55, '77', 3, 1, 16, 1, 8, 60, '52-60-55', 'd', 'd', 'hhhh', 'hhhh'),
(58, 'y', 3, 1, 16, 5, 7, 0, '58', 'e', 'e', 'lhfios', 'djshfoi'),
(60, 'js', 1, 5, 2, 1, 10, 52, '52-60', 'r', 'r', 'asjdfhoi', 'ldnfoi'),
(61, 'mn', 7, 4, 4, 2, 8, 0, '61', 'parent', 'parent', '4567818', '788765'),
(62, 'mn', 4, 9, 7, 1, 8, 61, '61-62', 'child', 'child', '4533', '4543'),
(63, 'g child', 11, 18, 20, 2, 15, 62, '61-62-63', 'gchild', 'hdfio', 'isahido', 'oiiohdf'),
(65, 'hd', 4, 9, 7, 5, 10, 58, '58-65', 'iodshr', 'jshoi', 'sahiod', 'shdio'),
(66, 'hd', 1, 5, 2, 2, 17, 62, '61-62-66', 's', 's', 'fghdjDHJ', 'ghjdhff'),
(67, 'y', 1, 5, 2, 4, 9, 54, '52-60-55-54-67', 'z', 'z', 'ihe', 'DSUHFO'),
(68, 'hsdu', 3, 1, 16, 2, 8, 62, '61-62-68', 'oishdo', 'iheo', 'ishe', 'iahe');

-- --------------------------------------------------------

--
-- Table structure for table `org_type`
--

CREATE TABLE `org_type` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `org_type`
--

INSERT INTO `org_type` (`id`, `code`, `name_en`, `name_np`) VALUES
(1, 'hisduhfoi', 'health care', 'swasthya sewa'),
(2, 'hero', 'ngo', 'ngo'),
(4, 'ksd', 'ingo', 'ingo'),
(5, 'ihii', 'lipon', 'oiiypo'),
(6, 'klsldpo', 'saofhpos', 'osihfpof'),
(7, 'b', 'duwasa', 'duwasa'),
(9, 'ikfh', 'type4', 'type4'),
(10, 'kdshfk', 'sdlihfp', 'dsfhgou'),
(12, 'jfop', 'posdjf', 'ojdif'),
(13, 'lksdlf', 'oisdfj', 'ndklfl'),
(14, 'sihfo', 'sidfjpo', 'ojfp');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `code`, `country_id`, `name_en`, `name_np`) VALUES
(1, '987', 2, 'Madhes', 'Madhesi'),
(3, '786', 2, 'terai', 'tarai'),
(4, '567', 42, 'utar pradesh', 'utar pradesh'),
(6, 'nnd', 2, 'pahad', 'pahad'),
(7, 'him', 2, 'himal', 'himal'),
(10, 'jh', 42, 'gujurat', 'gujurat'),
(11, 'uii', 34, 'iugi', 'oiho'),
(12, 'ijpfdo', 36, 'ifpo', 'spofjo'),
(16, 'sdkfnl', 21, 'sjhsdi', 'dlkfjdd'),
(18, 'iho', 28, 'soidho', 'oshdo'),
(20, 'hh', 79, 'jjj', 'nnn'),
(21, 'nm', 91, 'state1', 'state1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `local_gov`
--
ALTER TABLE `local_gov`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_id` (`state_id`),
  ADD KEY `local_gov_type_id` (`local_gov_type_id`),
  ADD KEY `district_id` (`district_id`);

--
-- Indexes for table `local_gov_type`
--
ALTER TABLE `local_gov_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org_level`
--
ALTER TABLE `org_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org_str`
--
ALTER TABLE `org_str`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_id` (`state_id`),
  ADD KEY `district_id` (`district_id`),
  ADD KEY `local_gov_id` (`local_gov_id`),
  ADD KEY `org_type_id` (`org_type_id`),
  ADD KEY `org_level_id` (`org_level_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `org_type`
--
ALTER TABLE `org_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `local_gov`
--
ALTER TABLE `local_gov`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `local_gov_type`
--
ALTER TABLE `local_gov_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `org_level`
--
ALTER TABLE `org_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `org_str`
--
ALTER TABLE `org_str`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `org_type`
--
ALTER TABLE `org_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `district`
--
ALTER TABLE `district`
  ADD CONSTRAINT `district_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`);

--
-- Constraints for table `local_gov`
--
ALTER TABLE `local_gov`
  ADD CONSTRAINT `local_gov_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  ADD CONSTRAINT `local_gov_ibfk_2` FOREIGN KEY (`local_gov_type_id`) REFERENCES `local_gov_type` (`id`),
  ADD CONSTRAINT `local_gov_ibfk_3` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`);

--
-- Constraints for table `org_str`
--
ALTER TABLE `org_str`
  ADD CONSTRAINT `org_str_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  ADD CONSTRAINT `org_str_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`),
  ADD CONSTRAINT `org_str_ibfk_3` FOREIGN KEY (`org_type_id`) REFERENCES `org_type` (`id`),
  ADD CONSTRAINT `org_str_ibfk_4` FOREIGN KEY (`org_level_id`) REFERENCES `org_level` (`id`),
  ADD CONSTRAINT `org_str_ibfk_5` FOREIGN KEY (`local_gov_id`) REFERENCES `local_gov` (`id`);

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `state_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
