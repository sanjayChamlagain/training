

var rowCountStaff=1;
var rowCountResource=1;
var rowCountParticipant=1;
var staffArray=[];
var resourcePersonArray=[];
var participantArray=[];

function clearData(){
$("#participant_name").val("");
 $("#gender").val("");
  $("#caste").val("");
  $("#age").val("");
  $("#level").val("");
  $("#title").val("");
     
  $("#p_ministry").val("");
   
  $("#agency").val("");
  $("#participant_phone").val("");
  $("#participant_email").val("");
  $("#p_remarks").val("");
  $("#participant_district").val("");

}

function saveTrainingInfo(e){

e.preventDefault();

var id=$("#id_no").val();
var comp= $("#component").val();
 var name=$("#name").val();
 var org= $("#organized_for").val();

 var activity= $("#activity").val();
 var ir= $("#immediate_result").val();
   var trainType=$("#training_type").val();
           
  var desc=$("#description").val();
  var sdate=$("#start_date").val();
   var edate=$("#end_date").val();
 var venue=$("#venue").val();
var remarks=$("#remarks").val();
var target=$("#target").val();
 var achv= $("#achievement").val();

if(comp==""){
  alert("Component is required");
         document.getElementById('component').focus();
   return false;

}

else if(activity==""){
  alert("Activity is required");
         document.getElementById('activity').focus();
   return false;

}
else if(ir==""){
  alert("Subactivity is required");
         document.getElementById('immediate_result').focus();
   return false;

}
else if(trainType==""){
  alert("Event Type is required");
         document.getElementById('training_type').focus();
   return false;

}
else if(name==""){
  alert("Tranining Name is required");
         document.getElementById('name').focus();
   return false;

}
else if(venue==""){
  alert("Venue is required");
         document.getElementById('venue').focus();
   return false;

}
else if(org==""){
  alert("Organized For is required");
         document.getElementById('organized_for').focus();
   return false;

}
else if(desc==""){
  alert("Description is required");
         document.getElementById('description').focus();
   return false;

}
else if(sdate==""){
  alert("Start Date is required");
         document.getElementById('start_date').focus();
   return false;

}
else if(edate==""){
  alert("End Date is required");
         document.getElementById('end_date').focus();
   return false;

}

else if(remarks==""){
  alert("Remarks is required");
         document.getElementById('remarks').focus();
   return false;

}
else if(target==""){
  alert("Target is required");
         document.getElementById('target').focus();
   return false;

}
else if(achv==""){
  alert("Achievement is required");
         document.getElementById('achievement').focus();
   return false;

}


if(id==""){
 var Url=baseUrl+"/traininginfo/savetraininginfo";

}else{

  var Url=baseUrl+"/traininginfo/updatetraininginfo/"+id;
}
  
 $.ajax({
           method:"POST",
           url:Url,
            data:$("#basic_info_form").serialize(),
         
           success:function(resp){
            var a =JSON.parse(resp);
              toast(a);
              $("#id_now").val(a.id);
              $("#id_no").val();
              $("#name").val("");
              $("#organized_for").val("");
              $("#component").val("");
              $("#activity").val("");
              $("#immediate_result").val("");
              $("#training_type").val("");
              $("#training_category").val("");
               $("#description").val("");
               $("#start_date").val("");
               $("#end_date").val("");
               $("#venue").val("");
               $("#remarks").val("");
               $("#target").val("");
               $("#achievement").val("");

          $("#sadd").attr('disabled',false);
          $("#radd").attr('disabled',false);
          $("#padd").attr('disabled',false);


        },
          fail:function(){

          alert("failed");
     }
      });
}





function tableShowStaff(tableName){
    var check=  $("#id_edit").val();
   var name=$("#staff_name :selected").text();
  // alert(name);
   var staffId=$("#staff_name :selected").val();
  var respon=  $("#staff_responsibility").val();
  var remarks=$("#staff_remarks").val();
if(check==0){
 if(name==null || name=="Select one.." || respon==null){
   alert("All the fields are required");
   return true;
}
var staff={staffId:staffId,name:name,responsibility:respon,remarks:remarks};
}
 $("#"+tableName).show();
    var t=document.getElementById(tableName+"_show");
 var editRow=$('body #staff_hidden').val();
//alert(JSON.stringify(this.staffArray));
    if(editRow!=""){
  
    $('body #staff_hidden').val("");
      
       this.staffArray[editRow-1]=staff;
     t.deleteRow(editRow);
   var row=t.insertRow(editRow);
    row.insertCell(0).innerHTML=editRow;
 row.insertCell(1).innerHTML=name;
    row.insertCell(2).innerHTML=respon;
   row.insertCell(3).innerHTML=remarks;
   row.insertCell(4).innerHTML="<a href='javascript:void(0)'  data-toggle='modal' data-target='#modal-staff' onclick='editStaff("+editRow+","+staffId+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit' ></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteStaff("+editRow+","+staffId+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
    
   }
   else{
    if(check==1){
     $("#staff_table_show").find("tr:gt(0)").remove();
     rowCountStaff=1;       
  for (var i in this.staffArray){
var row=t.insertRow(this.rowCountStaff);
 row.insertCell(0).innerHTML=this.rowCountStaff;
 row.insertCell(1).innerHTML=this.staffArray[i].name;
    row.insertCell(2).innerHTML=this.staffArray[i].responsibility;
   row.insertCell(3).innerHTML=this.staffArray[i].remarks;
   row.insertCell(4).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-staff' onclick='editStaff("+this.rowCountStaff+","+this.staffArray[i].staffId+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteStaff("+this.rowCountStaff+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountStaff++;
}

  }else{

    
 this.staffArray[this.rowCountStaff-1]=staff;
var row=t.insertRow(this.rowCountStaff);
 row.insertCell(0).innerHTML=this.rowCountStaff;
 row.insertCell(1).innerHTML=name;
    row.insertCell(2).innerHTML=respon;
   row.insertCell(3).innerHTML=remarks;
   row.insertCell(4).innerHTML="<a href='javascript:void(0)' onclick='editStaff("+this.rowCountStaff+","+staffId+")' class='btn btn-xs btn-primary' data-toggle='modal' data-target='#modal-staff'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteStaff("+this.rowCountStaff+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountStaff++;
}
}
$("#staff_name").val("");
  $("#staff_responsibility").val("");
  $("#staff_remarks").val("");
  // callForPagination();
 
     return true;
}

 function saveStaffInfo(){
var datas=JSON.stringify(staffArray);
var id=$("#id_now").val();
if(id==""){

alert("First fill the Training Info or edit for existing training ");
}else{
 $.ajax({
           method:"POST",
           url:baseUrl+"/traininginfo/savestaff/"+id,
            data:{data:datas},
         headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

           success:function(resp){
            var a =JSON.parse(resp);
           toast(a);
              
        },
          fail:function(){

          alert("failed");
     }
      });
}
 }

function editStaff(row,id){
 $("#staff_hidden").val(row);
 var table=document.getElementById("staff_table_show");

var responsibility = table.rows[row].cells[2].innerHTML;
var remarks = table.rows[row].cells[3].innerHTML;
$("#staff_name").val(id);
$("#staff_responsibility").val(responsibility);
  $("#staff_remarks").val(remarks);
  
}

 function deleteStaff(row){
      var conf=confirm("Are you sure you want to delete?");
   if(conf){
this.staffArray.splice(row-1,1);
    $("#staff_table_show").find("tr:gt(0)").remove();
   // var datas=JSON.stringify(staffArray);
    //alert(datas);
    var data=this.staffArray;
    this.rowCountStaff=1;
    if(data.length>0){
     var t=document.getElementById("staff_table_show");
   
    for(var i in data){
    var row=t.insertRow(this.rowCountStaff);
    //alert("yes");
    row.insertCell(0).innerHTML=this.rowCountStaff;

    row.insertCell(1).innerHTML=data[i].name;

    row.insertCell(2).innerHTML=data[i].responsibility;
    row.insertCell(3).innerHTML=data[i].remarks;
    row.insertCell(4).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-staff' onclick='editStaff("+this.rowCountStaff+","+this.staffArray[this.rowCountStaff-1].staffId+")' class='btn btn-xs btn-primary' ><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteStaff("+this.rowCountStaff+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountStaff++;
 }}else{

 }
}
}

function tableShowResource(tableName){
    var check=$("#id_edit").val();
   var name=$("#resource_person_name :selected").text();
   var rid=$("#resource_person_name :selected").val();
  var jobDes=  $("#job_description").val();
  var remarks=$("#resource_person_remarks").val();
  if(check==0)
  {
if(name==null || name=="Select one.." || jobDes==null){
  alert("All the fields are required");
  return true;
}
}
if(check!=1)
{
var rPerson={rPersonId:rid,name:name,jobDes:jobDes,remarks:remarks};
}
 $("#"+tableName).show();
    var t=document.getElementById(tableName+"_show");
 var editRow=$("#resource_hidden").val();
  

    if(editRow!=""){
    $("#resource_hidden").val("");
      
       this.resourcePersonArray[editRow-1]=rPerson;
     t.deleteRow(editRow);
   var row=t.insertRow(editRow);
    row.insertCell(0).innerHTML=editRow;
 row.insertCell(1).innerHTML=name;
    row.insertCell(2).innerHTML=jobDes;
   row.insertCell(3).innerHTML=remarks;
   row.insertCell(4).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-resource'  onclick='editResourcePerson("+editRow+","+rid+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteResourcePerson("+editRow+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
    
   }
   else{
    if(check==1){
     $("#resource_person_table_show").find("tr:gt(0)").remove();
     rowCountResource=1;       
  for (var i in this.resourcePersonArray){
var row=t.insertRow(this.rowCountResource);
 row.insertCell(0).innerHTML=this.rowCountResource;
 row.insertCell(1).innerHTML=this.resourcePersonArray[i].name;
    row.insertCell(2).innerHTML=this.resourcePersonArray[i].jobDes;
   row.insertCell(3).innerHTML=this.resourcePersonArray[i].remarks;
   row.insertCell(4).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-resource' onclick='editResourcePerson("+this.rowCountResource+","+this.resourcePersonArray[i].rPersonId+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteResourcePerson("+this.rowCountResource+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountResource++;
}

  }
   else{

 this.resourcePersonArray[this.rowCountResource-1]=rPerson;
var row=t.insertRow(this.rowCountResource);
 row.insertCell(0).innerHTML=this.rowCountResource;
 row.insertCell(1).innerHTML=name;
    row.insertCell(2).innerHTML=jobDes;
   row.insertCell(3).innerHTML=remarks;
   row.insertCell(4).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-resource' onclick='editResourcePerson("+this.rowCountResource+","+rid+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteResourcePerson("+this.rowCountResource+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountResource++;
}
}
$("#resource_person_name").val("");
  $("#job_description").val("");
  $("#resource_person_remarks").val("");
  // callForPagination();
     return true;
}

 function saveResourcePersonInfo(e){
    e.preventDefault();
var datas=JSON.stringify(resourcePersonArray);
var id=$("#id_now").val();
if(id==""){
alert("First fill the Training Info or edit for existing training ");
}else{
 $.ajax({
           method:"POST",
           url:baseUrl+"/traininginfo/saveresourceperson/"+id,
            data:{data:datas},
         headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

           success:function(resp){
            var a =JSON.parse(resp);
           toast(a);
              
        },
          fail:function(){

          alert("failed");
     }
      });
}
 }

function editResourcePerson(row,id){
 $("#resource_hidden").val(row);
 var table=document.getElementById("resource_person_table_show");

var jobDes = table.rows[row].cells[2].innerHTML;
var remarks = table.rows[row].cells[3].innerHTML;
$("#resource_person_name").val(id);
$("#job_description").val(jobDes);
  $("#resource_person_remarks").val(remarks);
  
}

 function deleteResourcePerson(row,id){
      var conf=confirm("Are you sure you want to delete?");
   if(conf){
   
this.resourcePersonArray.splice(row-1,1);
    $("#resource_person_table_show").find("tr:gt(0)").remove();
    //var datas=JSON.stringify(staffArray);
    //alert(datas);
    var data=this.resourcePersonArray;
    this.rowCountResource=1;
    if(data.length>0){
     var t=document.getElementById("resource_person_table_show");
   
    for(var i in data){
    var row=t.insertRow(this.rowCountResource);
    //alert("yes");
    row.insertCell(0).innerHTML=this.rowCountResource;
    row.insertCell(1).innerHTML=data[i].name;
    row.insertCell(2).innerHTML=data[i].jobDes;
    row.insertCell(3).innerHTML=data[i].remarks;
    row.insertCell(4).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-resource' onclick='editResourcePerson("+this.rowCountResource+","+this.resourcePersonArray[this.rowCountResource-1].rPersonId+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteResourcePerson("+this.rowCountResource+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountResource++;
 }}else{

 }
}
}

function retValue(vry,name,req){
var a=document.getElementById(name);
if(vry==0){
for(var i=1; i<a.length;i++)
{

if(req==a.options[i].text){

  return a.options[i].value;
}
}
}else{

  for(var i=1; i<a.length;i++)
{

if(req==a.options[i].value){

  return a.options[i].text;
}


}
}
return true;
} 
function tableShowParticipant(tableName){
  
  var check=$("#id_edit").val();
    var name=$("#participant_name").val();
    var gender=$("#gender :selected").val();
    var caste=$("#caste :selected").val();
    var age=$("#age :selected").val();
    var level=$("#level :selected").val();
   
    var title=$("#title :selected").val();
     
    var ministry=$("#p_ministry :selected").val();
   
    var agency=$("#agency").val();
    var phone=$("#participant_phone").val();
    var email=$("#participant_email").val();
    var remarks=$("#p_remarks").val();
    var district=$("#participant_district :selected").val();
   

    if(check==0){
    if(name=="")
    {
        alert("Your name is required");
        document.getElementById('participant_name').focus();
   return false;

    }
    else if(gender=="")
    {
        alert("Your gender is required");
        document.getElementById('gender').focus();
        return false;
    }
    else if(caste=="")
    {
        alert("Your caste is required");
        document.getElementById('caste').focus();      

       return false;

    }
    else if(age=="")
    {
        alert("Your age is required");
        document.getElementById('age').focus();
     return false;
    }
    else if(level=="")
    {
        alert("level field is required");
        document.getElementById('level').focus();
     return false;
    
    }
    else if(title=="")
    {
        alert("title field is required");
        document.getElementById('title').focus();
     return false;
    
    }
    
    else if(ministry=="")
    {
        alert("MInistry is required");
        document.getElementById('ministry').focus();
     return false;
    
    }
    else if(agency=="")
    {
        alert("Agency field is required");
        document.getElementById('agency').focus();
     return false;
    
    }
    
    else if(district=="")
    {
        alert("your district is required");
        document.getElementById('participant_district').focus();
     return false;
    
    }
    else if(phone=="")
    {
        alert("Your phone number is required");
        document.getElementById('participant_phone').focus();
     return false;
    
    }
    else if(email=="")
    {
        alert("Your email is required");
        document.getElementById('participant_email').focus();
     return false;
    
    }
    
   
}
    if(check!=1)
    {
  var participant={name:name,gender:gender,caste:caste,age:age,level:level,title:title,ministry:ministry,agency:agency,district:district,phone:phone,email:email,remarks:remarks};
     
    }
 $("#"+tableName).show();
    var t=document.getElementById(tableName+"_show");
  // $("body #org-table").find("tr:gt(0)").remove();
  var editRow=$("#participant_hidden").val();
  if(editRow!="")
  {
    $("#participant_hidden").val("");
    this.participantArray[editRow-1]=participant;
    t.deleteRow(editRow);
  
    var row=t.insertRow(editRow);

    row.insertCell(0).innerHTML=editRow;
    row.insertCell(1).innerHTML=name;
    row.insertCell(2).innerHTML=$("#gender option:selected").text();
    row.insertCell(3).innerHTML=$("#caste option:selected").text();
    row.insertCell(4).innerHTML=$("#age option:selected").text();
    row.insertCell(5).innerHTML=$("#level option:selected").text();
    row.insertCell(6).innerHTML=$("#title option:selected").text();
    
    row.insertCell(7).innerHTML=$("#p_ministry option:selected").text();
    row.insertCell(8).innerHTML=agency;
    row.insertCell(9).innerHTML=$("#participant_district option:selected").text();
     row.insertCell(10).innerHTML=phone;
    row.insertCell(11).innerHTML=email;
  row.insertCell(12).innerHTML=remarks;
 row.insertCell(13).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-participant'  onclick='editParticipant(event,"+editRow+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteParticipant("+editRow+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
      
  // callForPagination();
     //return true;
 }
 else
 {

    if(check==1)
    {
     //   alert(participantArray);
        $("#participant_table_show").find("tr:gt(0)").remove();


     rowCountParticipant=1;       
  for (var i in this.participantArray)
  {
    var row=t.insertRow(this.rowCountParticipant);
    row.insertCell(0).innerHTML=this.rowCountParticipant;
    row.insertCell(1).innerHTML=this.participantArray[i].name;
    row.insertCell(2).innerHTML=this.participantArray[i].gender;
    row.insertCell(3).innerHTML=this.participantArray[i].caste;
    row.insertCell(4).innerHTML=this.participantArray[i].age;
    row.insertCell(5).innerHTML=this.participantArray[i].title;
   this.participantArray[i].title=retValue(0,'title',this.participantArray[i].title);
       row.insertCell(6).innerHTML=this.participantArray[i].level;
     row.insertCell(7).innerHTML=this.participantArray[i].ministry;
     this.participantArray[i].ministry=retValue(0,'p_ministry',this.participantArray[i].ministry);
 
   // this.participantArray[i].ministry=$("#p_ministry").find('option[text="this.participantArray[i].ministry"]').val();
    row.insertCell(8).innerHTML=this.participantArray[i].agency;
    row.insertCell(9).innerHTML=this.participantArray[i].district;
      this.participantArray[i].district=retValue(0,'participant_district',this.participantArray[i].district);
 
       row.insertCell(10).innerHTML=this.participantArray[i].phone;
   //this.participantArray[i].district=$("#participnt_district").find('option[text="this.participantArray[i].district"]').val();  row.insertCell(10).innerHTML=this.participantArray[i].phone;
    row.insertCell(11).innerHTML=this.participantArray[i].email;
    row.insertCell(12).innerHTML=this.participantArray[i].remarks;
       row.insertCell(13).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-participant' onclick='editParticipant(event,"+this.rowCountParticipant+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteParticipant("+this.rowCountParticipant+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";

       this.rowCountParticipant++;
       //alert(row);
}

  }
  else
  {
     this.participantArray[this.rowCountParticipant-1]=participant;
var row=t.insertRow(this.rowCountParticipant);
    row.insertCell(0).innerHTML=this.rowCountParticipant;
    row.insertCell(1).innerHTML=name;
    row.insertCell(2).innerHTML=gender;
    row.insertCell(3).innerHTML=caste;
    row.insertCell(4).innerHTML=age;
    row.insertCell(5).innerHTML=$("#level option:selected").text();
    row.insertCell(6).innerHTML=$("#title option:selected").text();
    row.insertCell(7).innerHTML=$("#p_ministry option:selected").text();
    row.insertCell(8).innerHTML=agency;


    row.insertCell(9).innerHTML=$("#participant_district option:selected").text();
    row.insertCell(10).innerHTML=phone;
    row.insertCell(11).innerHTML=email;
    row.insertCell(12).innerHTML=remarks;
   row.insertCell(13).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-participant' onclick='editParticipant(event,"+this.rowCountParticipant+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteParticipant("+this.rowCountParticipant+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountParticipant++;
 }
}
  $("#participant_name").val("");
  $("#gender").val("");
  $("#caste").val("");
  $("#age").val("");
  $("#level").val("");
  $("#title").val("");
  $("#ministry").val("");
  $("#agency").val("");
  $("#participant_district").val("");
  $("#participant_phone").val("");
  $("#participant_email").val("");
  $("#p_remarks").val("");
 
}





function saveParticipantInfo(){
   //e.preventDefault();
var datas=JSON.stringify(participantArray);
var id=$("#id_now").val();
if(id==""){
alert("First fill the Training Info or edit for existing training ");
}else{
 $.ajax({
           method:"POST",
           url:baseUrl+"/traininginfo/saveparticipant/"+id,
            data:{data:datas},
         headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

           success:function(resp){
            var a =JSON.parse(resp);
           toast(a);
              
        },
          fail:function(){

          alert("failed");
     }
      });
}

}


 function editParticipant(e,row){
  
  e.preventDefault();
  
 $("#participant_hidden").val(row);
 var table=document.getElementById("participant_table_show");
var pName = table.rows[row].cells[1].innerHTML;

var pGender = table.rows[row].cells[2].innerHTML;
//alert(pGender);
var pCaste = table.rows[row].cells[3].innerHTML;;
//alert(pCaste);
var pAge = table.rows[row].cells[4].innerHTML;
var pTitle = this.participantArray[row-1].title;
var pLevel = table.rows[row].cells[6].innerHTML;
var pMinistry = this.participantArray[row-1].ministry;
var pAgency = table.rows[row].cells[8].innerHTML;

//alert(pOrganisation);
var pDistrict =this.participantArray[row-1].district;
var pPhone = table.rows[row].cells[10].innerHTML;
var pEmail = table.rows[row].cells[11].innerHTML;
var pRemarks= table.rows[row].cells[12].innerHTML;

$("#participant_name").val(pName).change();
  $("#participant_phone").val(pPhone).change();
  $("#participant_email").val(pEmail).change();
  $("#agency").val(pAgency).change();
  $("#caste").val(pCaste).change();
  $("#gender").val(pGender).change();
  $("#age").val(pAge).change();
  $("#level").val(pLevel).change();

  $("#title").val(pTitle).change();
  $("#p_ministry").val(pMinistry).change();
  $("#p_remarks").val(pRemarks);
  $("#participant_district").val(pDistrict).change();

}


 function deleteParticipant(row){
     var conf=confirm("Are you sure you want to delete?");
   if(conf){
   
this.participantArray.splice(row-1,1);

    $("#participant_table_show").find("tr:gt(0)").remove();
    //var datas=JSON.stringify(staffArray);
    //alert(datas);
    var data=this.participantArray;
    this.rowCountParticipant=1;
    if(data.length>0){
     var t=document.getElementById("participant_table_show");
   
    for(var i in data){
    var row=t.insertRow(this.rowCountParticipant);
    //alert("yes");
    row.insertCell(0).innerHTML=this.rowCountParticipant;
    row.insertCell(1).innerHTML=data[i].name;
    row.insertCell(2).innerHTML=data[i].gender;
    row.insertCell(3).innerHTML=data[i].caste;
    row.insertCell(4).innerHTML=data[i].age;
    row.insertCell(5).innerHTML=retValue(1,'title',this.participantArray[i].title);
      row.insertCell(6).innerHTML=data[i].level;
    row.insertCell(7).innerHTML=retValue(1,'p_ministry',this.participantArray[i].ministry);
  row.insertCell(8).innerHTML=data[i].agency;

    row.insertCell(9).innerHTML=retValue(1,'participant_district',this.participantArray[i].district);
   row.insertCell(10).innerHTML=data[i].phone;
    row.insertCell(11).innerHTML=data[i].email;
    row.insertCell(12).innerHTML=data[i].remarks;
    row.insertCell(13).innerHTML="<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-participant' onclick='editParticipant(event,"+this.rowCountParticipant+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='deleteParticipant("+this.rowCountParticipant+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
       this.rowCountParticipant++;
 }

 }
}
 else{
  return false;
 }
}