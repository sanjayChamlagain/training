<?php

namespace App;

class State extends BaseModel
{
    protected $table = 'state';
    protected $fillable = ['code', 'name_en', 'name_np','country_id'];
    protected $rules = [
        'code' => 'string',
        'name_en' => 'string|required',
        'name_np' => 'string',
        'country_id'=>'integer|required'
    ];
}
