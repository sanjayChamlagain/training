-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2017 at 08:24 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pfmsp`
--

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`) VALUES
(2, 'Achham'),
(3, 'Baglung'),
(4, 'Arghakhachi'),
(5, 'Baitadi'),
(6, 'Bajhang'),
(7, 'Bajura'),
(8, 'Banke'),
(9, 'Bara'),
(10, 'Bardiya'),
(11, 'Bhaktapur'),
(12, 'Bhojpur'),
(13, 'Chitawan'),
(14, 'Dadeldhura'),
(15, 'Dailekh'),
(16, 'Dang'),
(17, 'Darchula'),
(18, 'Dhading'),
(19, 'Dhankuta'),
(20, 'Dhanusha'),
(21, 'Dolakha'),
(22, 'Dolpa'),
(23, 'Doti'),
(24, 'Gorkha'),
(25, 'Gulmi'),
(26, 'Humla'),
(27, 'Ilam'),
(28, 'Jajarkot'),
(29, 'Jhapa'),
(30, 'Jumla'),
(31, 'Kailali'),
(32, 'Kalikot'),
(33, 'Kanchanpur'),
(34, 'Kapilvastu'),
(35, 'Kaski'),
(36, 'Kathmandu'),
(37, 'Kavrepalanchowk'),
(38, 'Khotang'),
(39, 'Lalitpur'),
(40, 'Lamjung'),
(41, 'Mahottari'),
(42, 'Makawanpur'),
(43, 'Manang'),
(44, 'Morang'),
(45, 'Mugu'),
(46, 'Mustang'),
(47, 'Myagdi'),
(48, 'Nawalparasi'),
(49, 'Nuwakot'),
(50, 'Okhaldhunga'),
(51, 'Palpa'),
(52, 'Panchthar'),
(53, 'Parbat'),
(54, 'Parsa'),
(55, 'Pyuthan'),
(56, 'Ramechhap'),
(57, 'Rasuwa'),
(58, 'Rautahat'),
(59, 'Rolpa'),
(60, 'Rukum'),
(61, 'Rupandehi'),
(62, 'Salyan'),
(63, 'Sankhuwasava'),
(64, 'Saptari'),
(65, 'Sarlahi'),
(66, 'Sidhupalchowk'),
(67, 'Sindhuli'),
(68, 'Siraha'),
(69, 'Solukhumbu'),
(70, 'Sunsari'),
(71, 'Surkhet'),
(72, 'Syangja'),
(73, 'Tanahun'),
(74, 'Taplejung'),
(75, 'Terhathum'),
(76, 'Udayapur');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
