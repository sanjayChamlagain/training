<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantInfo extends Model
{
    public $timestamps=false;
  protected $table = 'participant_info';
    protected $fillable = ['name','gender','caste','age','title','level','ministry','agency','district','phone','email','remarks'];
    protected $rules = [
        'name' => 'string',
        'gender' => 'string','caste' => 'integer','age' => 'integer','title' => 'integer','level' => 'integer','ministry' => 'string','agency' => 'string','district' => 'string','phone' => 'string','email' => 'string',
   'remarks'=>'string' ];
}