<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 use App\Designation;
 use App\Staff;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class StaffController extends Controller {

    public function index() {
      // return view('organization.index');
        //$countries = \App\Country::select('id','name_en')->get();
        return view('staff.index',['orgs'=>$this->getOrgs(),'desg'=>$this->getDesg()]);
    }

    
    public function creates(Request $request){
      $staff = new Staff();
      if($staff->validate($request->all())){
          $staff->fill($request->all());
          $staff->save();
         
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $gender=$request->input("gender",null);
         if($gender==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Gender is required."]);
        }
        
        $org=$request->input("organization_id",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $desg=$request->input("designation_id",null);
         if($desg==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Designation is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to save data"]);
      }
    }

    public function edits($id){
        $staff = Staff::find($id,['id','staff_id','name_en','name_np','gender','dob','designation_id','phone','mobile','email','organization_id','approved','disabled']);
        return $staff;
        //return view('Organization.index',['update'=>$Organization,'countries'=>$this->getCountries()]);
    }




    
    public function updates(Request $request,$id){
       $Organization = Staff::find($id);
      if($Organization->validate($request->all())){
          $Organization->fill($request->all());
          $Organization->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $gender=$request->input("gender",null);
         if($gender==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Gender is required."]);
        }
        
        $org=$request->input("organization_id",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $desg=$request->input("designation_id",null);
         if($desg==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Designation is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update data"]);
      }
    }

    
    public function lists(Request $request) {
     $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
      if($search==null){
        $acts = DB::table('staff_info')->select(['staff_info.id', 'staff_info.name_en','staff_info.phone', 'designation.name as cname_en','organization.name_en as oname_en'])
        ->join('designation','staff_info.designation_id','=','designation.id')
        ->join('organization','staff_info.organization_id','=','organization.id')
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
      else{

        $acts = DB::table('staff_info')->select(['staff_info.id', 'staff_info.name_en','staff_info.phone', 'designation.name as cname_en','organization.name_en as oname_en'])
        ->join('designation','staff_info.designation_id','=','designation.id')
        ->join('organization','staff_info.organization_id','=','organization.id')
        ->where('staff_info.name_en', 'LIKE', "%$search%")
         
         ->orwhere('designation.name','LIKE',"%$search%")
         ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }

    }

   
    public function deletes($id){
        $Organization = Staff::find($id);
        try{
        $Organization->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }
      catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }

    public function getOrgs(){
        return \App\Organization::select('id','name_en')->get();
    }

 public function getDesg(){
        return \App\Designation::select('id','name')->get();
    }
	
}
