<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class ActivityController extends Controller {

    public function index() {
       // return view('project_activity.index');
        //$countries = \App\Country::select('id','name_en')->get();
        return view('project_activity.index',['comp'=>$this->getComps()]);
    }

    
    public function creates(Request $request){
      $Org = new Activity();
      if($Org->validate($request->all())){
          $Org->fill($request->all());
          $Org->save();
         
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $component_id=$request->input("component_id",null);
         if($component_id==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Component is required."]);
        }
        $act=$request->input("activity",null);
        if($act==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Activity is required."]);

        }
         $desc=$request->input("description",null);
        if($desc==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Description is required."]);

        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to save data"]);
      }
    }

    
    public function edits($id){
        $Activity = Activity::find($id,['id','component_id','activity','description']);
        return $Activity;
        //return view('Activity.index',['update'=>$Activity,'countries'=>$this->getCountries()]);
    }

    
    public function updates(Request $request,$id){
       $Activity = Activity::find($id);
      if($Activity->validate($request->all())){
          $Activity->fill($request->all());
          $Activity->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $component_id=$request->input("component_id",null);
         if($component_id==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Component is required."]);
        }
        $act=$request->input("activity",null);
        if($act==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Activity is required."]);

        }
         $desc=$request->input("description",null);
        if($desc==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Description is required."]);

        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update data"]);
      }
    }



    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
      if($search==null){
        $acts = DB::table('project_activity')->select(['project_activity.id', 'project_activity.activity', 'project_component.name as cname_en'])
        ->join('project_component','project_activity.component_id','=','project_component.id')
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
      else{

        $acts = DB::table('project_activity')->select(['project_activity.id', 'project_activity.activity', 'project_component.name as cname_en'])
        ->join('project_component','project_activity.component_id','=','project_component.id')
        ->where('project_activity.activity', 'LIKE', "%$search%")
         
         ->orwhere('project_component.name','LIKE',"%$search%")
         ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
    }

   
    public function deletes($id){
        $Activity = Activity::find($id);
        try{
        $Activity->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }
      catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }

    public function getComps(){
        return \App\Project::select('id','name')->get();
    }


	
}
