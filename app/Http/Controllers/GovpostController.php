<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Govpost;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;
//repo test
class GovpostController extends Controller {

    public function index() {

    
      return view('gov_post.index');
    
    }
   

  

    public function creates(Request $request){
      $coun = new Govpost();
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
        
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{

        
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $lvl=$request->input("level",null);
         if($lvl==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Level is required."]);
        }
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
      }
    }

   

    public function edits($id){
      $coun = Govpost::find($id,['id','level','name_en','name_np','approved','disabled']);
      return $coun;

    }

    

    public function updates(Request $request,$id){
       $coun = Govpost::find($id);
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
         
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $lvl=$request->input("level",null);
         if($lvl==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Level is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update"]);
      }
    }

    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
    if($search==null){
       return $countries = DB::table('gov_post')->paginate($entry,['*'],'page', $page );

      // $table=$this->getData($countries);
     }
     else{

       $countries=DB::table('gov_post')->where('name_en', 'LIKE', "%$search%")->orwhere('name_np','LIKE',"%$search%")->paginate($entry,['*'],'page', $page );
       return $countries;
     }

       // return $table.$countries->links();


    //    return Datatables::of($countries)->addColumn('action', function ($countries) {
      //          return '<a  href="javascript:void(0)" onClick="edits('.$countries->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
                  //      '&nbsp;&nbsp;<a href="javascript:void(0)" onClick="deletes('.$countries->id.')" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
      //      })->make();
    }

   

    public function deletes($id){
        $coun = Govpost::find($id);
        try{
        $coun->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }
}
