<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourcePersonInfo extends BaseModel
{
    public $timestamps=false;
  protected $table = 'resource_person_info';
    protected $fillable = ['rPersonId','jobDes','remarks'];
    protected $rules = [
        'rPersonId' => 'integer',
        'jobDes' => 'string',
        'remarks' => 'string',
    ];
}