<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Govtype;
use App\Govorg;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class GovorgController extends Controller {

    public function index() {
      // return view('Govorg.index');
        //$countries = \App\Country::select('id','name_en')->get();
        return view('gov_org.index',['orgs'=>$this->getOrgs(),'pars'=>$this->getPars()]);
    }

    
    public function creates(Request $request){
      $Org = new Govorg();
      if($Org->validate($request->all())){
          $Org->fill($request->all());
          $Org->save();
         
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $code=$request->input("code",null);
         if($code==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Code is required."]);
        }
        $org=$request->input("orgtype_id",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $dis=$request->input("district",null);
         if($dis==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* District is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to save data"]);
      }
    }

    
    public function edits($id){
        $Govorg = Govorg::find($id,['id','parent_org','code','orgtype_id','name_en','name_np','district','postal','phone','fax','email','website','approved','disabled']);
        return $Govorg;
        //return view('Govorg.index',['update'=>$Govorg,'countries'=>$this->getCountries()]);
    }

    
    public function updates(Request $request,$id){
       $Govorg = Govorg::find($id);
      if($Govorg->validate($request->all())){
          $Govorg->fill($request->all());
          $Govorg->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $code=$request->input("code",null);
         if($code==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Code is required."]);
        }
        $org=$request->input("orgtype_id",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $dis=$request->input("district",null);
         if($dis==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* District is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update data"]);
      }
    }



    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
      if($search==null){
        $acts = DB::table('gov_org')->select(['gov_org.id', 'gov_org.parent_org', 'gov_org.name_en','gov_type.name_en as cname_en'])
        ->join('gov_type','gov_org.orgtype_id','=','gov_type.id')
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
      else{

      $acts = DB::table('gov_org')->select(['gov_org.id', 'gov_org.parent_org', 'gov_org.name_en','gov_type.name_en as cname_en'])
        ->join('gov_type','gov_org.orgtype_id','=','gov_type.id')
        ->where('gov_org.name_en', 'LIKE', "%$search%")
         
         // ->orwhere('gov_type.name_en','LIKE',"%$search%")
         ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }




        // return Datatables::of($Govorgs)->addColumn('action', function ($Govorgs) {
        //         return '<a href="javascript:void(0)" onClick="stedit('.$Govorgs->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
        //                 '&nbsp;&nbsp;<a href="javascript:void(0)" class="btn btn-xs btn-danger" onClick="stdelete('.$Govorgs->id.')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
        //     })->make();
    }

    public function delete($id){
        $Govorg = Govorg::find($id);
        $Govorg->delete();
        return redirect('/Govorg')->with('msg',json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Deleted"]));
    }
    public function deletes($id){
        $Govorg = Govorg::find($id);
        try{
        $Govorg->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }
      catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }

    public function getOrgs(){
        return \App\Govtype::select('id','name_en')->get();
    }


	public function getPars(){
        return \App\Govorg::select('id','name_en')->get();
    }
}
