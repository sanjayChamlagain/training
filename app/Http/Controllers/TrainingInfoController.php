<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;
use App\TrainingInfo;
use App\StaffInfo;
use App\ResourcePersonInfo;
use App\ParticipantInfo;
use Illuminate\Support\Facades; 
class TrainingInfoController extends Controller {
   
   public function index(){



return view('traininginfo',["organisedFor"=>$this->getGovOrgName(),'ir'=>$this->getSubactivity(),"component"=>$this->getProjectComponent(),"activity"=>$this->getProjectActivity(),"trainingType"=>$this->getProjectType(),"staff"=>$this->getStaff(),"resourcePerson"=>$this->getResourcePerson(),"desg"=>$this->getDesignation(),"dis"=>$this->getDistrict(),"min"=>$this->getMinistry()]);


   }

    public function getActivity($id){
        return \App\Activity::select('id','activity')->where('component_id','=',$id)->get();

   }

   public function getSubact($id){
    // return $train = DB::table('project_subactivity')->select('id','subactivity')->where('activity_id','=',$id)->get();
 return \App\SubActivity::select('id','subactivity')->where('activity_id','=',$id)->get();


  }

   public function getDistrict(){

return \App\District::select('id','name')->get();
   }

   public function getMinistry(){

return \App\Govorg::select('id','name_en')->get();
   }

public function getSubactivity(){

return \App\SubActivity::select('id','subactivity')->get();
   }

   public function getDesignation(){

return \App\Designation::select('id','name')->get();
   }

   public function getResourcePerson(){

return \App\Resourceperson::select('id','name_en')->get();
   }
   
   public function getStaff(){

return \App\Staff::select('id','name_en')->get();
   }
   

   public function getGovOrgName(){

return \App\Govorg::select('id','name_en')->get();
   }
   
public function getProjectComponent(){

return \App\Project::select('id','name')->get();
   }
   public function getProjectActivity(){
    return \App\Activity::select('id','activity')->get();

   }
   public function getProjectType(){
return \App\Trainingtype::select('id','name_en')->get();

   }
   public function getTrainingCategory(){

return \App\Trainingcat::select('id','name_en')->get();
   }
//    public function getTrainingTopic(){
// return \App\Trainingtopic::select('id','name_en')->get();

//    }


   public function saveTrainingInfo(Request $request)
   {

       $train = new TrainingInfo();
      if($train->validate($request->all())){
          $train->fill($request->all());

          $train->save();
        
          return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Saved","id"=>$train->id]);
      }else{
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
      }
    }


    public function updateTrainingInfo(Request $request,$id){
       $coun = TrainingInfo::find($id);
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
          return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Updated"]);
      }else{

return json_encode(['status'=>0,'title'=>"error",'text'=>"Data not Updated"]);
           }
    }


   
public function saveStaff($id,Request $request){
  $count=0;
$a=$request->input("data");
$data = json_decode($a, TRUE);
DB::table("staff_information")->where('training_id','=',$id)->delete();

foreach($data as $d){

  $train = new StaffInfo();
      if($train->validate($d)){
          $train->fill($d);
          $train->training_id=$id;
          $train->save();
        $count++;
              }else{
                }
              }
   return json_encode(['status'=>1,'title'=>"Success",'text'=>$count." Data Successfully Saved"]);
  
    }




public function saveResourcePerson($id,Request $request){

$count=0;
$a=$request->input("data");
$data = json_decode($a, TRUE);
DB::table("resource_person_info")->where('training_id','=',$id)->delete();

foreach($data as $d){
  $train = new ResourcePersonInfo();
      if($train->validate($d)){
          $train->fill($d);
          $train->training_id=$id;
          $train->save();
        $count++;
              }else{
          
    }
}
return json_encode(['status'=>1,'title'=>"Success",'text'=>$count." Data Successfully Saved"]);
  
}


public function saveParticipant($id,Request $request){

//return view('traininginfo');
$count=0;
$a=$request->input("data");
$data = json_decode($a,true);
//dd($data);
$deletedId=DB::table("participant_info")->where('training_id',$id)->pluck('id');

foreach($data as $d){
 $train = new ParticipantInfo();
    $train->fill($d);
     $train->training_id=$id;
     $train->save();
     $count++; 
}
foreach($deletedId as $d){
  ParticipantInfo::find($d)->delete();
}
return json_encode(['status'=>1,'title'=>"Success",'text'=>$count." Data Successfully Saved"]);

  
}



public function getTrainingList(){
return view("trainingview");
}


    public function lists(Request $request) {
    
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
      if($search==null){
         $train=DB::table('training_info')->select(['training_info.id','training_info.name','training_info.start_date','training_info.end_date','training_info.venue','project_component.name as componentName','project_activity.activity as act','project_subactivity.subactivity as subact','training_type.name_en as eventType','gov_org.name_en as organizer'])->join('project_component','training_info.component','=','project_component.id')->join('project_activity','training_info.activity','=','project_activity.id')->join('project_subactivity','training_info.immediate_result','=','project_subactivity.id')->join('training_type','training_info.training_type','=','training_type.id')->join('gov_org','training_info.organized_for','=','gov_org.id')->orderby('id',"desc")
        // $acts = DB::table('training_info')->select(['name','id'])
        ->Paginate($entry,['*'],'page', $page );
        return $train;
      }
      else{

        $train=DB::table('training_info')->select(['training_info.id','training_info.name','training_info.start_date','training_info.end_date','training_info.venue','project_component.name as componentName','project_activity.activity as act','project_subactivity.subactivity as subact','training_type.name_en as eventType','training_category.name_en as eventCat','gov_org.name_en as organizer'])->join('project_component','training_info.component','=','project_component.id')->join('project_activity','training_info.activity','=','project_activity.id')->join('project_subactivity','training_info.immediate_result','=','project_subactivity.id')->join('training_type','training_info.training_type','=','training_type.id')->join('training_category','training_info.training_category','=','training_category.id')->join('gov_org','training_info.organized_for','=','gov_org.id')
        ->where('training_info.name', 'LIKE', "%$search%")
         ->Paginate($entry,['*'],'page', $page );
        return $train;
      }



}


public function edit($id){
  
   $trainingInfo=DB::select("SELECT * FROM training_info WHERE id =" .$id);
//dd($trainingInfo);
$staffInfo=DB::table("staff_information")->select(['staff_information.staffId','staff_information.responsibility','staff_information.remarks','staff_info.name_en as name'])->where('training_id','=',$id)->join('staff_info','staff_information.staffId','staff_info.id')->get();
$resourcePersonInfo=DB::table("resource_person_info")->select(['resource_person_info.rPersonId','resource_person_info.jobDes','resource_person_info.remarks','resource_person.name_en as name'])->where('training_id','=',$id)->join('resource_person','resource_person_info.rPersonId','resource_person.id')->get();
 $participantInfo=DB::table("participant_info")->select(['participant_info.name','participant_info.gender','participant_info.caste','participant_info.age','designation.name as title','participant_info.level','gov_org.name_en as ministry','participant_info.agency','district.name as district','participant_info.phone','participant_info.email','participant_info.remarks'])->where('participant_info.training_id','=',$id)->join('gov_org','participant_info.ministry','gov_org.id')->join('district','participant_info.district','district.id') ->join('designation','participant_info.title','=','designation.id')->get();



return (["trainingInfoEdit"=>$trainingInfo,"staffInfoEdit"=>$staffInfo,"resourcePersonInfoEdit"=>$resourcePersonInfo,"participantInfoEdit"=>$participantInfo]);
//return(["trainingInfoEdit"=>$trainingInfo,"participantInfoEdit"=>$participantInfo]);
  


}


}
