<?php

namespace App;

class Project extends BaseModel {

    protected $table = 'project_component';
    protected $fillable = ['name', 'description','approved','disabled'];
    protected $rules = [
        
        'name' => 'string|required',
        'description' => 'string',
        'approved'=>'string',
        'disabled'=>'string',
    ];
    
}
