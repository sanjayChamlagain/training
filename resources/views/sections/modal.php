<div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                       <h4 id="modal-heading" class="modal-title"></h4>
                 <input type="hidden" name="_tokent" value="<?php echo csrf_token(); ?>" id="tok">

                     </div>

                   <!-- </div> -->

              <form id="form" action="" method="post" onsubmit="save(event)">
              <div id="modal-body" class="modal-body">
         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <!--  <label for="code" class="col-sm-12 ">Staff ID<b style="color:red">*</b></label> -->
                                <div style="display:none" class="col-sm-12">
                                    <select class="form-control" id="staff_id" name="staff_id">      
                                      
                                    </select>
                                </div>
                                <div class="form-group" >

                     <label for="code" class="col-sm-12 ">Old Password<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="oldpassword" placeholder="Password" type="password" name="oldpassword" >
                     </div>
                 </div>
                             <div class="form-group" >

                     <label for="code" class="col-sm-12 ">New Password<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="password" placeholder="Password" type="password" name="password" >
                     </div>
                 </div>
              <div class="form-group">
                     <label for="code" class="col-sm-12 ">Confirm Password<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="password_confirmation" placeholder="Repeat password" type="password" name="password_confirmation" >
                     </div>
                 </div>
                 
              </div>


               
              <div class="modal-footer">
                <ul id="selected-per"></ul>
                <div id="foot">
                <button type="submit" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="modal-save" type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
              </form>
            </div>
         </div>
         </div>

         
        <script>
     function save(e){
    e.preventDefault();
  $.ajax({
  method:'post',
 url:baseurl+"/pendinguser/changepass/",
 data:$("#form").serialize(),
  success:function(resp){

       var a =JSON.parse(resp);
          toast(a);
          
          
             },
     fail:function(){

      }
  });

}
     
        </script>