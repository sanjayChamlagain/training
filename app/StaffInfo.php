<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffInfo extends BaseModel
{
    public $timestamps=false;
  protected $table = 'staff_information';
    protected $fillable = ['staffId','responsibility','remarks','training_id'];
    protected $rules = [
        'staffId' => 'integer',
        'responsibility' => 'string',
        'remarks' => 'string',
        'training_id'=>'integer'
    ];
}