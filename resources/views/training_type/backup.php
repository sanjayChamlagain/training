
<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<div  id="body" class="content-wrapper">
<?php  } ?>

<!-- Content Wrapper. Contains page content -->
    <section class="content-header">
        <h1>
          Types of Event
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-5">

                    <form class="form-horizontal" id="form" onsubmit="formSubmit(event)" method="post" action="">

                        <input type="hidden" id="id" name="id" value="">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Types of Event</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Component</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="component_id" name="component_id" onchange="changeActivity(event)">      
                                      <option id="select" value="">Select one..</option>
                                        
                                            <?php foreach($comp as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                                
                                            <?php endforeach;?>
                                                                 
                                  
                                                
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Activity</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="activity_id" name="activity_id" onchange="changeSubActivity(event)">      
                                      <option id="select" value="">Select one..</option>
                                        
                                            <?php foreach($act as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->activity;?></option>
                                                
                                            <?php endforeach;?>
                                                                 
                                  
                                                
                                    </select>
                                </div>
                            </div>
                           <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Sub-Activity</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="subactivity_id" name="subactivity_id">      
                                      <option id="select" value="">Select one..</option>
                                        
                                            <?php foreach($sact as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->subactivity;?></option>
                                                
                                            <?php endforeach;?>
                                                                 
                                  
                                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_en" class="col-sm-3 control-label">Name(english)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_en" placeholder="Name(english)" type="text" name="name_en" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Name(Nepali)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_np" placeholder="Name(Nepali)" type="text" name="name_np" value="">
                                </div>
                            </div>
                      

                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-7">
    <div class="box col-md-4">
        <div class="box-header with-border">
            <h3 class="box-title">Training Type</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        
        
         <div id="table" class="box-body">
            <form id="tableform" >
            <b>SHOW</b><select id="selectentry" onchange="table(event)">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
                <b>ENTRIES</b>

                <div style="float:right">
          <form id="srch" name="srch">
        <input  id="searchfill" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn" name="submit">Search</button>
      </form>
    </div>

            <div id="showtable" class="box-body">
                <table id="org-table" class="table table-striped table-bordered">
                  <tr><th>ID</th><th>Name_en</th><th>Activity</th><th>Sub-Activity</th><th>ACTIONS</th></tr>

                    </table>
            </div>
          <div id="pagg">

            <ul class="pagination pagination-sm">
            </ul>

              </div>
        </form>


        </div>
    </div>
</div>

        </div>
    </section>
    <!-- /.content -->

<!-- /.content-wrapper -->

<script type="text/javascript">
var baseurl="<?php echo url('/');?>";
// var baseurl=window.location.hostname;

function changeActivity(e){
  e.preventDefault();
  var id= $("#component_id option:selected").val();

$.ajax({

    method:'get',
    url:baseurl+"/trainingtype/changeactivity/"+id,
       success:function(response){
        var items=response;
        $("#activity_id").empty().append('<option id="select" value="">Select Item..</option>');
        for(var i in items){
        $("#activity_id").append('<option value="'+items[i].id+'">'+items[i].activity+'</option>');
          }
          $('#activity_id').append('</select>');

    },
    fail:function(){
        alert("failed");
    }
});
}
function changeSubActivity(e){
  e.preventDefault();
  var id= $("#activity_id option:selected").val();

$.ajax({

    method:'get',
    url:baseurl+"/trainingtype/changesubactivity/"+id,
       success:function(response){
        var items=response;
        $("#subactivity_id").empty().append('<option id="select" value="">Select Item..</option>');
        for(var i in items){
        $("#subactivity_id").append('<option value="'+items[i].id+'">'+items[i].subactivity+'</option>');
          }
          $('#subactivity_id').append('</select>');

    },
    fail:function(){
        alert("failed");
    }
});
}



function table(){

    var entry=$("#selectentry").val();

var search=$("#searchfill").val();
 if(($("#searchfill").val())!=""){
    var search=$("#searchfill").val();
  }
var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index;
}
$.ajax({

    method:'get',
    url:baseurl+"/trainingtype/list?entry="+entry+"&page="+page+"&search="+search,
    success:function(response){
        createTable(response);

    },
    fail:function(){
        alert("failed");
    }
});



}



function searchClicked(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
    url:baseurl+"/trainingtype/list?entry="+entry+"&search="+search,
    success:function(response){
      createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});
}


function createTable(resp){

    var t=document.getElementById("org-table");
   $("body #org-table").find("tr:gt(0)").remove();

    var rowCount=1;
    var data=resp.data;
    for(var i in data){
    var row=t.insertRow(rowCount);

    row.insertCell(0).innerHTML=data[i].id;
    row.insertCell(1).innerHTML=data[i].name_en;
    row.insertCell(2).innerHTML=data[i].pactivity;
      row.insertCell(3).innerHTML=data[i].subactivity;

    row.insertCell(4).innerHTML="<a href='javascript:void(0)' onclick='edit("+data[i].id+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='delt("+data[i].id+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
        rowCount++;

   }
   callForPagination(resp);
     return true;
}

   function formSubmit(e){
    // alert(baseurl);
    e.preventDefault();
    if( $('#id').val()==""){
      // var url="http://localhost/pfmsp/trainingtype/creates";
            var url=baseurl+"/trainingtype/creates";
            }
           else{
            var url=baseurl+"/trainingtype/updates/"+ $('#id').val();
            }
       $.ajax({
           method:"POST",
           url:url,
           data:$("#form").serialize(),
           success:function(resp){
            var a =JSON.parse(resp);
             if(a.status==1){
           $("#component_id").val("");
           $("#activity_id").val("");
           $("#subactivity_id").val("");
             $('#name_en').val("");
             $('#name_np').val("");
             $("#id").val("");
              $("#submit").text("Create");
            }

               table();
                if(a.records){
                 var records=a.records;
                   a.records.delete;
                     toast(a);
              $("body .pagination li a").trigger("click",records);

                 }else{
                   toast(a);
                   table();
               }


        },
          fail:function(){

          alert("failed");
     }
      });
}

function edit(id){
    $("#submit").text("update");
     $.ajax({
     method:'get',
       url:baseurl+"/trainingtype/edits/"+id,
     success:function(resp){

             $('#id').val(resp.id);
            $("#component_id").val(resp.component_id);
            $("#activity_id").val(resp.activity_id);
            $("#subactivity_id").val(resp.subactivity_id);
             $('#name_en').val(resp.name_en);
             $('#name_np').val(resp.name_np);
             $('#approved').val(resp.approved);
             $('#disabled').val(resp.disabled);

         },
        fail:function(){

         }
     });

   }


function delt(id){
    var conf=confirm("Are you sure you want to delete?");
   if(conf){
    $.ajax({
        method:'get',
      url:baseurl+"/trainingtype/deletes/"+id,
        success:function(resp){
             var a =JSON.parse(resp);
             toast(a);
               table();
        },

        fail:function(){
            alert("Fail");

        }
    });
}
}
function resetForm(){
  $('#id').val('');
  $('#submit').text('Create');
}
</script>

<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>
