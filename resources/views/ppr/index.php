<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>

<div  id="body" class="content-wrapper">
<?php  } ?>


    <!-- Content Header (Page header) -->
    <section id="part" class="content-header">
        <h1>
        PPR Indicator
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->

        <div class="row">
            <div class="col-md-6">


                    <form class="form-horizontal" id="form" onsubmit="formSubmit(event)"  action="">

                        <input type="hidden" id="id" name="id" value="">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <!-- <h3 class="box-title">Project Activity</h3> -->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                         <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Select Training</label>
                              <div class="col-sm-9">
                                    <select class="form-control" id="training" name="training" required> 
                                    <option id="select" value="">Select one..</option>
                                              
                                        <?php foreach($train as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                                
                                            <?php endforeach;?> 
                                                                     
                                  
                                             
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Basis of </label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="basis" name="basis" required>                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                               <option value="gender">Gender</option>
                                               <option value="caste">Caste/Ethnicity</option>
                                               <option value="position">Position</option>
                                               <option value="age">Age Group</option>
                                                 <option value="agency">Agency</option>
                                           
                                               
                                    </select>
                                    </div>
                                </div>
                            
                               

                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-default">View</button>
                            
                        </div>
                    </div>
                </form>
               <!--  <div id="showtable" class="box-body">
                          <table id="participant-table" class="table table-striped table-bordered">
                              <tr><th> ID</th><th>Name</th><th>Gender</th><th>phone</th><th>Bank</th><th>Bank Ac</th></tr>

                                </table>


                        </div> -->
            
                      
            
                
            </div>

          </div>
          <!-- /.modal-dialog -->

        <!-- /.modal -->
    </section>
    
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
 var baseurl="<?php echo url('/');?>";
  

   function formSubmit(e){
    var id=$("#training option:selected").val();
    var basis=$("#basis option:selected").val();
   
    e.preventDefault();
    
 window.open(baseUrl+'/ppr/lists?id='+id+'&basis='+basis,'_blank');

}



</script>

<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>