<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 


// Route::get('/', function () {
     Route::get('/','LoginController@index');
    // return view('login.login');
// });


Route::get('pie',function(){
return view('piechart.index');
});




Route::group(['prefix' =>'login'], function () {

Route::post('/','LoginController@login');
// Route::post('changepass','LoginController@changepass');


});

Route::get('/logout', function () {

      Session::flush();
    return redirect("/");
});
Route::group(['prefix' =>'register'], function () {
    Route::get('/','RegisterController@index');
     Route::post('create','RegisterController@create');
   
  
    });

Route::group(['prefix' =>'pendinguser'], function () {

Route::get('/','PendinguserController@index');
  Route::get('list','PendinguserController@viewuserlist');
  Route::post('changepass','PendinguserController@changepass');

 // Route::get('lists','PendinguserController@lists');
 Route::get('enable/{id}','PendinguserController@enable');
 Route::get('disable/{id}','PendinguserController@disable');


});

Route::group(['prefix' =>'participant'], function () {

Route::get('/','participantController@index');
  Route::get('lists/{id}','participantController@viewuserlist');
 


});

Route::group(['prefix' =>'events'], function () {

Route::get('/','eventController@index');
  Route::get('lists/{id}','eventController@viewuserlist');
 


});

Route::group(['prefix' =>'ppr'], function () {

Route::get('/','pprController@index');
  Route::get('lists','pprController@lists');
 


});




    Route::group(['prefix' =>'traininginfo'], function () {
    Route::get('/','TrainingInfoController@index');
    Route::get('view','TrainingInfoController@getTrainingList');
    Route::post('savestaff/{id}','TrainingInfoController@saveStaff');
   Route::post('saveresourceperson/{id}','TrainingInfoController@saveResourcePerson');
   Route::post('savetraininginfo','TrainingInfoController@saveTrainingInfo');
   Route::post('updatetraininginfo/{id}','TrainingInfoController@updateTrainingInfo');
   Route::post('saveparticipant/{id}','TrainingInfoController@saveParticipant');
   Route::get('list','TrainingInfoController@lists');
    Route::get('edit/{id}','TrainingInfoController@edit');
    Route::get('changeactivity/{id}','TrainingInfoController@getActivity');
    Route::get('changesubactivity/{id}','TrainingInfoController@getSubact');
    
    
   
    });

Route::group(['prefix' =>'org'], function () {
    Route::get('/','OrgController@index');
    Route::get('list','OrgController@lists');
    Route::get('edits/{id}','OrgController@edits');
    Route::post('creates','OrgController@creates');
    Route::post('updates/{id}','OrgController@updates');
    Route::get('deletes/{id}','OrgController@deletes');
});




Route::group(['prefix' => 'organization'], function () {
    Route::get('/','OrganizationController@index');
    Route::get('list','OrganizationController@lists');
    Route::get('edits/{id}','OrganizationController@edits');
    Route::post('creates','OrganizationController@creates');
    Route::post('updates/{id}','OrganizationController@updates');
    Route::get('deletes/{id}','OrganizationController@deletes');
});

Route::group(['prefix' => 'project'], function () {
    Route::get('/','ProjectController@index');
    Route::get('list','ProjectController@lists');
    
    Route::get('edits/{id}','ProjectController@edits');
    Route::post('creates','ProjectController@creates');
    Route::post('updates/{id}','ProjectController@updates');
    Route::get('deletes/{id}','ProjectController@deletes');

});

Route::group(['prefix' =>'activity'], function () {
    Route::get('/','ActivityController@index');
    Route::get('list','ActivityController@lists');
    Route::get('edits/{id}','ActivityController@edits');
    Route::post('creates','ActivityController@creates');
    Route::post('updates/{id}','ActivityController@updates');
    Route::get('deletes/{id}','ActivityController@deletes');
});

Route::group(['prefix' =>'subactivity'], function () {
    Route::get('/','SubActivityController@index');
    Route::get('list','SubActivityController@lists');
    Route::get('edits/{id}','SubActivityController@edits');
    Route::post('creates','SubActivityController@creates');
    Route::post('updates/{id}','SubActivityController@updates');
    Route::get('deletes/{id}','SubActivityController@deletes');
     Route::get('changeactivity/{id}','SubActivityController@getActivity');
});
Route::group(['prefix' =>'designate'], function () {
    Route::get('/','DesignateController@index');
    Route::get('list','DesignateController@lists');
    Route::get('edits/{id}','DesignateController@edits');
    Route::post('creates','DesignateController@creates');
    Route::post('updates/{id}','DesignateController@updates');
    Route::get('deletes/{id}','DesignateController@deletes');
});

Route::group(['prefix' =>'staff'], function () {
    Route::get('/','StaffController@index');
    Route::get('list','StaffController@lists');
    Route::get('edits/{id}','StaffController@edits');
    Route::post('creates','StaffController@creates');
    Route::post('updates/{id}','StaffController@updates');
    Route::get('deletes/{id}','StaffController@deletes');
});

Route::group(['prefix' =>'govtype'], function () {
    Route::get('/','GovtypeController@index');
    Route::get('list','GovtypeController@lists');
    Route::get('edits/{id}','GovtypeController@edits');
    Route::post('creates','GovtypeController@creates');
    Route::post('updates/{id}','GovtypeController@updates');
    Route::get('deletes/{id}','GovtypeController@deletes');
});
Route::group(['prefix' =>'govorg'], function () {
    Route::get('/','GovorgController@index');
    Route::get('list','GovorgController@lists');
    Route::get('edits/{id}','GovorgController@edits');
    Route::post('creates','GovorgController@creates');
    Route::post('updates/{id}','GovorgController@updates');
    Route::get('deletes/{id}','GovorgController@deletes');
});

Route::group(['prefix' =>'govpost'], function () {
    Route::get('/','GovpostController@index');
    Route::get('list','GovpostController@lists');
    Route::get('edits/{id}','GovpostController@edits');
    Route::post('creates','GovpostController@creates');
    Route::post('updates/{id}','GovpostController@updates');
    Route::get('deletes/{id}','GovpostController@deletes');
});

Route::group(['prefix' =>'focal'], function () {
    Route::get('/','FocalController@index');
    Route::get('list','FocalController@lists');
    Route::get('edits/{id}','FocalController@edits');
    Route::post('creates','FocalController@creates');
    Route::post('updates/{id}','FocalController@updates');
    Route::get('deletes/{id}','FocalController@deletes');
});
Route::group(['prefix' =>'trainingtype'], function () {
    Route::get('/','TrainingtypeController@index');
    Route::get('list','TrainingtypeController@lists');
    Route::get('edits/{id}','TrainingtypeController@edits');
    Route::post('creates','TrainingtypeController@creates');
    Route::post('updates/{id}','TrainingtypeController@updates');
    Route::get('deletes/{id}','TrainingtypeController@deletes');
      Route::get('changeactivity/{id}','TrainingtypeController@getActivity');
       Route::get('changesubactivity/{id}','TrainingtypeController@getSubActivity');
});
Route::group(['prefix' =>'trainingcat'], function () {
    Route::get('/','TrainingcatController@index');
    Route::get('list','TrainingcatController@lists');
    Route::get('edits/{id}','TrainingcatController@edits');
    Route::post('creates','TrainingcatController@creates');
    Route::post('updates/{id}','TrainingcatController@updates');
    Route::get('deletes/{id}','TrainingcatController@deletes');
});

Route::group(['prefix' =>'trainingtopic'], function () {
    Route::get('/','TrainingtopicController@index');
    Route::get('list','TrainingtopicController@lists');
    Route::get('edits/{id}','TrainingtopicController@edits');
    Route::post('creates','TrainingtopicController@creates');
    Route::post('updates/{id}','TrainingtopicController@updates');
    Route::get('deletes/{id}','TrainingtopicController@deletes');
});

Route::group(['prefix' =>'resource'], function () {
    Route::get('/','ResourceController@index');
    Route::get('list','ResourceController@lists');
    Route::get('edits/{id}','ResourceController@edits');
    Route::post('creates','ResourceController@creates');
    Route::post('updates/{id}','ResourceController@updates');
    Route::get('deletes/{id}','ResourceController@deletes');
});