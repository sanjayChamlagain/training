<?php

namespace App;

class Activity extends BaseModel {

    protected $table = 'project_activity';
     
    protected $fillable = ['component_id','activity', 'description'];
    protected $rules = [
        'component_id'=>'integer',
        'activity' => 'string|required',
        'description' => 'string'
    ];

}
