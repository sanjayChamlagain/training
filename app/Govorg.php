<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Govorg extends BaseModel
{
    protected $table='gov_org';
    protected $fillable=['parent_org','code','orgtype_id','name_en','name_np','district','postal','phone','fax','email','website','approved','disabled'];
    protected $rules=[
      'parent_org'=>'nullable|integer',
      'code'=>'string',
      'orgtype_id'=>'integer|required',
      'name_en'=>'string|required',
      'name_np'=>'string',
      
      'district'=>'string',
      'postal'=>'nullable|string',
      
      'phone'=>'nullable|integer',
      'fax'=>'nullable|string',
      'email'=>'nullable|string',
      'website'=>'nullable|string',
      'approved'=>'string',
      'disabled'=>'string',


    ];

}
