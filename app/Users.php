<?php

namespace App;


class Users extends BaseModel
{
protected $table = 'users';
protected $hidden=['password'];
    protected $fillable = ['staff_id','username','password','flag'];
    protected $rules = [
    	'staff_id' => 'integer|required',
        'username' => 'string|required',
        'flag'=>'integer',
        'password' => 'min:6|confirmed|required',
         'password_confirmation'=>'required_with:password|min:6|same:password',
        
        ];
}
