<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends BaseModel
{
    protected $table='staff_info';
    protected $fillable=['staff_id','name_en','name_np','gender','dob','designation_id','phone','mobile','email','organization_id','approved','disabled'];
    protected $rules=[
      'staff_id'=>'nullable|string',
      'name_en'=>'string|required',
      'name_np'=>'string',
      'gender'=>'string|required',
      'dob'=>'nullable|date',
      'designation_id'=>'integer',
      'phone'=>'nullable|integer',
      'mobile'=>'nullable|integer',
      'email'=>'nullable|string',
      'organization_id'=>'integer',
      'approved'=>'string',
      'disabled'=>'string',


    ];

}
