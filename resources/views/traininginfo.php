<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<div  id="body" class="content-wrapper">
<?php  } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Training Info
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">

                    <form class="form-horizontal" novalidate id="basic_info_form" onsubmit="saveTrainingInfo(event)" method="post" action="">
                        <input type="hidden" id="id_edit" name="iddd" value="">
                        <input type="hidden" id="id_no" name="iddee" value="">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Basic Info</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        </div>
                         
                        <div class="box-body">
                      
                             <div class="row">
                               <div class="col-md-5">
                             <div class="form-group">
                                <label for="name" class="col-sm-5 control-label">Component</label>
                                    <div class="col-sm-7">
                                    <select class="form-control" id="component" name="component" onchange="changeActivity(event)" required>      
                                      <option id="select" value="">Select one..</option>
                                        
                                         <?php foreach($component as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                                
                                            <?php endforeach;?>
                                          
                                                
                                    </select>
                                </div>
                            </div>
                            </div>
                             <div class="form-group">
                               <div class="col-md-5">
                                <label for="name" class="col-sm-5 control-label">Activity</label>
                                 <div class="col-sm-7">
                                    <select class="form-control" id="activity" name="activity"  onchange="changeSubActivity(event)" required >      
                                      <option id="select" value="">Select one..</option>
                                        
                                          <?php foreach($activity as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->activity;?></option>
                                                
                                            <?php endforeach;?>
                                          
                                                
                                    </select>
                                </div>
                            </div>
                            </div>
                            </div>
                       <div class="row">
                         <div class="col-md-5">
                            <div class="form-group">
                               
                                <label for="name" class="col-sm-5 control-label">Sub-Activity</label>
                                 <div class="col-sm-7">
                                    <select class="form-control" id="immediate_result" name="immediate_result" required >      
                                      <option id="select" value="">Select one..</option>
                                        
                                          <?php foreach($ir as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->subactivity;?></option>
                                                
                                            <?php endforeach;?>
                                          
                                                
                                    </select>
                                </div>
                            <!-- </div> -->
                            </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-5">
                                <label for="name" class="col-sm-5 control-label">Event Type</label>
                                  <div class="col-sm-7">
                                    <select class="form-control" id="training_type" name="training_type" required >                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                               <?php foreach($trainingType as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name_en;?></option>
                                                
                                            <?php endforeach;?>
                                          
                                    </select>
                                </div>
                            </div>
                           </div>
                           </div>
                            <div class="row">
                              
                             <div class="col-md-5">
                            <div class="form-group">
                                <label for="name" class="col-sm-5 control-label">Name</label>
                                
                                <div class="col-sm-7">
                                    <input class="form-control" id="name" placeholder="Name" type="text" name="name" value="" required />
                                </div>
                            </div>
                            </div>
                              <div class="col-md-5">
                             <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Venue</label>

                                <div class="col-sm-7">
                                    <input class="form-control" id="venue" placeholder="Venue" type="text" name="venue" value="" required >
                                </div>
                            </div>
                            </div>
                           </div>
                             <div class="row">
                       

                              <div class="col-md-5">
                            <div class="form-group">
                                <label for="name" class="col-sm-5 control-label">Organized For</label>
                                 <div class="col-sm-7">
                                    <select class="form-control" id="organized_for" name="organized_for" required>      
                                      <option id="select" value="">Select one..</option>
                                        <?php foreach($organisedFor as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name_en;?></option>
                                                
                                            <?php endforeach;?>
                                          
                                                
                                    </select>
                                </div>
                            </div>
                            </div>
                             <div class="col-md-5">
                              <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Description</label>

                                     <div class="col-sm-7">
                                    <input class="form-control" id="description" placeholder="Description" type="text" name="description" value="" required>
                                </div>
                            </div>
                          
                           </div>
                            </div>
                            <div class="row">
                             <!--  -->


                              <div class="col-md-5">
                             <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Start Date</label>
                                   <div class="col-sm-7">
                                    <input class="form-control" id="start_date" placeholder="yyyy/mm/dd" type="date" name="start_date" value="" required>
                                </div>
                            </div>
                           </div>
                           <div class="col-md-5">
                            <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">End Date</label>

                                <div class="col-sm-7">
                                    <input class="form-control" id="end_date" placeholder="yyyy/mm/dd" type="date" name="end_date" value="" required>
                                </div>
                            </div>
                            </div>
                           </div>
                           <div class="row">
                             
                              <div class="col-md-5">
                             <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Target</label>

                                <div class="col-sm-7">
                                    <input class="form-control" id="target" placeholder="Target" type="text" name="target" value="" required >
                                </div>
                            </div>
                            </div>
                            <div class="col-md-5">
                            
                             <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Achievement</label>

                                <div class="col-sm-7">
                                    <input class="form-control" id="achievement" placeholder="Achievement" type="text" name="achievement" value="" required>
                                </div>
                            </div>
                             </div>
                            </div>
                            <div class="row">
                             
                             
                            <div class="col-md-5">
                            
                             <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Remarks</label>

                                <div class="col-sm-7">
                                    <input class="form-control" id="remarks" placeholder="Remarks" type="text" name="remarks" value="" required>
                                </div>
                            </div>
                             </div>
                            </div>
                            

                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit" onsubmit="saveTrainingInfo(event)" class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                                           </div>
                </form>
         </div>
 <input type="hidden" id="id_now" name="iddee" value="">
              
            <div class="row" id="staff-div">
    <div class="box col-md-4">
        <div class="box-header with-border">
            <h3 class="box-title">Staff Information</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="col-md-2" style="margin-left: 500px;" >
        <button type="submit" id="sadd"  class="btn btn-md btn-success" data-toggle='modal' data-target='#modal-staff' style="font-size:18px;">Add New</button>
        </div>
      <div class="row" >
        </div>
              <div id="staff_table" class="box-body" style="display:none">
            <form id="staff_tableform" >
            <b>SHOW</b><select id="selectentry1" onchange="table(event)">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
                <b>ENTRIES</b>

                <div style="float:right">
          <form id="srch" name="srch">
        <input  id="searchfill1" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn1" name="submit">Search</button>
      </form>
    </div>

            <div id="show" class="box-body" style="width:100%; max-height: 250px; overflow: auto; position:relative;">
                <table id="staff_table_show" class="table table-striped table-bordered"  >
                  <tr><th>SN</th><th>Name</th><th>Responsibility</th><th>Remarks</th><th>ACTIONS</th></tr>

                    </table>
            </div>
          <div id="pagg">

            <ul class="pagination pagination-sm">
            </ul>

              </div>
        </form>


    

        <div class="box-footer">
                            <button type="submit" id="submit"  onclick="saveStaffInfo()" class="btn btn-default">Save</button>
     

         </div>
              </div>
    </div>
</div>

        <div class="row" id="resource-div" >
    <div class="box col-md-4">
        <div class="box-header with-border">
            <h3 class="box-title">Resource Person</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="col-md-2" style="margin-left: 500px;" >
         <button type="submit" id="radd"   class="btn btn-md btn-success" data-toggle='modal' data-target='#modal-resource' style="font-size:18px;">Add New</button>
     </div>
 
      <div class="row">
      
       </div>

         <div id="resource_person_table" class="box-body" style="display:none">
            <form id="resource_person_tableform" >
            

                <div style="float:right">
          <form id="srch" name="srch">
        <input  id="searchfill" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn" name="submit">Search</button>
      </form>
    </div>

            <div id="resource_person_showtable" class="box-body" style="width:100%; max-height: 250px; overflow: auto; position:relative;">
                <table id="resource_person_table_show" class="table table-striped table-bordered">
                  <tr><th>SN</th><th>Name</th><th>Job Description</th><th>Remarks</th><th>ACTIONS</th></tr>

                    </table>
            </div>
          <div id="pagg">

            <ul class="pagination pagination-sm">
            </ul>
              </div>
        </form>


         <div class="box-footer">
                            <button type="submit" id="submit"  onclick="saveResourcePersonInfo(event)" class="btn btn-default">Save</button>
       
        </div>                     
         </div>
    </div>
   
</div>
  <div class="row" id="participant-div" >
    <div class="box col-md-4">
        <div class="box-header with-border">
            <h3 class="box-title">Participant</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
      
        <div class="col-md-2" style="margin-left: 500px;" >
         <button type="submit" id="padd"   class="btn btn-md btn-success" onclick="clearData()" data-toggle='modal' data-target='#modal-participant' style="font-size:18px;">Add New</button>
     </div>
 <div class="row">

 </div>
         <div id="participant_table" class="box-body" style="display:none">
            <form id="participant_tableform" >
            <b>SHOW</b><select id="selectentry2" onchange="table(event)">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
                <b>ENTRIES</b>

                <div style="float:right">
          <form id="srch" name="srch">
        <input  id="searchfill2" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn2" name="submit">Search</button>
      </form>
    </div>

            <div id="participant_table_showtable" class="box-body" style="width:100%; max-height: 250px; overflow: auto; position:relative;">
                <table id="participant_table_show" class="table table-striped table-bordered">
                  <tr><th>SN</th><th>Name</th><th>Gender</th><th>Caste</th><th>Age</th><th>Title of position</th><th>Level of position</th><th>Ministry</th><th>Agency</th><th>District</th><th>Phone</th><th>Email</th><th>Remarks</th><th>Actions</th></tr>

                    </table>
            </div>
          <div id="pagg">

            <ul class="pagination pagination-sm">
            </ul>

              </div>
        </form>


         <div class="box-footer">
                            <button type="submit" id="submit"  onclick="saveParticipantInfo();" class="btn btn-default">Save</button>
                             </div>
</div>    
</div>


  </div>

<div class="modal fade" id="modal-staff">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  
                <h4 id="modal-heading" class="modal-title"></h4>
               <div class="col-md-3">
       <input id="staff_hidden" type="hidden" value="">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Name</label>
                                    <select class="form-control" id="staff_name" name="staff_name">                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                           <?php foreach($staff as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name_en;?></option>
                                                
                                            <?php endforeach;?>    
                                    </select>
                                </div>
                            
                                   </div>
                                    <div class="col-md-4"> 

                                    <div class="form-group">
                                <label for="name_np" class="col-sm-1 control-label">Responsibility</label>
                                 <input class="form-control" id="staff_responsibility" placeholder="Responsibility" type="text" name="staff_responsibility" value="">
                                </div>
                            </div>
                                                        <div class="col-md-4">
                             <div class="form-group">
                                <label for="name_np" class="col-sm-1 control-label">Remarks</label>

                                    <input class="form-control" id="staff_remarks" placeholder="Remarks" type="text" name="staff_remarks" value="">
                                </div>
                            </div>
                           
                  </div>
              <div id="modal-body" class="modal-body">
              
              </div>
               
              <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="modal-save" type="button"  onclick="tableShowStaff('staff_table')"class="btn btn-primary">Save changes</button>
                 </div>
            </div>
            <!-- /.modal-content -->
          </div>
          </div>


<div class="modal fade" id="modal-resource">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  
                <h4 id="modal-heading" class="modal-title"></h4>
               

 <input id="resource_hidden" type="hidden" value="">
  
       <div class="col-md-3">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Name</label>
                              
                                    <select class="form-control" id="resource_person_name" name="resource_person_name"> <option id="select" value="">Select one..</option>
                                              
                                        <?php foreach($resourcePerson as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name_en;?></option>
                                                
                                            <?php endforeach;?> 
                                                                     
                                  
                                             
                                    </select>
                                </div>
                      

                                   </div>
                                    <div class="col-md-4"> 

                                    <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Job Description</label>

                                    <input class="form-control" id="job_description" placeholder="Job Description" type="text" name="job_description" value="">
                              
                            </div>
                            </div>
                             <div class="col-md-4">
                             <div class="form-group">
                                <label for="name_np" class="col-sm-1 control-label">Remarks</label>

                              
                                    <input class="form-control" id="resource_person_remarks" placeholder="Remarks" type="text" name="resource_person_remarks" value="">
                                </div>
                          
                             </div>
                 
                          
                  </div>
              <div id="modal-body" class="modal-body">
              
              </div>
               
              <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="modal-save" type="button" onclick="tableShowResource('resource_person_table')" class="btn btn-primary">Save changes</button>
                 </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>




<div class="modal fade" id="modal-participant">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  
                <h4 id="modal-heading" class="modal-title"></h4>
               
                <div class="row">

    <input id="participant_hidden" type="hidden" value="">
               <div class="col-md-5"> 
    
                                    <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Name</label>

                                    <input class="form-control" id="participant_name" placeholder="Name" type="text" name="participant_name" value="">
                              
                            </div>
                            </div>
       <div class="col-md-5">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Gender</label>
                              
                                    <select class="form-control" id="gender" name="gender">                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                      <option id="select" value="Men">Men</option>  
                                      <option id="select" value="Women">Women</option> 
                                       
                                                
                                    </select>
                                </div>
                      

                                   </div>
                                                          
       </div>
       <div class="row">
                                <div class="col-md-5">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Caste</label>
                              
                                    <select class="form-control" id="caste" name="caste">                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                                  <option value="Brahmin/Chhetri">Brahmin/Chhetri</option> 
                                                   <option value="Dalit" >Dalit</option> 
                                                   <option value="Indigenous Nationalities" >Indigenous Nationalities</option>
                                                   <option value="Muslim" >Muslim</option>
                                                   <option value="Newar" >Newar</option>
                                                   <option value="Others" >Others</option> 
                                    </select>
                                </div>
                      

                                   </div>
                                    <div class="col-md-5">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Age</label>
                              
                                    <select class="form-control" id="age" name="age">                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                         <option id="select" value="15-19">15-19</option>  
                                           <option id="select" value="20-24">20-24</option> 
                                           <option id="select" value="25-29">25-29</option>  
                                           <option id="select" value="30-34">30-34</option>

                                           <option id="select" value="35-39">35-39</option>
                                           <option id="select" value="40+">40+</option>     
                                    </select>
                                </div>
                      

                                   </div>
                                    

       </div>
       <div class="row">
        <div class="col-md-5">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Title of Position</label>
                              
                                    <select class="form-control" id="title" name="title">                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                         <?php foreach($desg as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                                
                                            <?php endforeach;?>
                                               
                                    </select>
                                </div>
                      

                                   </div>
                                   <div class="col-md-5">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Level of Position</label>
                              
                                    <select class="form-control" id="level" name="level">                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                               <option value="1">1st Class</option>
                                               <option value="2">2nd Class</option>
                                               <option value="3">3rd Class</option>
                                               <option value="4">Assistant</option>
                                           
                                               
                                    </select>
                                </div>
                      

                                   </div>
                                   
       </div>
       <div class="row">
      <div class="col-md-5">
                             <div class="form-group">
                              <label for="name" class="col-sm-1 control-label">Ministry</label>
                              
                               
                              <select class="form-control" id="p_ministry" name="ministry">
                               
                                <option id="select" value="">Select one..</option>
                                 <?php foreach($min as $m):?>
                                                
                                                <option value="<?php echo $m->id;?>"><?php echo $m->name_en;?></option>
                                                
                                            <?php endforeach;?>
                                          </select>
                                </div>
                          
                             </div>
                                   
                                        <div class="col-md-5">
                             <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">Agency</label>
                              
                                    <select class="form-control" id="agency" name="agency">                                    
                                  
                                      <option id="select" value="">Select one..</option>
                                               <option value="Central/ Ministry Level">Central/ Ministry Level </option>
                                               <option value="Regional/ State Level">Regional/ State Level </option>
                                               <option value="District/ Local Level">District/ Local Level</option>
                                               
                                           
                                               
                                    </select>
                                </div>
                          
                             </div>
                                   
                                  
       </div>
       <div class="row">
         <div class="col-md-5">
        <div class="form-group">
                                <label for="name" class="col-sm-1 control-label">District</label>
                              
                                    <select class="form-control" id="participant_district" name="participant_district">                                    
                                   <option id="select" value="">Select one..</option>
                                         <?php foreach($dis as $d):?>
                                                
                                                <option value="<?php echo $d->id;?>"><?php echo $d->name;?></option>
                                                
                                            <?php endforeach;?>
                                     
                                    </select>
                                </div>
                      

                                   </div>
                                   
                                    <div class="col-md-5"> 

                                    <div class="form-group">
                                <label for="name_np" class="col-sm-5 control-label">Phone</label>

                                    <input class="form-control" id="participant_phone" placeholder="Phone" type="text" name="participant_phone" value="">
                              
                            </div>
                            </div>
                             
       </div>
       <div class="row">
        <div class="col-md-5">
                             <div class="form-group">
                                <label for="name_np" class="col-sm-1 control-label">Email</label>

                              
                                    <input class="form-control" id="participant_email" placeholder="Email" type="text" name="participant_email" value="">
                                </div>
                          
                             </div>
                            
                             <div class="col-md-5">
                             <div class="form-group">
                                <label for="name_np" class="col-sm-1 control-label">Remarks</label>

                              
                                    <input class="form-control" id="p_remarks" placeholder="Remarks" type="text" name="p_remarks" value="">
                                </div>
                          
                             </div>
                                                         
       </div>
       
       <div id="modal-body" class="modal-body">

       </div> 
               
              <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="modal-save" type="button" onclick="tableShowParticipant('participant_table')" class="btn btn-primary">Save changes</button>
                 </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
</div>

<!-- /.content-wrapper -->
  </section>

    <!-- /.content -->


<script type="text/javascript">
var baseurl="<?php echo url('/');?>";
function changeActivity(e){
  e.preventDefault();
  var id= $("#component option:selected").val();

$.ajax({

    method:'get',
    url:baseurl+"/traininginfo/changeactivity/"+id,
       success:function(response){
        var items=response;
        $("#activity").empty().append('<option id="select" value="">Select Item..</option>');
        for(var i in items){
        $("#activity").append('<option value="'+items[i].id+'">'+items[i].activity+'</option>');
          }
          $('#activity').append('</select>');

    },
    fail:function(){
        alert("failed");
    }
});
}

function changeSubActivity(e){
  e.preventDefault();
  var id= $("#activity option:selected").val();

$.ajax({

    method:'get',
    url:baseurl+"/traininginfo/changesubactivity/"+id,
       success:function(response){
        var items=response;
        $("#immediate_result").empty().append('<option id="select" value="">Select Item..</option>');
        for(var i in items){
        $("#immediate_result").append('<option value="'+items[i].id+'">'+items[i].subactivity+'</option>');
          }
          $('#immediate_result').append('</select>');

    },
    fail:function(){
        alert("failed");
    }
});
}


 
</script>
<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>