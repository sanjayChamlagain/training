<?php include(resource_path().'/views/sections/header.php');?>

<?php include(resource_path().'/views/sections/leftmenu.php');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Registration
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-12">

                  <div class="register-box-body">
    <p class="login-box-msg">Create New User</p>

    <form id="registerform" action="javascript:void(0)" method="post">
      <div class="form-group has-feedback">
         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input type="text" class="form-control" id="fullname" placeholder="Full name" name="fullname">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control"  id="email" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="password_conf" placeholder="Retype password" name="password_confirmation" >
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
                <select class="form-control" id="role" name="role">
              <option id="select" value="">Select Role..</option>
              <option id="select" value="super-admin">Super Admin</option>
                <option id="select" value="branch-admin">Admin</option>
                  <option id="select" value="user">User</option>
                     </select>
                    </div>
      <div class="row">

        <!-- /.col -->
        <div class="col-xs-4">
          <button  type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>

        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    function register(){

        $("#registerform").on("submit",function(e){
            e.preventDefault();
              $.ajax({

                   method:"post",
                   url:"<?php echo url('/register/userregister');?>",
                   data:$("#registerform").serialize(),
                success:function(response){
                var resp=JSON.parse(response);
                  toast(resp);
                  if(resp.status==1){

                    $("#fullname").val("");
                    $("#email").val("");
                    $("#password").val("");
                    $("#password_conf").val("");
                    $("#role").val("");



                  }

                        },
                  fail:function(response){

                    //on failure
                  }
              });

        });

  }
</script>

<?php include(resource_path().'/views/sections/footer.php');

