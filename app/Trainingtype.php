<?php

namespace App;

class Trainingtype extends BaseModel {

    protected $table = 'training_type';
    protected $fillable = [ 'name_en'];
    protected $rules = [
          'name_en' => 'string|required',
        'name_np' => 'string|nullable'
        
    ];
    
}
