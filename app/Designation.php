<?php

namespace App;

class Designation extends BaseModel {

    protected $table = 'designation';
    protected $fillable = ['name', 'level','approved','disabled'];
    protected $rules = [
        'name' => 'string',
        'level' => 'integer',
       
        'approved'=>'string',
        'disabled'=>'string',
    ];
    
}
