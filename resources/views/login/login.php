<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PFMSP | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo asset('assets/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo asset('css/AdminLTE.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo asset('assets/plugins/iCheck/square/blue.css');?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

   <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
</head>

<body class="hold-transition login-page">



 <!--  <div class="login-logo">
	<img src="images/nepal-govt-logo.png" >
	<h5><b>Government of Nepal</b></h5>
	<h5><b>Ministry of Health</b></h5>
	<h5><b>Department of Health Services</b></h5>
	<h3><b>Logistics Management Division</b></h3>
	<h1><b>Technical Specification Bank</b></h1>

  </div> -->
  <div class="error-display">
  <div class="login-box">
  <?php if(Session::has('message')):?>

<div class="alert alert-success"><h3>Successfully Registered</h3><?php echo Session::get('message'); ?></div>
<?php endif;?>
 <?php if(Session::has('password')):?>

<div class="alert alert-success"><h3>Check email</h3><?php echo Session::get('password'); ?></div>
<?php endif;?>
 <?php if(Session::has('verification')):?>

<div class="alert alert-success"><h3>User Verified</h3><?php echo Session::get('verification'); ?></div>
<?php endif;?>
<?php if(Session::has('msgerror')):?>

<div class="alert alert-error"><h3>Login Failed.</h3><?php echo Session::get('msgerror'); ?></div>
<?php endif;?>

<?php if(Session::has('passwordChanged')):?>

<div class="alert alert-success"><h3>Password Succesfully Changed.</h3>User can start your session</div>
<?php endif;?>
</div>
</div>
  <!-- /.login-logo -->
  <div class="login-box">
    <p class="login-box">Sign in to start your session</p>

    <form method="post" action="<?php echo url('/login');?>" >
      <div class="form-group">
         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
         
        <!-- <input id="email" type="email" class="form-control" placeholder="Email" name="email"> -->
        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
      </div>
       <div id="error"  style="text-align:center;color:red;display:none;font-family: "Times New Roman", Georgia, Serif;font-size=20px;"><i class="fa fa-fw fa-minus-square"></i></div>
      <div class="form-group">

                   <div id="spouse" class="form-group" >
                     <label for="code" class="col-sm-12 ">Username<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="username" placeholder="Username" type="text" name="username" >
                     </div>
                 </div>
                 <div class="form-group" >
                     <label for="code" class="col-sm-12 ">Password<b style="color:red">*</b></label>

                     <div class="col-sm-12">
                         <input class="form-control" id="password" placeholder="Password" type="password" name="password" >
                     </div>
                 </div>
       <!--  <input id="password" type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
      </div>
      <div class="row">
       
        <div class="col-xs-12" >
			<div class="col-xs-4"></div>
			<div class="col-xs-4"></br>
			<button  type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
         

			</div>
			<div class="col-xs-4"></div>
        </div>
        <div class="col-xs-12">
		<div class="col-xs-4"></div>
		 <div class="col-xs-6">
          <div class="checkbox icheck">
            <!-- <label> -->
             <!-- Please <a href="<?php echo url('/register');?>" class="font-size">Register</a> your account here. -->
             <!-- <a href="<?php echo url('/login/forget');?>" class="font-size-sm">Forget Password?</a> -->
            <!-- </label> -->
          </div>
        </div>
		<div class="col-xs-2"></div>
		</div>
		
        
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!--<a href="<?php //echo url('/signup');?>" class="btn btn-primary btn-block btn-flat">Sign Up</a>
  <!-- /.login-box-body -->
</div>
<div class="signup-section">
  <!-- Please <a href="<?php echo url('/register');?>" class="font-size">Register</a> your account here. -->
</div>

<!-- /.login-box -->
</body>

<!-- jQuery 2.2.3 -->
<script src="<?php echo asset('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo asset('assets/bootstrap/js/bootstrap.min.js');?>"></script>

<!-- iCheck -->
<script src="<?php echo asset('assets/plugins/iCheck/icheck.min.js');?>"></script>
<script>
  $(function(){
    <?php
    if(Session::has('error')){?>
       var emsg='<?php echo Session::get('error');?>';
       $("#error").text(emsg);
       $("#error").show();

   <?php } ?>





  });


$("#email,#password").on("focus",function(){
  $("#error").hide();
})




</script>
</body>
</html>
<?php include_once(resource_path().'/views/sections/loginfooter.php');
