<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\ParticipantInfo;
use App\District;
use Session;
use App\Log;
use Illuminate\Support\Facades\Hash;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class participantController extends Controller {
 public function index(){
      return view('participant.index',['train'=>$this->getTrain()]);
     
    }


    public function viewuserlist($id) {
     

      $participant = DB::table('participant_info')->select(['participant_info.id','participant_info.name','district.name as district','participant_info.gender','participant_info.phone','participant_info.email','designation.name as title','participant_info.agency','participant_info.level','gov_org.name_en as ministry','participant_info.age','participant_info.caste','participant_info.remarks'])
      ->join('gov_org','participant_info.ministry','=','gov_org.id')
      ->join('district','participant_info.district','=','district.id')
       ->join('designation','participant_info.title','=','designation.id')
      ->where('participant_info.training_id','=',$id)->get();

      

      $traine=DB::table('training_info')->select(['training_info.name','training_info.start_date','training_info.end_date','training_info.venue','project_component.name as componentName','project_activity.activity as act','project_subactivity.subactivity as subact','training_type.name_en as eventType','gov_org.name_en as organizer'])->join('project_component','training_info.component','=','project_component.id')->join('project_activity','training_info.activity','=','project_activity.id')->join('project_subactivity','training_info.immediate_result','=','project_subactivity.id')->join('training_type','training_info.training_type','=','training_type.id')->join('gov_org','training_info.organized_for','=','gov_org.id')->where('training_info.id','=',$id)->get();

          // $("#reg-form").show();
      foreach($traine as $train){
  $tab_text="<table id='participant_report'><thead><tr><td colspan='13'><h3 style='margin-left: 188px;font-weight:bold;'>NEPAL USAID/ Public Financial Management Stregnthening Project</h3></td></tr><tr><td colspan='13'><h4 style='margin-left: 320px;font-weight:bold;'>PFMSP Databse for Internal Use Only</h4></td></tr><tr><td colspan='13'><h4 style='margin-left: 360px;'>Individual Level Information</h4></td></tr><tr><td colspan='13'></td></tr><tr><td colspan='6'>Component:   ".$train->componentName."</td><td colspan='7'>IR(Immediate Result):  ".$train->act."</td></tr><tr><td colspan='6'>IND Number:   ".$train->subact."</td><td colspan='7'>Types of Event:   ".$train->eventType."</td></tr><tr><td colspan='6'>Start Date(month/date/year):  ".$train->start_date."</td><td colspan='7'>Venue:   ".$train->venue."</td></tr><tr><td colspan='6'>Activity Number ...........</td><td colspan='7'>Name of Event:   ".$train->name."</td></tr><tr><td colspan='6'>End Date(month/date/year):   ".$train->end_date."</td><td colspan='7'>Organizer Ministry/Agency:  ".$train->organizer."</td></tr><tr><td colspan='13'><h3> Details of Participant:</h3></td></tr></thead>";
      }
      
  $tab=$tab_text." <tbody><tr bgcolor='#87AFC6'><th> SN.</th><th>Name of Participants</th><th>Gender</th><th>Age</th><th>Caste</th><th>Title of Position</th><th>Level Of Position</th><th>Ministry</th><th>Agency</th><th>District</th><th>phone</th><th>Email</th><th>Remarks</th></tr>";
  $i=1;
   foreach($participant as $p){
$tab.="<tr><td>".$i."</td><td>".$p->name."</td><td>".$p->gender."</td><td>".$p->age."</td><td>".$p->caste."</td><td>".$p->title."</td><td>".$p->level."</td><td>".$p->ministry."</td><td>".$p->agency."</td><td>".$p->district."</td><td>".$p->phone."</td><td>".$p->email."</td><td>".$p->remarks."</td></tr>";
$i++;
   }         
$tab.="</tbody></table>";
return view('participant.view',['tab'=>$tab]);
     }
    
  
   
public function getTrain(){
   return \App\TrainingInfo::select('id','name')->get();
}


  }
