<?php

namespace App;

class Org extends BaseModel {

    protected $table = 'org_type';
    protected $fillable = [ 'name_en', 'name_np','approved','disabled'];
    protected $rules = [
        
        'name_en' => 'string|required',
        'name_np' => 'string',
        'approved'=>'string',
        'disabled'=>'string',
    ];
    
}
