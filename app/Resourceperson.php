<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resourceperson extends BaseModel
{
    protected $table='resource_person';
    protected $fillable=['name_en','name_np','gender','caste','age','expertise','organization_id','designation_id','phone','mobile','email','postal','approved','disabled'];
    protected $rules=[
      
      'name_en'=>'string|required',
      'name_np'=>'string',
      'gender'=>'string|required',
      'caste'=>'string',
      'age'=>'integer',
      'expertise'=>'string',
      'organization_id'=>'integer',
      'designation_id'=>'integer',
      'phone'=>'nullable|integer',
      'mobile'=>'nullable|integer',
      'email'=>'nullable|string',
      'postal'=>'nullable|string',
      'approved'=>'string',
      'disabled'=>'string',


    ];

}
