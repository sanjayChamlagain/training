<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<div  id="body" class="content-wrapper">
<?php  } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          All Users
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->

        <div class="row">
            <div class="col-md-12">

                    <div class="box col-md-6">
                        <div class="box-header with-border">
                            <h3 class="box-title">User List</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                  <div class="box-body">
                          <div id="table" class="box-body">
                         <div class="mb10">
		  <div class="pull-left">
                        <form id="tableform" >
                        <b>SHOW</b><select id="selectentry" onchange="viewBranchAdmin(event)">

                          
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                            <b>ENTRIES</b>
                          
						  </div>
                <div style="float:right">
                      <form id="srch" name="srch">
        <input  id="searchfill" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn" name="submit">Search</button>
      </form>
                </div>
			</div>

                        <div id="showtable" class="box-body">
                          <table id="pendinguser-table" class="table table-striped table-bordered">
                              <tr><th> SN</th><th>Username</th><th>Actions</th><th>Change Password</th></tr>

                                </table>


                        </div>
                      <div id="pagg">

                        <ul class="pagination pagination-sm">

                        </ul>

                          </div>




                    </div>


                        </div>

                    </div>
                </form>
            </div>

          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
var baseurl="<?php echo url('/');?>";



function table(){

    var entry=$("#selectentry").val();

var search=$("#searchfill").val();
 if(($("#searchfill").val())!=""){
    var search=$("#searchfill").val();
  }
var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index;
}
$.ajax({

    method:'get',
    url:baseurl+"/pendinguser/list?entry="+entry+"&page="+page+"&search="+search,
    success:function(response){
        createTable(response);

    },
    fail:function(){
        alert("failed");
    }
});



}



function searchClicked(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
    url:baseurl+"/pendinguser/list?entry="+entry+"&search="+search,
    success:function(response){
      createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});
}


function createTable(resp){
    var t=document.getElementById("pendinguser-table");

   $("body #pendinguser-table").find("tr:gt(0)").remove();
  var sn=resp.from;
    var rowCount=1;
    var data=resp.data;

    for(var i in data){


    var row=t.insertRow(rowCount);
    row.insertCell(0).innerHTML=sn;
    row.insertCell(1).innerHTML=data[i].username;
   

       row.insertCell(2).innerHTML="<button  id='a"+data[i].id+"enbuser' type='submit' onclick='enable("+data[i].id+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Enable</button>&nbsp;&nbsp;<button id='a"+data[i].id+"disuser'  type='submit' onclick='disable("+data[i].id+")' class='btn btn-xs btn-danger' ><i class='glyphicon glyphicon-trash'>Disable</button>"
      button(data[i].id,data[i].flag);

       row.insertCell(3).innerHTML= "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#modal-default' onclick='profile("+data[i].id+")'>Change Password</button>";

       // '<a id="passwordchange" class="btn btn-default btn-flat" data-toggle='modal' data-target='#modal-default'onclick="profile('+data[i].id+')">Change Password</a>';
        rowCount++;
     sn++;




   }
 callForPagination(resp);
   return true;
}

function button(id,flag){
 if(flag==1){
 $("#a"+id+"enbuser").attr("disabled",true);
 $("#a"+id+"disuser").attr("disabled",false);
     }
  if(flag==0){
    $("#a"+id+"disuser").attr("disabled",true);
            $("#a"+id+"enbuser").attr("disabled",false);

  }

}

function profile(id){

  $("#modal-heading").text("Change Password.");
 
   $("#modal-body #staff_id").empty().append('<option value="'+id+'">'+id+'</option></select>');
      // $("#modal-body #staff_id").append('<option value="1">ram</option></select>');

}

function enable(id){

    $.ajax({
    method:'get',
   url:baseurl+"/pendinguser/enable/"+id,
    success:function(resp){


         var a =JSON.parse(resp);
            toast(a);
            button(id,1);
              },
       fail:function(){

        }
    });
}
function disable(id){
  $.ajax({
  method:'get',
 url:baseurl+"/pendinguser/disable/"+id,
  success:function(resp){

       var a =JSON.parse(resp);
          toast(a);
          button(id,0);
             },
     fail:function(){

      }
  });

}


</script>

<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>