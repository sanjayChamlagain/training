<?php

namespace App;

class Govpost extends BaseModel {

    protected $table = 'gov_post';
    protected $fillable = ['name_en', 'name_np','level','approved','disabled'];
    protected $rules = [
        
        'name_en' => 'string|required',
        'name_np' => 'string',
        'level' => 'integer',
        'approved'=>'string',
        'disabled'=>'string',
    ];
    
}
