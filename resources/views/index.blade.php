@extends('layout')

@section('title','Hello!')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Country
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">
                <?php if(isset($update)):?>
                    <form class="form-horizontal" method="post" action="<?php echo url('/country/update/'.$update->id);?>">
                <?php else:?>
                    <form class="form-horizontal" method="post" action="<?php echo url('/country/create');?>">
                <?php endif;?>
                
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Countries</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Code</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="code" placeholder="Code" type="text" name="code" value="<?php echo isset($update)?$update->code:"";?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_en" class="col-sm-3 control-label">Name(English)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_en" placeholder="Name(English)" type="text" name="name_en" value="<?php echo isset($update)?$update->name_en:"";?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Name(Nepali)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_np" placeholder="Name(Neplai)" type="text" name="name_np" value="<?php echo isset($update)?$update->name_np:"";?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Create</button>
                            <button type="reset" class="btn btn-info pull-right">Reset</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-6">
                <div class="box col-md-4">
                    <div class="box-header with-border">
                        <h3 class="box-title">Countries List</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="country-table" class="table table-striped table-bordered" data-url="<?php echo url('/').'/country/lists';?>">
<!--                            <tr><th>ID</th><th>Name English</th><th>Name Nepali</th></tr>-->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection