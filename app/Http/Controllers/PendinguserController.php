<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Session;
use App\Log;
use Illuminate\Support\Facades\Hash;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class PendinguserController extends Controller {


    public function viewuserlist(Request $request) {

      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
    if($search==null){
      $acts = DB::table('users')->select(['users.id','users.flag','users.username',])->orderby('id',"desc")
        // ->join('staff_info','users.staff_id','=','staff_info.id')
        
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
     }
     else{

       $acts = DB::table('users')->select(['users.id','users.flag','users.username',])->orderby('id',"desc")
        // ->join('staff_info','users.staff_id','=','staff_info.id')
        ->where('username', 'LIKE', "%$search%")
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
       
      
     }
      
    }
    public function index(){
      return view('pendinguser.index');
    }
    public function enable($id){
      
       $user = Users::find($id);
       // $email=$user->email;
       $user->flag=1;
       $user->save();
      
        return json_encode(['status'=>1,'title'=>"success",'text'=>"User account Enabled"]);
    }
    public function disable($id){
     
       $user = Users::find($id);
       // $email=$user->email;
       $user->flag=0;
       $user->save();
       
        return json_encode(['status'=>1,'title'=>"success",'text'=>"User account Disabled"]);
    }


    public function changepass(Request $request){
//dd($request->all());
 $staff_id=$request->input("staff_id");
 $password_confirmation=$request->input("password_confirmation");
$newpass=$request->input("password");
$user=Users::find($staff_id);
$pass=$user->password;
if($newpass!=$password_confirmation){
  return json_encode(['status'=>0,'title'=>"error",'text'=>"Password and confirm password Not Matched!!"]);
}
$oldPass=($request->input("oldpassword"));
  if(Hash::check($oldPass,$pass)){
    
 $user->password=Hash::make($request->input("password"));
  $user->save();

   
return json_encode(['status'=>1,'title'=>"success",'text'=>"Password Sucessfully Changed."]);
}else{
  

 return json_encode(['status'=>0,'title'=>"error",'text'=>"Password Not Matched!!"]);
     }
}
  }
