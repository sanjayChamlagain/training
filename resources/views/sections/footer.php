<style>
  .overlayer {
    position: absolute;
    
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.5);
    z-index: 1031;
    cursor: pointer;
}
.loader{
      POSITION: RELATIVE;
    MARGIN: 0 AUTO;
    Z-INDEX: 1032;
    left: 38%;
    top: 17%;
    border-radius: 50%;
    border-top: 4px solid blue;
    border-right: 6px solid green;
    border-bottom: 7px solid red;
    border-left: 6px solid white;
    width: 25px;
    height: 25px;
}
.loding-text{
  position: relative;
    Z-INDEX: 1032;
    Z-INDEX: 1033;
    width: 100%;
    height: 100%;
    margin: 0 auto;
    left: 9%;
    top: -103%;
    color: #de0404;
    font-size: 32px;
}
.outer-div{
  
    margin: 0 auto;
    width: 202px;
    height: 42px;
    border: thick solid #3c8dbc;
    position: relative;
    top: 45%;
    /* padding: 0 50px; */
    background: white;

}
</style>
<footer class="main-footer">
<!--    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>-->
    <strong>Copyright &copy; 2014-2016 <a href="#">SAIPAL</a>.</strong> All rights
    reserved.
    
  </footer>
</div>
<!-- ./wrapper -->

<div class="overlayer" style="display:none;">
        <div class="outer-div">
          <div id="loader" class="loader" style=""> </div>
          <div class="loding-text">Loading...</div>
        </div>
          
        </div>

<?php include(resource_path().'/views/sections/modal.php');?>



<!-- jQuery 2.2.3 -->
<script src="<?php echo asset('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo asset('assets/bootstrap/js/bootstrap.min.js');?>"></script>


<!-- SlimScroll -->
<script src="<?php echo asset('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo asset('assets/plugins/fastclick/fastclick.js');?>"></script>
<script src="<?php echo asset('js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo asset('js/jquery.toast.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo asset('js/app.min.js');?>"></script>
<script src="<?php echo asset('js/pagination.js');?>"></script>
<script src="<?php echo asset('js/infoedit.js');?>"></script>
<script src="<?php echo asset('js/excel.js');?>"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script type="text/javascript">

 $(document).ajaxStart(function (event) {
  $(".overlayer").show();
 //alert(event.target.text);
});

 $(document).ajaxStop(function () {
   $(".overlayer").hide();
 //. $("#loader").hide();
  //alert("yuop");
});



function ajaxCall(e,name,check=0,id=0){
  
    if(e!==0){
     e.preventDefault();

  $('.active').removeClass('active');
$(e.currentTarget).parents('li.treeview').addClass('active');
         
   $(e.currentTarget).parent().addClass('active');
  }
   $.ajax({
     method:'get',
       url:baseUrl+"/"+name,
     success:function(resp){
          $("#body").empty().html(resp);

          if(check===0){
       window.history.pushState("", "",baseUrl+"/"+name);
            }
            if(name!=='traininginfo'){
             if(name!=='participant'){
               if(name!=='events'){
                  if(name!=='ppr'){
             
             table();
         }}}}

         if(name==='traininginfo'){
         if (id!=0){
          sequentialAjax(id);
         }else{
         

          $("#sadd").attr('disabled',true);
          $("#radd").attr('disabled',true);
          $("#padd").attr('disabled',true);

         }
     
        
         }
            return true;
             },
      
        fail:function(){

         }
     });
   
}
window.onpopstate = function (e) {
e.preventDefault();
var a=window .location .href.split('/');

ajaxCall(0,a[a.length-1],1);
}



<?php if("table"){?>
 if(typeof(table)=="function"){ 

  table();

 }
<?php } ?>

<?php if("editSetup"){?>
 if(typeof(editSetup)=="function"){ 

  editSetup();

 }
<?php } ?>

(function(){
    //work for the notifications
    if(typeof notification === "object" && notification !== null){
        if(notification.status==0){
           var icon = "error";
        }
        else if(notification.status==1){
            var icon = "success";
        }
        else if(notification.status==2){
            var icon = "warning";
        }
        else{
            var icon = "";
        }
        var tostObj = {
            heading:notification.title,
            text:notification.text,
            showHideTransition: 'slide',
            position:'top-right',
            icon:icon
        };
        $.toast(tostObj);
    }


})();

(function ($) {
    
var a=window .location .href.split('/');
if(a[a.length-1]==='traininginfo'){


          $("#sadd").attr('disabled',true);
          $("#radd").attr('disabled',true);
          $("#padd").attr('disabled',true);

}

    //for making the adminlte leftmenu active seeing the current url
        var currentUrl = document.location.href;
        var a = $('ul.sidebar-menu').find('a[href*="'+currentUrl+'"]:first');
        $('ul.sidebar-menu').find('a').each(function(){
            var href = $(this).attr('href');
            if(currentUrl.indexOf(href) !==-1){
                $(this).parents('li.treeview').addClass('active');
                $(this).parent('li').addClass('active');
            }
        });
    //end of making adminlte leftmentu active functionality
}(jQuery));
    function toast(a){
     var tostObj = {
                heading:a.title,
                text:a.text,
                showHideTransition:'slide',
                 position:'top-right',
                icon:a.title
               };

             $.toast(tostObj);


      return true;
    }


function sequentialAjax(id){
 
  $.ajax({

    method:'get',
    url:baseUrl+"/traininginfo/edit/"+id,
    success:function(response){        

    var tInfo=response.trainingInfoEdit;
        $("#id_edit").val(1);
 
      $("#id_now").val(tInfo[0].id);
      $("#id_no").val(tInfo[0].id);
      
      $("#name").val(tInfo[0].name);
      $("#organized_for").val(tInfo[0].organized_for);
      $("#component").val(tInfo[0].component);
      $("#activity").val(tInfo[0].activity);
      $("#immediate_result").val(tInfo[0].immediate_result);
      $("#training_type").val(tInfo[0].training_type);
      $("#training_category").val(tInfo[0].training_category);
      $("#training_topic").val(tInfo[0].training_topic);
      $("#description").val(tInfo[0].description);
      $("#start_date").val(tInfo[0].start_date);
      $("#end_date").val(tInfo[0].end_date);
      $("#venue").val(tInfo[0].venue);
      $("#remarks").val(tInfo[0].remarks);
       $("#target").val(tInfo[0].target);
        $("#achievement").val(tInfo[0].achievement);
      
       // do for all and also set id
        $("#staff-div").show();
              $("#resource-div").show();
              $("#participant-div").show();
       
      
    var sInfo=response.staffInfoEdit;
    staffArray=[];
      if(sInfo.length>0){
       staffArray=sInfo;
    tableShowStaff("staff_table");
}else{
 $("#staff_table_show").find("tr:gt(0)").remove();
   $("#staff_table").hide()


}
var rInfo=response.resourcePersonInfoEdit;
   resourcePersonArray=[];
   //alert(resourcePersonArray);
if(rInfo.length>0){
       resourcePersonArray=rInfo;
    tableShowResource("resource_person_table");
}else{
 $("#resource_person_table_show").find("tr:gt(0)").remove();
   $("#resource_person_table").hide()
}


var pInfo=response.participantInfoEdit;
//alert(pInfo);
   participantArray=[];
      if(pInfo.length>0){
        //alert(pInfo.length);
        participantArray=pInfo;
     tableShowParticipant("participant_table");
     console.log(participantArray);
 }else{
    $("#participant_table_show").find("tr:gt(0)").remove();
    $("#participant_table").hide()


 }

 $("#id_edit").val(0);
    },
    fail:function(){
        alert("failed");
    }
});

}

function editTrainingAll(id){
    ajaxCall(0,"traininginfo",0,id);

}




          <?php if(Session::has('fullname')):?>
$("#user-fullname").text('<?php echo Session::get('fullname');?>');

  <?php endif; ?>

  
 
</script>
</body>
</html>
