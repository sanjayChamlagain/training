<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Designation;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;
//repo test
class DesignateController extends Controller {

    public function index() {

    
      return view('designation.index');
    
    }
   

  

    public function creates(Request $request){
      $coun = new Designation();
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
        
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $name=$request->input("name",null);
         if($name==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name is required."]);
        }
        $level=$request->input("level",null);
         if($level==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Level is required."]);
        }
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
      }
    }

   

    public function edits($id){
      $coun = Designation::find($id,['id','name','level','approved','disabled']);
      return $coun;

    }

    

    public function updates(Request $request,$id){
       $coun = Designation::find($id);
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $name=$request->input("name",null);
         if($name==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name is required."]);
        }
        $level=$request->input("level",null);
         if($level==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Level is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update"]);
      }
    }

    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
    if($search==null){
       return $countries = DB::table('Designation')->paginate($entry,['*'],'page', $page );

      // $table=$this->getData($countries);
     }
     else{

       $countries=DB::table('Designation')->where('name', 'LIKE', "%$search%")->orwhere('level','LIKE',"%$search%")->paginate($entry,['*'],'page', $page );
       return $countries;
     }

       return $table.$countries->links();


    //    return Datatables::of($countries)->addColumn('action', function ($countries) {
      //          return '<a  href="javascript:void(0)" onClick="edits('.$countries->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
                  //      '&nbsp;&nbsp;<a href="javascript:void(0)" onClick="deletes('.$countries->id.')" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
      //      })->make();
    }

   

    public function deletes($id){
        $coun = Designation::find($id);
        try{
        $coun->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }
}
