<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainingtype;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;
//repo test
class TrainingtypeController extends Controller {

    public function index() {

    
      return view('training_type.index',['comp'=>$this->getComps(),'act'=>$this->getActs(),'sact'=>$this->getSacts()]);
    
    }
      public function getComps(){
        return \App\Project::select('id','name')->get();
    }

  public function getActs(){
        return \App\Activity::select('id','activity')->get();
    }
   
 public function getSacts(){
        return \App\SubActivity::select('id','subactivity')->get();
    }
   public function getActivity($id){
        return \App\Activity::select('id','activity')->where('component_id','=',$id)->get();

   }

  public function getSubActivity($id){
return \App\SubActivity::select('id','subactivity')->where('activity_id','=',$id)->get();


  }

    public function creates(Request $request){
      $coun = new Trainingtype();
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
        
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
            $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $comp=$request->input("component_id",null);
         if($comp==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* component is required."]);
       }
       $act=$request->input("activity_id",null);
         if($act==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Activity is required."]);
       }
       $sact=$request->input("subactivity_id",null);
         if($sact==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Sub-Activity is required."]);
       }
       
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
      }
    }
    

   

    public function edits($id){
      $coun = Trainingtype::find($id,['id','name_en','name_np','component_id','activity_id','subactivity_id']);
      return $coun;

    }

    

    public function updates(Request $request,$id){
       $coun = Trainingtype::find($id);
      if($coun->validate($request->all())){
          $coun->fill($request->all());
          $coun->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
     }else{
            $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $comp=$request->input("component_id",null);
         if($comp==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* component is required."]);
       }
       $act=$request->input("activity_id",null);
         if($act==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Activity is required."]);
       }
       $sact=$request->input("subactivity_id",null);
         if($sact==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Sub-Activity is required."]);
       }
       
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Error to save data"]);
      }
    }
    

    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     
       if($page==null){
          $page=1;
        }
    if($search==null){
       // return $train = DB::table('training_type')->paginate($entry,['*'],'page', $page );

      // $table=$this->getData($countries);

       $acts = DB::table('training_type')->select(['training_type.id', 'training_type.name_en','project_activity.activity as pactivity', 'project_component.name as cname_en','project_subactivity.subactivity'])
        ->join('project_component','training_type.component_id','=','project_component.id')
        ->join('project_activity','training_type.activity_id','=','project_activity.id')
        ->join('project_subactivity','training_type.subactivity_id','=','project_subactivity.id')
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
     }
     else{

        $acts = DB::table('training_type')->select(['training_type.id', 'training_type.name_en','project_activity.activity as pactivity', 'project_component.name as cname_en','project_subactivity.subactivity'])
        ->join('project_component','training_type.component_id','=','project_component.id')
        ->join('project_activity','training_type.activity_id','=','project_activity.id')
        ->join('project_subactivity','training_type.subactivity_id','=','project_subactivity.id')
       ->where('name_en', 'LIKE', "%$search%")
       ->orwhere('name_np','LIKE',"%$search%")->paginate($entry,['*'],'page', $page );
       return $acts;
     }

       


    }

   

    public function deletes($id){
        $coun = Trainingtype::find($id);
        try{
        $coun->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }
}
