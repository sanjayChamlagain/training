<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\ParticipantInfo;
use App\District;
use App\TrainingInfo;
use Session;
use App\Log;
use Illuminate\Support\Facades\Hash;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class eventController extends Controller {
 public function index(){
      return view('events.index',['train'=>$this->getTrain()]);
     
    }


    public function viewuserlist($id) {

      $m = DB::table('participant_info')->where('gender','=','Men')->where('training_id','=',$id)->count();
 $f = DB::table('participant_info')->where('gender','=','Women')->where('training_id','=',$id)->count();
 $bc = DB::table('participant_info')->where('caste','=','Brahmin/Chhetri')->where('training_id','=',$id)->count();
 $dalit = DB::table('participant_info')->where('caste','=','Dalit')->where('training_id','=',$id)->count();
 $ind = DB::table('participant_info')->where('caste','=','Indigenous Nationalities')->where('training_id','=',$id)->count();
 $newar = DB::table('participant_info')->where('caste','=','Newar')->where('training_id','=',$id)->count();
 $muslim = DB::table('participant_info')->where('caste','=','Muslim')->where('training_id','=',$id)->count();
 $other = DB::table('participant_info')->where('caste','=','Others')->where('training_id','=',$id)->count();
 $age1 = DB::table('participant_info')->where('age','=','15-19')->where('training_id','=',$id)->count();
 $age2 = DB::table('participant_info')->where('age','=','20-24')->where('training_id','=',$id)->count();
 $age3 = DB::table('participant_info')->where('age','=','25-29')->where('training_id','=',$id)->count();
 $age4 = DB::table('participant_info')->where('age','=','30-34')->where('training_id','=',$id)->count();
 $age5 = DB::table('participant_info')->where('age','=','35-39')->where('training_id','=',$id)->count();
 $age6 = DB::table('participant_info')->where('age','=','40+')->where('training_id','=',$id)->count();
 $lv1 = DB::table('participant_info')->where('level','=','1')->where('training_id','=',$id)->count();
  $lv2 = DB::table('participant_info')->where('level','=','2')->where('training_id','=',$id)->count();
   $lv3 = DB::table('participant_info')->where('level','=','3')->where('training_id','=',$id)->count();
    $lv4 = DB::table('participant_info')->where('level','=','4')->where('training_id','=',$id)->count();
     

      // $participant = DB::table('participant_info')->select(['participant_info.id','participant_info.name','district.name as district','participant_info.gender','participant_info.phone','participant_info.email','designation.name as title','participant_info.agency','participant_info.level','gov_org.name_en as ministry','participant_info.age','participant_info.caste','participant_info.remarks'])
      // ->join('gov_org','participant_info.ministry','=','gov_org.id')
      // ->join('district','participant_info.district','=','district.id')
      //  ->join('designation','participant_info.title','=','designation.id')
      // ->where('participant_info.training_id','=',$id)->get();

     
     $name=TrainingInfo::find($id)->name;
  
      $traine=DB::table('training_info')->select(['training_info.name','training_info.target','training_info.achievement','training_info.start_date','training_info.end_date','training_info.venue','project_component.name as componentName','project_activity.activity as act','project_subactivity.subactivity as subact','training_type.name_en as eventType','gov_org.name_en as organizer'])->join('project_component','training_info.component','=','project_component.id')->join('project_activity','training_info.activity','=','project_activity.id')->join('project_subactivity','training_info.immediate_result','=','project_subactivity.id')->join('training_type','training_info.training_type','=','training_type.id')->join('gov_org','training_info.organized_for','=','gov_org.id')->where('training_info.id','=',$id)->get();

          // $("#reg-form").show();
      // foreach($traine as $train){
  $tab_text="<table><thead><tr><td colspan='33'><h3 style='font-weight:bold;'>NEPAL USAID/ Public Financial Management Stregnthening Project</h3></td></tr><tr><td colspan='33'><h4 style='font-weight:bold;'>Tracking of ".$name." Information with GESI disaggregation data</h4></td></tr></thead>";
      

  $tab=$tab_text." <tbody ><tr bgcolor='#87AFC6'><th rowspan='2'> SN.</th><th rowspan='2'>IR(Immediate Result)</th><th rowspan='2'>IND Number</th><th rowspan='2'>Types of Event</th><th rowspan='2'>Name of Event</th><th colspan='2'>Dates Of Event</th><th rowspan='2'>Target</th><th rowspan='2'>Total Achievement</th><th rowspan='2'>Women</th><th rowspan='2'>Men</th><th rowspan='2'>Brahmin/ Chhetri</th><th rowspan='2'>Dalit</th><th rowspan='2'>Indigenous Nationalities</th><th rowspan='2'>Newar</th><th rowspan='2'>Muslim</th><th rowspan='2'>Others</th><th colspan='6'>Youth(15-34) years</th><th colspan='4'>Beneficaries by Positions</th><th colspan='4'>Ministry/Agency</th><th rowspan='2'>Variance</th><th rowspan='2'>Justification if Target is not fulfilled or approach is changed.</th></tr><tr><th>Start Date</th><th>End Date</th><th>15-19</th><th>20-24</th><th>25-29</th><th>30-34</th><th>35-39</th><th>40 and above</th><th>1st Class</th><th>2nd Class</th><th>3rd Class</th><th>Assistant</th><th>Name</th><th >CLO</th><th>District</th><th>Total</th></tr>";

 
 
  $i=1;
   foreach($traine as $p){
$tab.="<tr><td>".$i."</td><td>".$p->act."</td><td>".$p->subact."</td><td>".$p->eventType."</td><td>".$p->name."</td><td>".$p->start_date."</td><td>".$p->end_date."</td><td>".$p->target."</td><td>".$p->achievement."</td><td>".$f."</td><td>".$m."</td><td>".$bc."</td><td>".$dalit."</td><td>".$ind."</td><td>".$newar."</td><td>".$muslim."</td><td>".$other."</td><td>".$age1."</td><td>".$age2."</td><td>".$age3."</td><td>".$age4."</td><td>".$age5."</td><td>".$age6."</td><td>".$lv1."</td><td>".$lv2."</td><td>".$lv3."</td><td>".$lv4."</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
$i++;
   }         
$tab.="</tbody></table>";
return view('events.view',['tab'=>$tab]);
     }
    
  
   
public function getTrain(){
   return \App\TrainingInfo::select('id','name')->get();
}


  }
