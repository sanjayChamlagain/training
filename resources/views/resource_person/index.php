<?php if(!Request::ajax()){ ?>
<?php include(resource_path().'/views/sections/header.php');?>
<?php include(resource_path().'/views/sections/leftmenu.php');?>
<div  id="body" class="content-wrapper">
<?php  } ?>
<!-- Content Wrapper. Contains page content -->

    <section class="content-header">
        <h1>
            Resource Person
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-6">

                    <form class="form-horizontal" id="form" onsubmit="formSubmit(event)" method="post" action="">

                        <input type="hidden" id="id" name="id" value="">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box col-md-4">
                        <div class="box-header with-border">
                            <h3 class="box-title">Resource Person</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                           

                            <div class="form-group">
                                <label for="name_en" class="col-sm-3 control-label">Name(english)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_en" placeholder="Name(english)" type="text" name="name_en" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Name(Nepali)</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="name_np" placeholder="Name(Nepali)" type="text" name="name_np" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="gender" name="gender">      
                                      <option id="select" value="">Select one..</option>
                                        
                                          
                                                                 
                                  
                                                   <option value="Men">Men</option> 
                                                  <option value="Women" >Women</option> 
                                                  
                                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Caste</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="caste" name="caste">      
                                      <option id="select" value="">Select one..</option>
                                        
                                          
                                                                 
                                  
                                                   <option value="brahmin">Brahmin</option> 
                                                  <option value="chhetri" >Chhetri</option> 
                                                   <option value="dalit" >Dalit</option> 
                                                   <option value="indigenous nationalities" >Indigenous Nationalities</option>
                                                   <option value="muslim" >Muslim</option>
                                                   <option value="newar" >Newar</option>
                                                   <option value="others" >Others</option>
                                                
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Age</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="age" placeholder="age" type="text" name="age" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Expertise</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="expertise" placeholder="expertise" type="text" name="expertise" value="">
                                </div>
                            </div>

                             <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Organization</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="organization_id" name="organization_id">      
                                      <option id="select" value="">Select one..</option>
                                        
                                            <?php foreach($orgs as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name_en;?></option>
                                                
                                            <?php endforeach;?>
                                                                 
                                  
                                                  <!-- <option value="yes" selected="selected">Yes</option> -->
                                                  <!-- <option value="no" >No</option> -->
                                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Designation</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="designation_id" name="designation_id">      
                                      <option id="select" value="">Select one..</option>
                                        
                                            <?php foreach($desg as $c):?>
                                                
                                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                                
                                            <?php endforeach;?>
                                                                 
                                  
                                                  <!-- <option value="yes" selected="selected">Yes</option> -->
                                                  <!-- <option value="no" >No</option> -->
                                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Phone</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="phone" placeholder="phone" type="text" name="phone" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Mobile</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="mobile" placeholder="mobile" type="text" name="mobile" value="">
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="email" placeholder="Email" type="text" name="email" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name_np" class="col-sm-3 control-label">Postal Address</label>

                                <div class="col-sm-9">
                                    <input class="form-control" id="postal" placeholder="postal address" type="text" name="postal" value="">
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Approved</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="approved" name="approved">                                    
                                  
                                                  <option value="yes" selected="selected">Yes</option>
                                                  <option value="no" >No</option>
                                                
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-sm-3 control-label">Disabled</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="disabled" name="disabled">                                    
                                  
                                                  <option value="no" selected="selected">No</option>
                                                  <option value="yes" >Yes</option>
                                                
                                    </select>
                                </div>
                            </div>









                        </div>
                        <div class="box-footer">
                            <button type="submit" id="submit" class="btn btn-default">Create</button>
                            <button type="reset" onClick="resetForm()" class="btn btn-info pull-right">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
    <div class="box col-md-4">
        <div class="box-header with-border">
            <h3 class="box-title">Resource Person</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
         <div id="table" class="box-body">
            <form id="tableform" >
            <b>SHOW</b><select id="selectentry" onchange="table(event)">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
                <b>ENTRIES</b>

                <div style="float:right">
          <form id="srch" name="srch">
        <input  id="searchfill" placeholder="  search here" type="text" name="search">
        <button type="submit" id="searchbtn" name="submit">Search</button>
      </form>
    </div>

            <div id="showtable" class="box-body">
                <table id="organization-table" class="table table-striped table-bordered">
                  <tr><th>SN</th><th>Name(en)</th><th>Gender</th><th>Expertise</th><th>Mobile</th><th>ACTIONS</th></tr>

                    </table>
            </div>
          <div id="pagg">

            <ul class="pagination pagination-sm">
            </ul>

              </div>
        </form>


        </div>
    </div>
</div>

        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
var baseurl="<?php echo url('/');?>";
// var baseurl=window.location.hostname;


function table(){

    var entry=$("#selectentry").val();

var search=$("#searchfill").val();
 if(($("#searchfill").val())!=""){
    var search=$("#searchfill").val();
  }
var index= $("body #pagg ul li.active").text();
if(index!=null){
 var page=index;
}
$.ajax({

    method:'get',
    url:baseurl+"/resource/list?entry="+entry+"&page="+page+"&search="+search,
    success:function(response){
        createTable(response);

    },
    fail:function(){
        alert("failed");
    }
});



}



function searchClicked(e){
e.preventDefault();
var search=$("#searchfill").val();
  var entry=$("#selectentry").val();
$.ajax({

    method:'get',
    url:baseurl+"/resource/list?entry="+entry+"&search="+search,
    success:function(response){
      createTable(response);
    },
    fail:function(){
        alert("failed");
    }
});
}

function createTable(resp){

    var t=document.getElementById("organization-table");
   $("body #organization-table").find("tr:gt(0)").remove();
var sn=resp.from;
    var rowCount=1;
    var data=resp.data;
    for(var i in data){
    var row=t.insertRow(rowCount);

    row.insertCell(0).innerHTML=sn;
    row.insertCell(1).innerHTML=data[i].name_en;
    row.insertCell(2).innerHTML=data[i].gender;
    row.insertCell(3).innerHTML=data[i].expertise;
    row.insertCell(4).innerHTML=data[i].mobile;
    row.insertCell(5).innerHTML="<a href='javascript:void(0)' onclick='edit("+data[i].id+")' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i>Edit</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick='delt("+data[i].id+")' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-trash'></i>Delete</a>";
        rowCount++;
        sn++;
   }
   callForPagination(resp);
     return true;
}

   function formSubmit(e){
    // alert(baseurl);
    e.preventDefault();
    if( $('#id').val()==""){
      // var url="http://localhost/pfmsp/resource/creates";
            var url=baseurl+"/resource/creates";
            }
           else{
            var url=baseurl+"/resource/updates/"+ $('#id').val();
            }
       $.ajax({
           method:"POST",
           url:url,
           data:$("#form").serialize(),
           success:function(resp){
            var a =JSON.parse(resp);
             if(a.status==1){
             $('#caste').val("");
              $('#expertise').val("");
              $('#postal').val("");
             $('#name_en').val("");
             $('#name_np').val("");
             $("#age").val("");
             $('#gender').val("");
             $('#designation_id').val("");
             $("#phone").val("");
             $('#mobile').val("");
             $('#email').val("");
             $("#organization_id").val("");
            
             $("#id").val("");
             $("#submit").text("Create");
           }
               table();
                if(a.records){
                 var records=a.records;
                   a.records.delete;
                     toast(a);
              $("body .pagination li a").trigger("click",records);

                 }else{
                   toast(a);
                   table();
               }


        },
          fail:function(){

          alert("failed");
     }
      });
}

function edit(id){
    $("#submit").text("update");
     $.ajax({
     method:'get',
       url:baseurl+"/resource/edits/"+id,
     success:function(resp){

             $('#id').val(resp.id);
            $("#age").val(resp.age);
             $('#name_en').val(resp.name_en);
             $('#name_np').val(resp.name_np);
             $("#postal").val(resp.postal);
               $("#expertise").val(resp.postal);
               $("#expertise").val(resp.expertise);
               $("#caste").val(resp.caste);
             $('#gender').val(resp.gender);
             $('#designation_id').val(resp.designation_id);
             $("#phone").val(resp.phone);
             $('#mobile').val(resp.mobile);
             $('#email').val(resp.email);
             $("#organization_id").val(resp.organization_id);
             $('#approved').val(resp.approved);
             $('#disabled').val(resp.disabled);
         },
        fail:function(){

         }
     });

   }


function delt(id){
    var conf=confirm("Are you sure you want to delete?");
   if(conf){
    $.ajax({
        method:'get',
      url:baseurl+"/resource/deletes/"+id,
        success:function(resp){
             var a =JSON.parse(resp);
             toast(a);
               table();
        },

        fail:function(){
            alert("Fail");

        }
    });
}
}
function resetForm(){
  $('#id').val('');
  $('#submit').text('Create');
}
</script>


<?php if(!Request::ajax()){ ?>
</div>
<?php include(resource_path().'/views/sections/footer.php');
 } ?>
