<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public $timestamps=false;
  protected $table = 'participant';
    protected $fillable = ['name','gender_id','caste_id','age_id','designation_id','organization','district_id','phone','email','bank','account'];
    protected $rules = [
        'name' => 'string',
        'gender_id' => 'integer','caste_id' => 'integer','age_id' => 'integer','designation_id' => 'integer','organization' => 'string','district_id' => 'integer','phone' => 'string','email' => 'string','bank' => 'string','account' => 'nullable|string',
    ];
}