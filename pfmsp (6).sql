-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2017 at 10:33 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pfmsp`
--

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(5) NOT NULL,
  `name` varchar(200) NOT NULL,
  `level` int(5) NOT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `name`, `level`, `approved`, `disabled`) VALUES
(1, 'Chief of the party', 1, 'no', 'no'),
(2, 'Deputy Chief of the Party', 3, 'yes', 'no'),
(3, 'Expert, Public Finance', 4, 'yes', 'no'),
(4, 'Monitoring and Evaluation Specialist', 5, 'no', 'no'),
(6, 'Senior Finance and Compliance Manager', 7, 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`) VALUES
(2, 'Achham'),
(3, 'Baglung'),
(4, 'Arghakhachi'),
(5, 'Baitadi'),
(6, 'Bajhang'),
(7, 'Bajura'),
(8, 'Banke'),
(9, 'Bara'),
(10, 'Bardiya'),
(11, 'Bhaktapur'),
(12, 'Bhojpur'),
(13, 'Chitawan'),
(14, 'Dadeldhura'),
(15, 'Dailekh'),
(16, 'Dang'),
(17, 'Darchula'),
(18, 'Dhading'),
(19, 'Dhankuta'),
(20, 'Dhanusha'),
(21, 'Dolakha'),
(22, 'Dolpa'),
(23, 'Doti'),
(24, 'Gorkha'),
(25, 'Gulmi'),
(26, 'Humla'),
(27, 'Ilam'),
(28, 'Jajarkot'),
(29, 'Jhapa'),
(30, 'Jumla'),
(31, 'Kailali'),
(32, 'Kalikot'),
(33, 'Kanchanpur'),
(34, 'Kapilvastu'),
(35, 'Kaski'),
(36, 'Kathmandu'),
(37, 'Kavrepalanchowk'),
(38, 'Khotang'),
(39, 'Lalitpur'),
(40, 'Lamjung'),
(41, 'Mahottari'),
(42, 'Makawanpur'),
(43, 'Manang'),
(44, 'Morang'),
(45, 'Mugu'),
(46, 'Mustang'),
(47, 'Myagdi'),
(48, 'Nawalparasi'),
(49, 'Nuwakot'),
(50, 'Okhaldhunga'),
(51, 'Palpa'),
(52, 'Panchthar'),
(53, 'Parbat'),
(54, 'Parsa'),
(55, 'Pyuthan'),
(56, 'Ramechhap'),
(57, 'Rasuwa'),
(58, 'Rautahat'),
(59, 'Rolpa'),
(60, 'Rukum'),
(61, 'Rupandehi'),
(62, 'Salyan'),
(63, 'Sankhuwasava'),
(64, 'Saptari'),
(65, 'Sarlahi'),
(66, 'Sidhupalchowk'),
(67, 'Sindhuli'),
(68, 'Siraha'),
(69, 'Solukhumbu'),
(70, 'Sunsari'),
(71, 'Surkhet'),
(72, 'Syangja'),
(73, 'Tanahun'),
(74, 'Taplejung'),
(75, 'Terhathum'),
(76, 'Udayapur');

-- --------------------------------------------------------

--
-- Table structure for table `focal_person`
--

CREATE TABLE `focal_person` (
  `id` int(10) NOT NULL,
  `gov_org_id` int(10) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) NOT NULL,
  `post_id` int(10) NOT NULL,
  `phone` int(15) DEFAULT NULL,
  `mobile` int(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `focal_person`
--

INSERT INTO `focal_person` (`id`, `gov_org_id`, `name_en`, `name_np`, `post_id`, `phone`, `mobile`, `email`, `approved`, `disabled`) VALUES
(1, 1, 'ram kumar', 'ramkumar', 1, 987654, 9876543, NULL, 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `gov_org`
--

CREATE TABLE `gov_org` (
  `id` int(10) NOT NULL,
  `parent_org` int(10) DEFAULT NULL,
  `orgtype_id` int(10) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) NOT NULL,
  `district` varchar(100) NOT NULL,
  `postal` varchar(100) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `approved` varchar(10) DEFAULT NULL,
  `disabled` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gov_org`
--

INSERT INTO `gov_org` (`id`, `parent_org`, `orgtype_id`, `name_en`, `name_np`, `district`, `postal`, `phone`, `fax`, `email`, `website`, `approved`, `disabled`) VALUES
(1, NULL, 1, 'Ministry of Finance', 'Ministry of Finance', 'jhapa', NULL, NULL, NULL, NULL, NULL, 'yes', 'no'),
(2, NULL, 2, 'Ministry of Education', 'Ministry of Education', 'ilam', NULL, NULL, NULL, NULL, NULL, 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `gov_post`
--

CREATE TABLE `gov_post` (
  `id` int(10) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_np` varchar(100) NOT NULL,
  `level` int(5) NOT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gov_post`
--

INSERT INTO `gov_post` (`id`, `name_en`, `name_np`, `level`, `approved`, `disabled`) VALUES
(1, 'secretary', 'secratary', 1, 'no', 'no'),
(2, 'Minister', 'Minister', 2, 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `gov_type`
--

CREATE TABLE `gov_type` (
  `id` int(10) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) NOT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gov_type`
--

INSERT INTO `gov_type` (`id`, `name_en`, `name_np`, `approved`, `disabled`) VALUES
(1, 'Ministry', 'Ministry', 'yes', 'no'),
(2, 'Department;;', 'Department', 'yes', 'no'),
(4, 'central', 'kendriya', 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `id` int(5) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) NOT NULL,
  `org_type` varchar(200) NOT NULL,
  `district` varchar(50) NOT NULL,
  `vdc` varchar(50) NOT NULL,
  `ward` varchar(20) DEFAULT NULL,
  `street` varchar(20) DEFAULT NULL,
  `house_no` varchar(20) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `phone` int(13) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `name_en`, `name_np`, `org_type`, `district`, `vdc`, `ward`, `street`, `house_no`, `latitude`, `longitude`, `phone`, `fax`, `email`, `website`) VALUES
(3, 'Public Financial Management Strengthening Project', 'Public Financial Management Strengthening Project', 'Central Office', 'ktm', 'odihfi', 'oihfio', 'oihfoi', 'oihfoi', 'ihfbio', 'oihfoi', 9876543, 'oishfo', 'ihcis', 'iohfoi');

-- --------------------------------------------------------

--
-- Table structure for table `org_type`
--

CREATE TABLE `org_type` (
  `id` int(5) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_np` varchar(100) NOT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `org_type`
--

INSERT INTO `org_type` (`id`, `name_en`, `name_np`, `approved`, `disabled`) VALUES
(3, 'Central Office', 'Central Office', 'yes', 'no'),
(4, 'district office', 'ff', 'yes', 'no'),
(5, 'zonal', 'j', 'yes', 'no'),
(6, 'regional', 'jhjh', 'yes', 'no'),
(7, 'sss', 'ss', 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `participant_info`
--

CREATE TABLE `participant_info` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `caste` varchar(100) NOT NULL,
  `age` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `ministry` varchar(2540) NOT NULL,
  `agency` varchar(250) NOT NULL,
  `district` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `training_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participant_info`
--

INSERT INTO `participant_info` (`id`, `name`, `gender`, `caste`, `age`, `title`, `level`, `ministry`, `agency`, `district`, `phone`, `email`, `remarks`, `training_id`) VALUES
(74, 'Bimala sharma', 'Women', 'Brahmin/chhetri', '20-24', '1', '1', '1', 'Central/ Ministry Level', '13', '987654', 'bimala@cc.org', 'yes', 27),
(75, 'Ram kumar bk', 'Men', 'Dalit', '25-29', '2', '4', '2', 'Regional/ State Level', '36', '98876544', 'ram@r.com', 'nntt', 27),
(102, 'Mina shrestha', 'Women', 'Newar', '25-29', '2', '3', '2', 'edu', '16', '987666', 'mina@min', 'nb', 28),
(103, 'Ramesh  paudel', 'Men', 'Brahmin/chhetri', '35-39', '6', '1', '2', 'fin', '11', '98765432', 'ramesh@hh', 'y', 28);

-- --------------------------------------------------------

--
-- Table structure for table `project_activity`
--

CREATE TABLE `project_activity` (
  `id` int(10) NOT NULL,
  `component_id` int(10) NOT NULL,
  `activity` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_activity`
--

INSERT INTO `project_activity` (`id`, `component_id`, `activity`, `description`) VALUES
(7, 6, 'IR 1:Improved Budget Planning and Execution Capacity of GON', 'IR1'),
(8, 7, 'IR2: Improved Control Environment and Procurement Capacity', 'IR 2'),
(9, 8, 'IR3: Strengthened Financial Management Functions of National Reconstruction Authority', 'IR 3');

-- --------------------------------------------------------

--
-- Table structure for table `project_component`
--

CREATE TABLE `project_component` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_component`
--

INSERT INTO `project_component` (`id`, `name`, `description`, `approved`, `disabled`) VALUES
(6, 'Component A', 'this is component A', 'yes', 'no'),
(7, 'Component B', 'this is component B', 'yes', 'no'),
(8, 'Component C', 'this is component C', 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `project_subactivity`
--

CREATE TABLE `project_subactivity` (
  `id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `subactivity` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_subactivity`
--

INSERT INTO `project_subactivity` (`id`, `component_id`, `activity_id`, `subactivity`, `description`) VALUES
(3, 6, 7, 'IR 1.1 improved', 'IR 1.1 subactivity'),
(4, 7, 8, 'IR 2.1 control......', 'IR 2.1 sub activity of B'),
(5, 8, 9, 'IR 3.1 managerial.......', 'IR 3.1 subactivity');

-- --------------------------------------------------------

--
-- Table structure for table `resource_person`
--

CREATE TABLE `resource_person` (
  `id` int(10) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_np` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `caste` varchar(100) NOT NULL,
  `age` int(5) NOT NULL,
  `expertise` varchar(50) NOT NULL,
  `organization_id` int(10) NOT NULL,
  `designation_id` int(10) NOT NULL,
  `phone` int(15) DEFAULT NULL,
  `mobile` int(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `postal` varchar(50) DEFAULT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource_person`
--

INSERT INTO `resource_person` (`id`, `name_en`, `name_np`, `gender`, `caste`, `age`, `expertise`, `organization_id`, `designation_id`, `phone`, `mobile`, `email`, `postal`, `approved`, `disabled`) VALUES
(7, 'rakesh', 'rakesh', 'Men', 'brahmin', 23, 'data', 1, 1, 987654, 9876543, NULL, NULL, 'yes', 'no'),
(8, 'sarmila', 'sarmila', 'Women', 'newar', 31, 'data entry', 1, 4, NULL, 987654, NULL, NULL, 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `resource_person_info`
--

CREATE TABLE `resource_person_info` (
  `id` int(255) NOT NULL,
  `rPersonId` int(255) NOT NULL,
  `jobDes` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `training_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource_person_info`
--

INSERT INTO `resource_person_info` (`id`, `rPersonId`, `jobDes`, `remarks`, `training_id`) VALUES
(19, 7, '', '', 27),
(21, 8, 'nn', '', 28),
(22, 7, 'mm', 'bbb', 32);

-- --------------------------------------------------------

--
-- Table structure for table `staff_info`
--

CREATE TABLE `staff_info` (
  `id` int(5) NOT NULL,
  `staff_id` varchar(10) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `name_np` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` date DEFAULT NULL,
  `designation_id` int(5) NOT NULL,
  `phone` int(15) DEFAULT NULL,
  `mobile` int(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `organization_id` int(5) NOT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_info`
--

INSERT INTO `staff_info` (`id`, `staff_id`, `name_en`, `name_np`, `gender`, `dob`, `designation_id`, `phone`, `mobile`, `email`, `organization_id`, `approved`, `disabled`) VALUES
(1, '101', 'hariuuuuuuu', 'hari', 'Men', NULL, 1, 9876543, NULL, NULL, 3, 'yes', 'no'),
(2, '1', 'ssyy', 'ss', 'Women', '2017-11-15', 4, 9823456, 34667688, 'ghklk@gmail.com', 3, 'yes', 'no'),
(3, '111', 'bishal thapa', 'bishal', 'Men', '1990-12-05', 1, 98765444, NULL, NULL, 3, 'yes', 'no'),
(4, '1', 'Admin', 'ADMIN', 'Women', '2017-12-14', 1, 11111, 11111, 'qq@qq.com', 3, 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `staff_information`
--

CREATE TABLE `staff_information` (
  `id` int(11) NOT NULL,
  `staffId` int(255) NOT NULL,
  `responsibility` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `training_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_information`
--

INSERT INTO `staff_information` (`id`, `staffId`, `responsibility`, `remarks`, `training_id`) VALUES
(159, 3, 'mm', 'nm', 28),
(166, 2, 'dd', 'ee', 32),
(167, 1, 'mmmmmm', 'ssssii', 27),
(168, 2, '', '', 27),
(169, 3, 'tt', 'uu', 35);

-- --------------------------------------------------------

--
-- Table structure for table `training_category`
--

CREATE TABLE `training_category` (
  `id` int(10) NOT NULL,
  `event_id` int(20) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_category`
--

INSERT INTO `training_category` (`id`, `event_id`, `name_en`, `name_np`) VALUES
(5, 9, 'it training', NULL),
(6, 10, 'lms training', NULL),
(7, 11, 'TOT', NULL),
(8, 12, 'data entry', NULL),
(9, 13, 'analysis', NULL),
(10, 14, 'accounting', NULL),
(11, 15, 'lmbis orientation', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `training_info`
--

CREATE TABLE `training_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `organized_for` int(255) NOT NULL,
  `component` int(255) NOT NULL,
  `activity` int(255) NOT NULL,
  `immediate_result` int(10) NOT NULL,
  `training_type` int(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `target` varchar(20) NOT NULL,
  `achievement` varchar(20) NOT NULL,
  `venue` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_info`
--

INSERT INTO `training_info` (`id`, `name`, `organized_for`, `component`, `activity`, `immediate_result`, `training_type`, `description`, `start_date`, `end_date`, `target`, `achievement`, `venue`, `remarks`) VALUES
(27, 'pfmsp training', 1, 6, 7, 3, 16, 'mnmn', '2017-12-06', '2017-12-13', '1', '2', 'Anamnagar, Kathmandu', 'yes'),
(28, 'IT', 2, 7, 8, 4, 16, 'aa', '2017-12-05', '2017-12-12', '4', '3', 'nnnn', 'aasss'),
(29, 'js training', 2, 6, 7, 3, 18, 'javascript code camp', '2017-12-13', '2017-12-20', '20', '25', 'saipal tech hall', 'gone cool'),
(30, '.net', 2, 6, 7, 3, 19, 'building apps', '2017-12-20', '2017-12-06', '50', '45', 'anamnagar', 'gone preety cool'),
(32, 'qq', 1, 6, 7, 3, 18, 'qq', '2017-12-01', '2018-01-17', '11', '22', 'ee', 'eee'),
(36, 'one day orientation on hygeine', 2, 6, 7, 3, 15, 'preety well done', '2017-12-13', '2017-12-13', '23', '25', 'anamnagar', 'good');

-- --------------------------------------------------------

--
-- Table structure for table `training_topic`
--

CREATE TABLE `training_topic` (
  `id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) NOT NULL,
  `approved` varchar(10) NOT NULL,
  `disabled` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training_type`
--

CREATE TABLE `training_type` (
  `id` int(10) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_np` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_type`
--

INSERT INTO `training_type` (`id`, `name_en`, `name_np`) VALUES
(15, 'Orientation', NULL),
(16, 'TrainingProcurmentAccounting', NULL),
(17, 'OrientationProcureAccount', NULL),
(18, 'Seminar', NULL),
(19, 'Workshop', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `flag` int(2) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `staff_id`, `password`, `flag`, `username`) VALUES
(11, 3, '$2y$10$RcK.8InDMtFK/Jr3RBlX5Oie9qF6So07PNG/a8z.i5L.xcHydZ6ly', 1, 'bishal@thapa'),
(12, 4, '$2y$10$tcUjfG4Fzndv4PcCJ6BZquuJN9tsAXJlDsGqlce5rAfURQmnQKJSi', 1, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `focal_person`
--
ALTER TABLE `focal_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gov_org_id` (`gov_org_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `gov_org`
--
ALTER TABLE `gov_org`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orgtype_id` (`orgtype_id`);

--
-- Indexes for table `gov_post`
--
ALTER TABLE `gov_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gov_type`
--
ALTER TABLE `gov_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org_type`
--
ALTER TABLE `org_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant_info`
--
ALTER TABLE `participant_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_id` (`training_id`);

--
-- Indexes for table `project_activity`
--
ALTER TABLE `project_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `component_id` (`component_id`);

--
-- Indexes for table `project_component`
--
ALTER TABLE `project_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_subactivity`
--
ALTER TABLE `project_subactivity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `component_id` (`component_id`),
  ADD KEY `activity_id` (`activity_id`);

--
-- Indexes for table `resource_person`
--
ALTER TABLE `resource_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `designation_id` (`designation_id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Indexes for table `resource_person_info`
--
ALTER TABLE `resource_person_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rPersonId` (`rPersonId`),
  ADD KEY `training_id` (`training_id`);

--
-- Indexes for table `staff_info`
--
ALTER TABLE `staff_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `desingnation_id` (`designation_id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Indexes for table `staff_information`
--
ALTER TABLE `staff_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_id` (`training_id`),
  ADD KEY `staff_id` (`staffId`);

--
-- Indexes for table `training_category`
--
ALTER TABLE `training_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `training_info`
--
ALTER TABLE `training_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organized_for` (`organized_for`),
  ADD KEY `component` (`component`),
  ADD KEY `activity` (`activity`),
  ADD KEY `training_type` (`training_type`),
  ADD KEY `immediate_result` (`immediate_result`);

--
-- Indexes for table `training_topic`
--
ALTER TABLE `training_topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `training_type`
--
ALTER TABLE `training_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staff_id` (`staff_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `focal_person`
--
ALTER TABLE `focal_person`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gov_org`
--
ALTER TABLE `gov_org`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gov_post`
--
ALTER TABLE `gov_post`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gov_type`
--
ALTER TABLE `gov_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `org_type`
--
ALTER TABLE `org_type`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `participant_info`
--
ALTER TABLE `participant_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `project_activity`
--
ALTER TABLE `project_activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `project_component`
--
ALTER TABLE `project_component`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `project_subactivity`
--
ALTER TABLE `project_subactivity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `resource_person`
--
ALTER TABLE `resource_person`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `resource_person_info`
--
ALTER TABLE `resource_person_info`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `staff_info`
--
ALTER TABLE `staff_info`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `staff_information`
--
ALTER TABLE `staff_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `training_category`
--
ALTER TABLE `training_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `training_info`
--
ALTER TABLE `training_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `training_topic`
--
ALTER TABLE `training_topic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `training_type`
--
ALTER TABLE `training_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `focal_person`
--
ALTER TABLE `focal_person`
  ADD CONSTRAINT `focal_person_ibfk_1` FOREIGN KEY (`gov_org_id`) REFERENCES `gov_org` (`id`),
  ADD CONSTRAINT `focal_person_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `gov_post` (`id`);

--
-- Constraints for table `gov_org`
--
ALTER TABLE `gov_org`
  ADD CONSTRAINT `gov_org_ibfk_1` FOREIGN KEY (`orgtype_id`) REFERENCES `gov_type` (`id`);

--
-- Constraints for table `participant_info`
--
ALTER TABLE `participant_info`
  ADD CONSTRAINT `participant_info_ibfk_1` FOREIGN KEY (`training_id`) REFERENCES `training_info` (`id`);

--
-- Constraints for table `project_activity`
--
ALTER TABLE `project_activity`
  ADD CONSTRAINT `project_activity_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `project_component` (`id`);

--
-- Constraints for table `project_subactivity`
--
ALTER TABLE `project_subactivity`
  ADD CONSTRAINT `project_subactivity_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `project_component` (`id`),
  ADD CONSTRAINT `project_subactivity_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `project_activity` (`id`);

--
-- Constraints for table `resource_person`
--
ALTER TABLE `resource_person`
  ADD CONSTRAINT `resource_person_ibfk_1` FOREIGN KEY (`designation_id`) REFERENCES `designation` (`id`),
  ADD CONSTRAINT `resource_person_ibfk_2` FOREIGN KEY (`organization_id`) REFERENCES `gov_org` (`id`);

--
-- Constraints for table `resource_person_info`
--
ALTER TABLE `resource_person_info`
  ADD CONSTRAINT `resource_person_info_ibfk_1` FOREIGN KEY (`rPersonId`) REFERENCES `resource_person` (`id`),
  ADD CONSTRAINT `resource_person_info_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `training_info` (`id`);

--
-- Constraints for table `staff_info`
--
ALTER TABLE `staff_info`
  ADD CONSTRAINT `staff_info_ibfk_1` FOREIGN KEY (`designation_id`) REFERENCES `designation` (`id`),
  ADD CONSTRAINT `staff_info_ibfk_2` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`);

--
-- Constraints for table `training_category`
--
ALTER TABLE `training_category`
  ADD CONSTRAINT `training_category_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `training_type` (`id`);

--
-- Constraints for table `training_info`
--
ALTER TABLE `training_info`
  ADD CONSTRAINT `training_info_ibfk_1` FOREIGN KEY (`organized_for`) REFERENCES `gov_org` (`id`),
  ADD CONSTRAINT `training_info_ibfk_2` FOREIGN KEY (`component`) REFERENCES `project_component` (`id`),
  ADD CONSTRAINT `training_info_ibfk_3` FOREIGN KEY (`activity`) REFERENCES `project_activity` (`id`),
  ADD CONSTRAINT `training_info_ibfk_6` FOREIGN KEY (`training_type`) REFERENCES `training_type` (`id`),
  ADD CONSTRAINT `training_info_ibfk_7` FOREIGN KEY (`immediate_result`) REFERENCES `project_subactivity` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
