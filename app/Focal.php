<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Focal extends BaseModel
{
    protected $table='focal_person';
    protected $fillable=['gov_org_id','name_en','name_np','post_id','phone','mobile','email','approved','disabled'];
    protected $rules=[
      'gov_org_id'=>'integer',
      
      'name_en'=>'string|required',
      'name_np'=>'string',
      
      'post_id'=>'integer',
      
      'phone'=>'nullable|integer',
      'mobile'=>'nullable|integer',
      'email'=>'nullable|string',
     
      'approved'=>'string',
      'disabled'=>'string',


    ];

}
