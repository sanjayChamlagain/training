<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends BaseModel
{
    protected $table='organization';
    protected $fillable=['code','name_en','name_np','org_type','district','vdc','ward','street','house_no','latitude','longitude','phone','fax','email','website'];
    protected $rules=[
      'code'=>'string',
      'name_en'=>'string|required',
      'name_np'=>'string',
      'org_type'=>'string|required',
      'district'=>'string',
      'vdc'=>'string',
      'ward'=>'nullable|string',
      'street'=>'nullable|string',
      'house_no'=>'nullable|string',
      'latitude'=>'nullable|string',
      'longitude'=>'nullable|string',
      'phone'=>'nullable|integer',
      'fax'=>'nullable|string',
      'email'=>'nullable|string',
      'website'=>'nullable|string',


    ];

}
