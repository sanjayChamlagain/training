<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\ParticipantInfo;
use App\District;
use App\TrainingInfo;
use Session;
use App\Log;
use Illuminate\Support\Facades\Hash;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;

class pprController extends Controller {
 public function index(){
      return view('ppr.index',['train'=>$this->getTrain()]);
     
    }
    public function getTrain(){
   return \App\TrainingInfo::select('id','name')->get();
}
public function lists(Request $request) {
	 $id=$request->input("id");
     $basis=$request->input("basis");
         
     if($basis=="gender"){
     	
 $m = DB::table('participant_info')->where('gender','=','Men')->where('training_id','=',$id)->count();
         $f = DB::table('participant_info')->where('gender','=','Women')->where('training_id','=',$id)->count();
         $data=json_encode(["Men"=>$m,"Women"=>$f]);
    return view('piechart.index',["Topic"=>"Gender","data"=>$data]);
    
 }

 if($basis=="caste"){
 	
     $bc = DB::table('participant_info')->where('caste','=','Brahmin/Chhetri')->where('training_id','=',$id)->count();
      $dalit = DB::table('participant_info')->where('caste','=','Dalit')->where('training_id','=',$id)->count();
 	$ind = DB::table('participant_info')->where('caste','=','Indigenous Nationalities')->where('training_id','=',$id)->count();
 	$newar = DB::table('participant_info')->where('caste','=','Newar')->where('training_id','=',$id)->count();
 	$muslim = DB::table('participant_info')->where('caste','=','Muslim')->where('training_id','=',$id)->count();
 	$other = DB::table('participant_info')->where('caste','=','Others')->where('training_id','=',$id)->count();

   $data=json_encode(["Brahmin/Chhetri"=>$bc,"Dalit"=>$dalit,"Indigenous Nationalities"=>$ind,"Newar"=>$newar,"Muslim"=>$muslim,"Others"=>$other]);
return view('piechart.index',["Topic"=>"Caste","data"=>$data]);

 }
 if($basis=="position"){
 	$lv1 = DB::table('participant_info')->where('level','=','1')->where('training_id','=',$id)->count();
  $lv2 = DB::table('participant_info')->where('level','=','2')->where('training_id','=',$id)->count();
   $lv3 = DB::table('participant_info')->where('level','=','3')->where('training_id','=',$id)->count();
    $lv4 = DB::table('participant_info')->where('level','=','4')->where('training_id','=',$id)->count();

$data=json_encode(["1"=>$lv1,"2"=>$lv2,"3"=>$lv3,"4"=>$lv4]);
return view('piechart.index',["Topic"=>"Position","data"=>$data]);

 }
 if($basis=="age"){
 $age1 = DB::table('participant_info')->where('age','=','15-19')->where('training_id','=',$id)->count();
 $age2 = DB::table('participant_info')->where('age','=','20-24')->where('training_id','=',$id)->count();
 $age3 = DB::table('participant_info')->where('age','=','25-29')->where('training_id','=',$id)->count();
 $age4 = DB::table('participant_info')->where('age','=','30-34')->where('training_id','=',$id)->count();
 $age5 = DB::table('participant_info')->where('age','=','35-39')->where('training_id','=',$id)->count();
 $age6 = DB::table('participant_info')->where('age','=','40+')->where('training_id','=',$id)->count();

 $data=json_encode(["15-19"=>$age1,"20-24"=>$age2,"25-29"=>$age3,"30-34"=>$age4,"35-39"=>$age5,"40+"=>$age6]);
return view('piechart.index',["Topic"=>"Age Group","data"=>$data]);
 }
 if($basis=="agency"){
 	$central = DB::table('participant_info')->where('agency','=','Central/ Ministry Level')->where('training_id','=',$id)->count();
 	$regional = DB::table('participant_info')->where('agency','=','Regional/ State Level')->where('training_id','=',$id)->count();
 	$district = DB::table('participant_info')->where('agency','=','District/ Local Level')->where('training_id','=',$id)->count();

 	$data=json_encode(["Central/ Ministry Level"=>$central,"Regional/ State Level"=>$regional,"District/ Local Level"=>$district]);
return view('piechart.index',["Topic"=>"Agency","data"=>$data]);
 }
	}
}
