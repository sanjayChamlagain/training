<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingInfo extends BaseModel
{
    public $timestamps=false;
  protected $table = 'training_info';
    protected $fillable = ['name','organized_for','component','activity','immediate_result','training_type','description','start_date','end_date','target','achievement','venue','remarks'];
    protected $rules = [
        'name' => 'string',
        'organized_for' => 'integer','component' => 'integer','activity' => 'integer','immediate_result' => 'integer','training_type' => 'integer','start_date' => 'date','end_date' => 'date',
        'target'=>'string','achievement'=>'string','venue' => 'string','remarks' => 'string',
        
    ];
}

