<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 use App\Govorg;
 use App\Resourceperson;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class ResourceController extends Controller {

    public function index() {
      // return view('designation.index');
        //$countries = \App\Country::select('id','name_en')->get();
        return view('resource_person.index',['orgs'=>$this->getOrgs(),'desg'=>$this->getDesg()]);
    }

    
    public function creates(Request $request){
      $res = new Resourceperson();
      if($res->validate($request->all())){
          $res->fill($request->all());
          $res->save();
         
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{

        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $gender=$request->input("gender",null);
         if($gender==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Gender is required."]);
        }
         $caste=$request->input("caste",null);
         if($caste==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Caste is required."]);
        }
        $age=$request->input("age",null);
         if($age==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Age is required."]);
        }
         $expert=$request->input("expertise",null);
         if($expert==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Expertise is required."]);
        }
        $org=$request->input("organization_id",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $desg=$request->input("designation_id",null);
         if($desg==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Designation is required."]);
        }

          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to save data"]);
      }
    }

    
    public function edits($id){
        $res = Resourceperson::find($id,['id','name_en','name_np','gender','caste','age','expertise','designation_id','organization_id','phone','mobile','email','postal','approved','disabled']);
        return $res;
        //return view('designation.index',['update'=>$designation,'countries'=>$this->getCountries()]);
    }

    
    public function updates(Request $request,$id){
       $res = Resourceperson::find($id);
      if($res->validate($request->all())){
          $res->fill($request->all());
          $res->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $gender=$request->input("gender",null);
         if($gender==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Gender is required."]);
        }
         $caste=$request->input("caste",null);
         if($caste==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Caste is required."]);
        }
        $age=$request->input("age",null);
         if($age==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Age is required."]);
        }
         $expert=$request->input("expertise",null);
         if($expert==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Expertise is required."]);
        }
        $org=$request->input("organization_id",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization is required."]);
        }
        $desg=$request->input("designation_id",null);
         if($desg==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Designation is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update data"]);
      }
    }



    public function lists(Request $request) {
     $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
      if($search==null){
        $acts = DB::table('resource_person')->select(['resource_person.id', 'resource_person.name_en','resource_person.gender','resource_person.mobile','resource_person.expertise','gov_org.name_en as cname_en','designation.name as dname_en'])
        ->join('gov_org','resource_person.organization_id','=','gov_org.id')
        ->join('designation','resource_person.designation_id','=','designation.id')
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
      else{

        $acts = DB::table('resource_person')->select(['resource_person.id', 'resource_person.name_en','resource_person.gender','resource_person.mobile','resource_person.expertise','gov_org.name_en as cname_en','designation.name as dname_en'])
        ->join('gov_org','resource_person.organization_id','=','gov_org.id')
        ->join('designation','resource_person.designation_id','=','designation.id')
        ->where('resource_person.name_en', 'LIKE', "%$search%")
         
         ->orwhere('resource_person.expertise','LIKE',"%$search%")
         ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }

    }

   
    public function deletes($id){
        $res = Resourceperson::find($id);
        try{
        $res->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }
      catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }

    public function getOrgs(){
        return \App\Govorg::select('id','name_en')->get();
    }

 public function getDesg(){
        return \App\Designation::select('id','name')->get();
    }
	
}
