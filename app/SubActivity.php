<?php

namespace App;

class SubActivity extends BaseModel {

    protected $table = 'project_subactivity';
     
    protected $fillable = ['component_id','activity_id','subactivity', 'description'];
    protected $rules = [
        'component_id'=>'integer',
        'activity_id' => 'integer',
        'subactivity'=>'string|required',
        'description' => 'string'];

}
