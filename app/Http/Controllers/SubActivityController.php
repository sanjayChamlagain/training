<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubActivity;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class SubActivityController extends Controller {

    public function index() {
       // return view('project_activity.index');
        //$countries = \App\Country::select('id','name_en')->get();
        return view('project_subactivity.index',['comp'=>$this->getComps(),'act'=>$this->getActs()]);
    }

      public function getActivity($id){
        return \App\Activity::select('id','activity')->where('component_id','=',$id)->get();

   }
    
    public function creates(Request $request){
      $Org = new SubActivity();
      if($Org->validate($request->all())){
          $Org->fill($request->all());
          $Org->save();
         
          return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Saved"]);
      }else{
        $component_id=$request->input("component_id",null);
         if($component_id==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Component is required."]);
        }
        $act=$request->input("activity_id",null);
        if($act==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Activity is required."]);

        }
        $sact=$request->input("subactivity",null);
        if($act==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* SubActivity is required."]);

        }
        

         $desc=$request->input("description",null);
        if($desc==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Description is required."]);

        }
          return json_encode(['status'=>0,'title'=>"error",'text'=>"Failed to save data"]);
      }
    }

    
    public function edits($id){
        $Activity = SubActivity::find($id,['id','component_id','activity_id','subactivity','description']);
        return $Activity;
        //return view('Activity.index',['update'=>$Activity,'countries'=>$this->getCountries()]);
    }

    
    public function updates(Request $request,$id){
       $Activity = SubActivity::find($id);
      if($Activity->validate($request->all())){
          $Activity->fill($request->all());
          $Activity->save();
          return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Updated"]);
      }else{
        $component_id=$request->input("component_id",null);
         if($component_id==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Component is required."]);
        }
        $act=$request->input("activity_id",null);
        if($act==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Activity is required."]);

        }

$sact=$request->input("subactivity",null);
        if($act==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* SubActivity is required."]);

        }
        
         $desc=$request->input("description",null);
        if($desc==""){
                    return json_encode(['status'=>0,'title'=>"error",'text'=>"* Description is required."]);

        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update data"]);
      }
    }



    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
      if($search==null){
        $acts = DB::table('project_subactivity')->select(['project_subactivity.id', 'project_subactivity.subactivity','project_subactivity.description','project_activity.activity', 'project_component.name as cname_en'])
        ->join('project_component','project_subactivity.component_id','=','project_component.id')->join('project_activity','project_subactivity.activity_id','=','project_activity.id')
        ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
      else{

        $acts = DB::table('project_subactivity')->select(['project_subactivity.id','project_subactivity.description', 'project_subactivity.subactivity','project_activity.activity', 'project_component.name as cname_en'])
        ->join('project_component','project_subactivity.component_id','=','project_component.id')->join('project_activity','project_subactivity.activity_id','=','project_activity.id')
       ->where('project_activity.activity', 'LIKE', "%$search%")
         
         ->orwhere('project_component.name','LIKE',"%$search%")
         ->Paginate($entry,['*'],'page', $page );
        return $acts;
      }
    }

   
    public function deletes($id){
        $Activity = SubActivity::find($id);
        try{
        $Activity->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }
      catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }

    public function getComps(){
        return \App\Project::select('id','name')->get();
    }

  public function getActs(){
        return \App\Activity::select('id','activity')->get();
    }

	
}
