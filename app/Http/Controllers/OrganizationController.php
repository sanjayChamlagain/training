<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Input;


class OrganizationController extends Controller {

    public function index(Request $request) {
        

        return view('organization.index',['orgs'=>$this->getOrgs()]);
   
      // return view('organization.index');
        //$countries = \App\Country::select('id','name_en')->get();
         }

    
    public function creates(Request $request){
      $Org = new Organization();
      if($Org->validate($request->all())){
          $Org->fill($request->all());
          $Org->save();
         
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Saved"]);
      }else{
        $code=$request->input("code",null);
         if($code==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Code is required."]);
        }

        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $org=$request->input("org_type",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization Type is required."]);
        }
        $dis=$request->input("district",null);
         if($dis==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* District is required."]);
        }
        $vdc=$request->input("vdc",null);
         if($vdc==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Vdc is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to save data"]);
      }
    }

    
    public function edits($id){
        $Organization = Organization::find($id,['id','code','name_en','name_np','org_type','district','vdc','ward','street','house_no','latitude','longitude','phone','fax','email','website']);
        return $Organization;
        //return view('Organization.index',['update'=>$Organization,'countries'=>$this->getCountries()]);
    }

    
    public function updates(Request $request,$id){
       $Organization = Organization::find($id);
      if($Organization->validate($request->all())){
          $Organization->fill($request->all());
          $Organization->save();
          return json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Updated"]);
      }else{
        $code=$request->input("code",null);
         if($code==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Code is required."]);
        }

        $name_en=$request->input("name_en",null);
         if($name_en==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(english) is required."]);
        }
        $name_np=$request->input("name_np",null);
         if($name_np==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Name(nepali) is required."]);
        }
        $org=$request->input("org_type",null);
         if($org==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Organization Type is required."]);
        }
        $dis=$request->input("district",null);
         if($dis==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* District is required."]);
        }
        $vdc=$request->input("vdc",null);
         if($vdc==""){
          return json_encode(['status'=>0,'title'=>"error",'text'=>"* Vdc is required."]);
        }
          return json_encode(['status'=>0,'title'=>"Error",'text'=>"Failed to update data"]);
      }
    }



    public function lists(Request $request) {
      $entry=$request->input("entry");
     $search=$request->input("search",null);
      $page=$request->input("page",null);
     // return [$pgno,$srch];
       if($page==null){
          $page=1;
        }
    if($search==null){
       return $countries = DB::table('organization')->paginate($entry,['*'],'page', $page );

      // $table=$this->getData($countries);
     }
     else{

       $countries=DB::table('organization')->where('name_en', 'LIKE', "%$search%")->orwhere('name_np','LIKE',"%$search%")->paginate($entry,['*'],'page', $page );
       return $countries;
     }

       return $table.$countries->links();




        // return Datatables::of($Organizations)->addColumn('action', function ($Organizations) {
        //         return '<a href="javascript:void(0)" onClick="stedit('.$Organizations->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
        //                 '&nbsp;&nbsp;<a href="javascript:void(0)" class="btn btn-xs btn-danger" onClick="stdelete('.$Organizations->id.')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
        //     })->make();
    }

    public function delete($id){
        $Organization = Organization::find($id);
        $Organization->delete();
        return redirect('/Organization')->with('msg',json_encode(['status'=>1,'title'=>"Success",'text'=>"Data Successfully Deleted"]));
    }
    public function deletes($id){
        $Organization = Organization::find($id);
        try{
        $Organization->delete();
        return json_encode(['status'=>1,'title'=>"success",'text'=>"Data Successfully Deleted"]);
      }
      catch(\Exception $e){
        return json_encode(['status'=>0,'title'=>"error",'text'=>"Unable to Delete Parent row"]);
      }
    }

    public function getOrgs(){
        return \App\Org::select('id','name_en')->get();
    }


	
}
